jQuery(document).ready(function() {









    jQuery("#new_category").validate({
    errorElement:'div',
    rules: {
        
       "category[name]":{
                required:true,
                maxlength:100
                            
            }
                        
        },
    messages: {
    
      "category[name]":{
                required:"Please enter category name"
            }
                
                                                                
        }
        
    });



    jQuery("#new_publisher").validate({
    errorElement:'div',
    rules: {
        
       "publisher[name]":{
                required:true,
                maxlength:100
                            
            }
                        
        },
    messages: {
    
      "publisher[name]":{
                required:"Please enter publisher name"
            }
                
                                                                
        }
        
    });



    jQuery("#new_news_update").validate({
    errorElement:'div',
    rules: {
        
       "news_update[title]":{
                required:true
                            
            },
            "news_update[description]":{
                required:true
                            
            }
                        
        },
    messages: {
    
      "news_update[title]":{
                required:"Please enter news title"
            },

              "news_update[description]":{
                required:"Please enter news description"
            }   
                                                                
        }
        
    });
    jQuery("#new_report").validate({
    errorElement:'div',
    rules: {
        
       "report[title]":{
                required:true
                            
            },
            "report[description]":{
                required:true
                            
            },
            "report[price]":{
                required:true
                            
            },
            "report[report_type_id]":{
                required:true
                            
            },
            "report[category_id]":{
                required:true
                            
            },
            "report[country_id]":{
                required:true
                            
            }

                        
        },
    messages: {
    
      "report[title]":{
                required:"Please enter report title"
            },

              "report[description]":{
                required:"Please enter report description"
            },
            "report[price]":{
               required:"Please enter report price"
                            
            },
            "report[report_type_id]":{
               required:"Please select report publisher"
                            
            },
            "report[category_id]":{
                required:"Please select report category"
                            
            },
            "report[country_id]":{
              required:"Please select report country"
                            
            }   
                                                                
        }
        
    });
});