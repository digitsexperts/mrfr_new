function buy(form_data){
    $("#terms_popup").click();
    // $.blockUI({ message: '<h3 style="color:#fff"> <b>Please wait...</b></h3>' });
  
}


function getUserDetail(form_data){
  $.ajax({
    url: '/user_detail',
    type: 'POST',
    data: form_data,
    success: function(data){
      $('#hdfc_billing_name').val(JSON.parse(data.user).first_name +" "+ JSON.parse(data.user).last_name);
      $('#hdfc_billing_address').val(JSON.parse(data.user).customer.address_street1+" "+JSON.parse(data.user).customer.address_street2);
      $('#hdfc_billing_city').val(JSON.parse(data.user).customer.city);
      $('#hdfc_billing_zip').val(JSON.parse(data.user).customer.zip_code);
      $('#hdfc_billing_tel').val(JSON.parse(data.user).customer.phone_no);
      $('#hdfc_billing_email').val(JSON.parse(data.user).email);
      hdfcSecret();
    }
  })
}

function hdfcSecret(){
  $.ajax({
    url: '/hdfc_secret',
    type: 'POST',
    data: $('#hdfc_form').serialize(),
    success: function(data){
      $('#hdfc_secret').val(data.hdfc_secret);

      $("#hdfc_form").submit();
    }
  })
}
function registerNewUser(callback,form_data){
  $.ajax({
    url: "/register_user",
    type: "POST",
    data: $("#review_order_form").serialize(),
    success: function(data){

      if(data.success){
        $("#existing_email").val(data.user.email);
        $("#existing_password").val($('#password').val())
        callback(form_data);
      }else{
        alert(data.error_message);
      }
    }
  })
}

function checkValidLogin(callback,form_data){
  $.ajax({
    url: "/login_user",
    type: "POST",
    data: {email: $("#existing_email").val(), password: $("#existing_password").val()},
    success: function(data){

      if(data.valid_credentials){
        callback(form_data);
      }else{
        alert("Invalid email or password.")
      }
    }
  })
}

function purchaseCCAVENUE(form_data){
  if (form_data.includes('report_id')){
    $.ajax( {
      type: "POST",
      url: '/buynow_ccavenue_transfer',
      data: form_data,
      success: function( response ) {   
       $("#billing_name").val(response.first_name+ " "+ response.last_name);
       $("#billing_address").val(response.customer.address_street1+ " "+ response.customer.address_street2);
       $("#billing_city").val(response.customer.city);
       $("#billing_zip").val(response.customer.zip_code);
       $("#billing_country").val(response.customer.country.name);
       $("#billing_tel").val(response.customer.phone_no);
       $("#billing_email").val($("#existing_email").val());
       $("#amount").val(response.price);
       $("#currency").val(response.currency);
       $("#ccavenue_buynow_form").submit();
      },
      error: function(response){
      }
    });
  }else{
    $.ajax( {
      type: "POST",
      url: '/ccavenue_transfer',
      data: {sendto: $("#existing_email").val(), password: $("#existing_password").val()},
      success: function( response ) {   
       $("#billing_name").val(response.first_name+ " "+ response.last_name);
       $("#billing_address").val(response.customer.address_street1+ " "+ response.customer.address_street2);
       $("#billing_city").val(response.customer.city);
       $("#billing_zip").val(response.customer.zip_code);
       $("#billing_country").val(response.customer.country.name);
       $("#billing_tel").val(response.customer.phone_no);
       $("#billing_email").val($("#existing_email").val());
       $("#cc_avenue_form").submit();
      },
      error: function(response){
      }
    });
  }

}

function purchaseWireTransfer(form_data){
  $.ajax( {
    type: "POST",
    data: form_data,
    url: '/wire_transfer',
    success: function( response ) {
      window.location.href = response.redirect_url;
    },
    error: function(response){
    }
  });
}

function purchasePaypal(form_data){
  if (form_data.includes('report_id')){
    $.ajax({
      type: "POST",
      data: form_data,
      url: '/pay_checkout',
      success: function( response ) {
        window.location.href = response.redirect_url;
      },
      error: function(response){
    
      }
    });
  }else{
    $.ajax({
      type: "POST",
      url: '/express_checkout',
      success: function( response ) {
        window.location.href = response.redirect_url;
      },
      error: function(response){
    
      }
    });
  }

}



function userSignedIn(){
 //if user is signed in then input #receive_updates_yes does not exist on the page
 return($('#receive_updates_yes').length==0);
}

function completeOrder(){
  
  var form_data = $("#review_order_form").serialize();
  

    if(userSignedIn()){
      if ($("#existing_password").val() == ""){alert("Please enter password."); return false;}
      checkValidLogin(buy,form_data);
    }else{
      if ($('#receive_updates_no').is(':checked')){
        if ($("#first_name").val() == "" || $("#address_street1").val() == "" || $("#city").val()=="" || $( "#country_select" ).val() == "" || $("#phone_no").val() == "" ||  $("#email").val() =="" ){
          alert("Kindly fill all the details in the registration form."); return false;
        }
        registerNewUser(buy,form_data);
      }else if($('#receive_updates_yes').is(':checked')){
        if ($("#existing_email").val() == ""){alert("Please enter email."); return false;}
        if ($("#existing_password").val() == ""){alert("Please enter password."); return false;}
        checkValidLogin(buy,form_data);
      }
    }

}


function submitTerms(){
// $('#submit_terms').click(function(){
  $('#terms_condition').modal('hide');
  $.blockUI({ message: '<h3 style="color:#fff"> <b>Please wait...</b></h3>' });
  var form_data = $("#review_order_form").serialize();
  payment_method = $("[name='paymentmethods']:checked");
  if (payment_method.hasClass("credit_card")){
    purchaseCCAVENUE(form_data); 
  }else if (payment_method.hasClass("wiretransfer")) {
    purchaseWireTransfer(form_data);
  }else if (payment_method.hasClass("paypal")){
    purchasePaypal(form_data);
  }else if (payment_method.hasClass("hdfc")){
    getUserDetail(form_data);
  }
};


function creditcardSubmit(){
  cart_has_items();
}

function cart_has_items(){
  if(validCreditCardform()){
    $.blockUI({ message: '<h4><img src="/assets/busy.gif" />  Please wait...</h4>' });
    payStripe();
  }else{
    alert("invalid")
    $.unblockUI();
  }

}


var stripeResponseHandlerCVC = function(status, response) {
var $form = $('#review_order_form');
$user_name="";
if (response.error) {
  // Show the errors on the form
  alert(response.error.message);
  $.unblockUI();
  return false;
} else {
  // token contains id, last4, and card type
  // var card_id = $("[name='payment_options']:checked").val()
  // var address_id = $("[name='existing_address']:checked").val() || $("#address_id").val()
  // var card_number = $("#Text1").val();
  var token = response.id;
  // Insert the token into the form so it gets submitted to the server
  // review_form_data = encoded_review_form();
  $.ajax( {
    type: "POST",
    url: '/charges',
    data: { "stripeToken": token},
    success: function( response ) {
      if(response.error){
        alert(response.error);
      }else{
        window.onbeforeunload = null;
        parent.location.href = response.redirect_url;
        $('#cvc_field').removeAttr('disabled');
        $('#verify').removeAttr('disabled');
      }
    },
    error: function(response){
      $("#verify_cvc_modal .modal-body").prepend(response.message);
    }
  });
}
}

function validCreditCardform(){
  number = $("[data-stripe='number']").val();
  cvc = $("[data-stripe='cvc']").val();
  // address = $("#address_id").val();
  var valid = $.payment.validateCardNumber(number);
  var validcvc = $.payment.validateCardCVC(cvc); 
  if(valid && validcvc ){
    return true;
  }else{
    return false;
  }
}

 function payStripe(){
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'));
  Stripe.card.createToken({
    number: $("[data-stripe='number']").val(),
    cvc: $("[data-stripe='cvc']").val(),
    exp_month: $("[data-stripe='exp-month']").val(),
    exp_year: $("[data-stripe='exp-year']").val(),
    address_line1: $("[data-source='street1']").text(),
    address_line2: $("[data-source='street2']").text(),
    // address_city: card.stripe_card.address_city,
    address_state: $("[data-source='state']").text(),
    address_zip: $("[data-source='country']").text(),
    // address_country: card.stripe_card.address_country,
    // name: $user_name
  }, stripeResponseHandlerCVC);
}



  $("#message").keypress(function(e) {
      if(e.which == 13) {
        $.ajax({
          url: "/messages/create",
          type: "POST",
          data: {message: $(this).val(),id: $(this).attr("data-id")}
        })
       }
    })