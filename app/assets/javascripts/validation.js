jQuery(document).ready(function() {




   jQuery.validator.addMethod("alpha", function (value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    });




    jQuery("#new_category").validate({
    errorElement:'div',
    rules: {
        
       "category[name]":{
                required:true,
                maxlength:100
                            
            }
                        
        },
    messages: {
    
      "category[name]":{
                required:"Please enter category name"
            }
                
                                                                
        }
        
    });



    jQuery("#new_publisher").validate({
    errorElement:'div',
    rules: {
        
       "publisher[name]":{
                required:true,
                maxlength:100
                            
            }
                        
        },
    messages: {
    
      "publisher[name]":{
                required:"Please enter publisher name"
            }
                
                                                                
        }
        
    });
    jQuery("#subscriber_form").validate({
    errorElement:'div',
    rules: {
        
       "subscriber[email]":{
                required:true,
                email:true
                            
            }
                        
        },
    messages: {
    
      "subscriber[email]":{
                required:"Please enter an email",
                email:"Please an valid email"
            }
                
                                                                
        }
        
    });
  jQuery("#chat_form").validate({
    errorElement:'div',
    rules: {
        
       "name":{
                required:true,
                maxlength:100
                            
            },
             "email":{
                required:true,
                email:true
                            
            },
             "phone_no":{
       
                maxlength:100,
                number: true
                            
            },
             "message":{
                required:true,
                maxlength:500
                            
            }             
        },
    messages: {
    
           "name":{
                required:"Please enter name"
            },
           "email":{
                required:"Please enter email"
            }, 
             "message":{
                required:"Please enter message"
            }   
                                                                
        }
        
    });

  jQuery("#email_report_form").validate({
    errorElement:'div',
    rules: {
        
       "name":{
                required:true,
                maxlength:100
                            
            },
             "email":{
                required:true,
                email:true
                            
            },
             "friend_name":{
                required:true,
                maxlength:100
                            
            },
             "friend_email":{
                required:true,
                email:true
                            
            },
             "message":{
                required:true,
                maxlength:500
                            
            }             
        },
    messages: {
    
           "name":{
                required:"Please enter name"
            },
           "email":{
                required:"Please enter email"
            }, 
             "message":{
                required:"Please enter message"
            }   
                                                                
        }
        
    });




    jQuery("#send_email_report_form").validate({
    errorElement:'div',
    rules: {
        
       "send_name":{
                required:true,
                maxlength:100
                            
            },
             "send_email":{
                required:true,
                email:true
                            
            },
             "send_friend_name":{
                required:true,
                maxlength:100
                            
            },
             "send_friend_email":{
                required:true,
                email:true
                            
            },
             "send_message":{
                required:true,
                maxlength:500
                            
            }             
        },
    messages: {
    
           "send_name":{
                required:"Please enter name"
            },
           "send_email":{
                required:"Please enter email"
            }, 
             "send_message":{
                required:"Please enter message"
            } 
            , 
             "send_friend_name":{
                required:"Please enter name"
            }, 
             "send_friend_email":{
                required:"Please enter email"
            }   
                                                                
        }
    });


    jQuery("#new_news_update").validate({
    errorElement:'div',
    rules: {
        
       "news_update[title]":{
                required:true
                            
            },
            "news_update[description]":{
                required:true
                            
            }
                        
        },
    messages: {
    
      "news_update[title]":{
                required:"Please enter news title"
            },

              "news_update[description]":{
                required:"Please enter news description"
            }   
                                                                
        }
        
    });
    jQuery("#new_report").validate({
    errorElement:'div',
    rules: {
        
       "report[title]":{
                required:true
                            
            },
            "report[description]":{
                required:true
                            
            },
            "report[price]":{
                required:true
                            
            },
            "report[no_of_pages]":{
                required:true
                            
            },
            "report[report_type_id]":{
                required:true
                            
            },
            "report[category_id]":{
                required:true
                            
            },
           
            "report[report_pdf]":{
               required:true  
            },
           "report[one_user]":{
               required:true  
            }
                        
        },
    messages: {
    
      "report[title]":{
                required:"Please enter report title"
            },

              "report[description]":{
                required:"Please enter report description"
            },
            "report[price]":{
               required:"Please enter report price"
                            
            },
            "report[no_of_pages]":{
               required:"Please enter no of pages"
                            
            },
            "report[report_type_id]":{
               required:"Please select report publisher"
                            
            },
            "report[category_id]":{
                required:"Please select report category"
                            
            },
          
            "report[report_pdf]":{
               required:"Please select report pdf"
            } ,  
            "report[one_user]":{
               required:"Please enter report price"
            }                                                      
        }
        
    });

    jQuery("#update_quantity").validate({
    errorElement:'div',
    rules: {
        
       "item[quantity]":{
                required:true,
                number: true,
                min: 1       
            }
                        
        },
    messages: {
    
      "item[quantity]":{
                required:"Please enter valid quantity"
            }                                                
        }
        
    });

    jQuery("#new_user").validate({
    errorElement:'div',
    rules: {
        "user[first_name]":{
                required:true,
                alpha: true,
                maxlength: 100
                            
            },
            "user[email]":{
                required:true,
                email:true
                            
            },
            "user[password]":{
                required:true
                            
            },
            "user[password_confirmation]":{
                required:true,
                 equalTo: "#password"
                            
            }
                        
        },
    messages: {
    
      "user[first_name]":{
                required:"Please enter first name",
                alpha: "Please enter letters"
            },

            "user[email]":{
               required:"Please enter email"
                            
            },     
             "user[password]":{
                    required: "Please enter password"
                   
              },
            "user[password_confirmation]":{
                    required:"Please enter confirmation password",
                    equalTo: "Password does not match"
            }                                                   
        }
        
    });


    jQuery("#new_customer").validate({
    errorElement:'div',
    rules: {
        "customer[company_name]":{
                required:true,
                alpha: true,
                maxlength: 100
                            
            },
            "customer[job_title]":{
                required:true,
                alpha: true,
                maxlength: 100
                            
            },
            "customer[address_street1]":{
                required:true
                            
            },
            "customer[city]":{
                required:true,
                alpha: true,
                maxlength: 100
                            
            },
            "customer[zip_code]":{
                required:true,
                zipcode:true
                            
            },
            "customer[phone_no]":{
                required:true,
                digits: true
                            
            }
                        
        },
    messages: {
    
      "customer[company_name]":{
                required:"Please enter company name",
                alpha: "Please enter letters"
            },

            "customer[job_title]":{
               required:"Please enter job title",
                alpha: "Please enter letters"
                            
            },     
             "customer[address_street1]":{
                    required: "Please enter address_street1"
                   
              },
            "customer[city]":{
                    required:"Please enter valid city",
                alpha: "Please enter letters"
            },
            "customer[zip_code]":{
                    required:"Please enter valid zip code"
            },
            "customer[phone_no]":{
                    required:"Please enter valid phone no"
            }                                                         
        }
        
    });



});