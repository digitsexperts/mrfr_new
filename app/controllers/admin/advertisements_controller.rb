class Admin::AdvertisementsController < Admin::BaseController
  before_action :set_advertisement, only: [:show, :edit, :update, :destroy]


  def index
    @advertisements = Admin::Advertisement.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @advertisement = Admin::Advertisement.new
  end

  def create
    
    @advertisement = Admin::Advertisement.new(advertisement_params)
    if @advertisement.save
      flash[:success] = "Advertisement created successfully"
      redirect_to admin_advertisements_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @advertisement.update_attributes(advertisement_params)
      flash[:success] = "Advertisement saved successfully"
      redirect_to admin_advertisements_path
    else
      render :new
    end
  end


  def destroy
    if @advertisement.destroy
      flash[:success] = "Advertisement deleted successfully"
      redirect_to admin_advertisements_path
    end 
  end



  def show
  end


  private



  def set_advertisement
    @advertisement = Admin::Advertisement.find(params[:id])
  end

  def advertisement_params
    params.require(:admin_advertisement).permit(:name, :advertisement_image)
  end
end
