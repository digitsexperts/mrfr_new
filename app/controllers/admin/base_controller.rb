class Admin::BaseController < ApplicationController
  before_action :require_user, except: [:mark_posted,:mark_failed, :make_website_automation_on, :make_website_automation_off]
  layout 'admin'
  

  def background(&block)
    Thread.new do
      yield
      ActiveRecord::Base.connection.close
    end
  end
end
