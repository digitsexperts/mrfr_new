class Admin::BlogRssFeedsController < Admin::BaseController
  before_action :set_admin_blog_rss_feed, only: [:show, :edit, :update, :destroy]

  # GET /admin/blog_rss_feeds
  # GET /admin/blog_rss_feeds.json
  def index
    @admin_blog_rss_feeds = Admin::BlogRssFeed.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /admin/blog_rss_feeds/1
  # GET /admin/blog_rss_feeds/1.json
  def show
  end

  # GET /admin/blog_rss_feeds/new
  def new
    @admin_blog_rss_feed = Admin::BlogRssFeed.new
  end

  # GET /admin/blog_rss_feeds/1/edit
  def edit
  end

  # POST /admin/blog_rss_feeds
  # POST /admin/blog_rss_feeds.json
  def create
    @admin_blog_rss_feed = Admin::BlogRssFeed.new(admin_blog_rss_feed_params)

    respond_to do |format|
      if @admin_blog_rss_feed.save
        format.html { redirect_to admin_blog_rss_feeds_path, notice: 'Blog rss feed was successfully created.' }
        format.json { render :show, status: :created, location: @admin_blog_rss_feed }
      else
        format.html { render :new }
        format.json { render json: @admin_blog_rss_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/blog_rss_feeds/1
  # PATCH/PUT /admin/blog_rss_feeds/1.json
  def update
    respond_to do |format|
      if @admin_blog_rss_feed.update(admin_blog_rss_feed_params)
        format.html { redirect_to admin_blog_rss_feeds_path, notice: 'Blog rss feed was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_blog_rss_feed }
      else
        format.html { render :edit }
        format.json { render json: @admin_blog_rss_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/blog_rss_feeds/1
  # DELETE /admin/blog_rss_feeds/1.json
  def destroy
    @admin_blog_rss_feed.destroy
    respond_to do |format|
      format.html { redirect_to admin_blog_rss_feeds_url, notice: 'Blog rss feed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_blog_rss_feed
      @admin_blog_rss_feed = Admin::BlogRssFeed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_blog_rss_feed_params
      params.require(:admin_blog_rss_feed).permit(:url, :title, :number_of_feeds)
    end
end
