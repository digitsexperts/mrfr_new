class Admin::CategoriesController < Admin::BaseController

  after_filter :expire_cache, only: [:create,  :update, :destroy]
  before_filter :correct_user

  def index
    @categories = Category.arrange_as_array({:order => 'name'})
  end

  def show
    @category = Category.friendly.find(params[:id])
    render :index
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      ActionController::Base.new.expire_fragment(/admin-all-categories/)
      flash[:success] = "Category created successfully"
      redirect_to admin_categories_path
    else
      render :new
    end
  end
    
  def edit
    @category = Category.friendly.find(params[:id])
    @categories = Category.all
  end

  def update
    @category = Category.friendly.find(params[:id])
    if @category.update_attributes(category_params)
      ActionController::Base.new.expire_fragment(/admin-all-categories/)
      flash[:success] = "Category saved successfully"
      redirect_to admin_categories_path
    else
      render :new
    end
  end


  def destroy
    @category = Category.friendly.find(params[:id])
    if @category.destroy
      ActionController::Base.new.expire_fragment(/admin-all-categories/)
      flash[:success] = "Category deleted successfully"
      redirect_to admin_categories_path
    end
  end
  private

  def correct_user
    redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
  end

  def category_params
    params.require(:category).permit(:name,:parent_id,:names_depth_cache,:abbreviation,:category_report_image, :category_image, :category_icon)
  end

  def expire_cache
    expire_fragment('all_categories')
    expire_fragment('allcategories')
  end

end
