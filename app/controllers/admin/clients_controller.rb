class Admin::ClientsController < Admin::BaseController
  before_action :set_client, only: [:show, :edit, :update, :destroy]


  def index
    @clients = Client.order("name asc").paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @client = Client.new
  end

  def create
    
    @client = Client.new(client_params)
    if @client.save
      ActionController::Base.new.expire_fragment(/admin-client-list/)
      ActionController::Base.new.expire_fragment(/client-section/)
      ActionController::Base.new.expire_fragment(/client_name_images/)
      flash[:success] = "Client created successfully"
      redirect_to admin_clients_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @client.update_attributes(client_params)
      ActionController::Base.new.expire_fragment(/admin-client-list/)
      ActionController::Base.new.expire_fragment(/client-section/)
      ActionController::Base.new.expire_fragment(/client_name_images/)
      flash[:success] = "Client saved successfully"
      redirect_to admin_clients_path
    else
      render :new
    end
  end


  def destroy
    if @client.destroy
      ActionController::Base.new.expire_fragment(/admin-client-list/)
      ActionController::Base.new.expire_fragment(/client-section/)
      ActionController::Base.new.expire_fragment(/client_name_images/)
      flash[:success] = "Client deleted successfully"
      redirect_to admin_clients_path
    end 
  end



  def show
  end


  private



  def set_client
    @client = Client.friendly.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:name,:client_logo)
  end
end
