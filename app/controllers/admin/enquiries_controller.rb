class Admin::EnquiriesController < Admin::BaseController

  def index

    date = Date.parse(params[:date])
    start_date = date.beginning_of_day
    end_date = date.end_of_day
    @enquiries = Enquiry.where("created_at >= ? && created_at <=?",start_date,end_date).order("id desc").paginate(:per_page=>10,:page=>params[:page]) rescue []
    if params[:type].present?
      @enquiries = @enquiries.where("enquiry_type in (?)", params[:type].parameterize.underscore)
    end
  end

  def list
    require 'will_paginate/array'
    @dates = Enquiry.select("created_at").group("created_at").order("created_at desc").map{|record| record.created_at.strftime("%Y-%m-%d")}.uniq.paginate(:per_page=>30,:page=>params[:page]) rescue []
  end

  def show
  	 @enquiry = Enquiry.find(params[:id])
  end

  def destroy
  	 @enquiry = Enquiry.find(params[:id])
  	 if @enquiry.destroy
       flash[:success] = "Enquiry deleted successfully"
       redirect_to list_admin_enquiries_path
     end 
  end

  def delete_list
    date = Date.parse(params[:date])
    start_date = date.beginning_of_day
    end_date = date.end_of_day
    if Enquiry.where("created_at >= ? && created_at <=?",start_date,end_date).delete_all
       flash[:success] = "Enquiries deleted successfully"
       redirect_to list_admin_enquiries_path      
    end
  end

  def contact_us
     @contact_us = ContactUs.paginate(:page => params[:page], :per_page => 10)
  end

  def delete_contact_us
     @contact_us = ContactUs.find(params[:id])
     if @contact_us.destroy
       flash[:success] = "Contact Us deleted successfully"
       redirect_to admin_contact_us_path
     end 
  end

  def edit_multiple
    Enquiry.delete_all(id: params[:id])
    render :json=>{:status=>200}
  end
  
end
