class Admin::PagesController < Admin::BaseController
  before_action :set_admin_page, only: [:show, :edit, :update, :destroy]
  before_filter :correct_user 
  # GET /admin/pages
  # GET /admin/pages.json
  def index
    @admin_pages = Admin::Page.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /admin/pages/1
  # GET /admin/pages/1.json
  def show
  end

  # GET /admin/pages/new
  def new
    @admin_page = Admin::Page.new
  end

  # GET /admin/pages/1/edit
  def edit
  end

  # POST /admin/pages
  # POST /admin/pages.json
  def create
    @admin_page = Admin::Page.new(admin_page_params)

    respond_to do |format|
      if @admin_page.save
        format.html { redirect_to admin_pages_url, notice: 'Page was successfully Created.' }
        format.json { render :show, status: :created, location: @admin_page }
      else
        format.html { render :new }
        format.json { render json: @admin_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/pages/1
  # PATCH/PUT /admin/pages/1.json
  def update
    respond_to do |format|
      if @admin_page.update(admin_page_params)
          format.html { redirect_to admin_pages_url, notice: 'Page was successfully Updated.' }
        format.json { render :show, status: :ok, location: @admin_page }
      else
        format.html { render :edit }
        format.json { render json: @admin_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/pages/1
  # DELETE /admin/pages/1.json
  def destroy
    @admin_page.destroy
    respond_to do |format|
      format.html { redirect_to admin_pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def correct_user
     redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_page
      @admin_page = Admin::Page.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_page_params
      params.require(:admin_page).permit(:title, :description, :slug)
    end
end
