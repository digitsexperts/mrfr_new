class Admin::ReportTypesController < Admin::BaseController
  before_action :set_report_type, only: [:show, :edit, :update, :destroy]
  before_filter :correct_user

  def index
    @report_types = ReportType.order("name asc").paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @report_type = ReportType.new
  end

  def create
    
    @report_type = ReportType.new(report_type_params)
    if @report_type.save
      # ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      # ActionController::Base.new.expire_fragment(/allpublishers/)
      # ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Report Type created successfully"
      redirect_to admin_report_types_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @report_type.update_attributes(report_type_params)
      # ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      # ActionController::Base.new.expire_fragment(/allpublishers/)
      # ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Report Type saved successfully"
      redirect_to admin_report_types_path
    else
      render :new
    end
  end


  def destroy
    if @report_type.destroy
      # ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      # ActionController::Base.new.expire_fragment(/allpublishers/)
      # ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Report Type deleted successfully"
      redirect_to admin_report_types_path
    end 
  end



  def show
  end


  private

  def correct_user
    redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
  end

  def set_report_type
    @report_type = ReportType.friendly.find(params[:id])
  end

  def report_type_params
    params.require(:report_type).permit(:name,:description,:abbreviation,:seo_description,:report_type_image)
  end
end
