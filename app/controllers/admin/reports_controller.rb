class Admin::ReportsController < Admin::BaseController
  helper_method :sort_column, :sort_direction
  before_filter :correct_user

  def index
    @reports = Report.admin_filter(params)
  end

  def new
    @report = Report.new
    @categories = Category.arrange_as_array({:order => 'name'})
    @report_types = ReportType.all
    @countries = Country.all
    @regions = Region.all
    @continents = Continent.all
  end

  def create
    @report = Report.new(report_params)
    if @report.save
      flash[:success] = "Report created successfully"
      if params[:selectItemreport].nil? && params[:report][:region_id].blank? && params[:report][:continent_id].blank?
        @report.update_columns(:country_id => "Global")
      end
      # @report.generate_report_code
      redirect_to admin_reports_path
    else
      render :new
    end
  end

  def edit
    @report = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}')",:limit => 1).first
    category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
    @categories = Category.search(:max_matches =>category_count ,:sql=>{:select=>"categories.name,categories.id"},:per_page=>category_count)
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    @report_types = ReportType.search(:max_matches =>report_type_count ,:sql=>{:select=>"report_types.name,report_types.id"},:per_page=>report_type_count)
    @countries = Country.search(:sql=>{:select=>"countries.name,countries.id"},:per_page=>Country.search(:sql=>{:select=>"countries.id"}).total_entries)
    # @report = Report.unscoped.friendly.find(params[:id])
    # @categories = Category.all
    # @report_types = Publisher.all
    # @countries = Country.all
    session[:return_to] = params[:return_url]
  end

  def update
    @report = Report.unscoped.friendly.find(params[:id])
    country_id = params[:selectItemreport].present? ? params[:selectItemreport][":country_id"] : "Global"
    if @report.update_attributes(report_params.merge({:country_id=> country_id}))
      flash[:success] = "Report saved successfully"
      if session[:return_to] == "labels"
        redirect_to admin_report_labels_path
      else
        redirect_to admin_reports_path
      end
    else
      render :new
    end
    session[:return_to] = nil
  end


  def destroy
    @report = Report.unscoped.friendly.find(params[:id])
    if @report.destroy
      flash[:success] = "Report deleted successfully"
      redirect_to admin_reports_path
    end 
  end

  def show
    @report = Report.unscoped.friendly.find(params[:id])
  end
 
  def report_labels
     @reports = Report.search_report(params[:q],params[:page])
  end

  def search
    index
    render :index
  end

  def deactive_reports
    if params[:q].present?
      @search = Report.unscoped.deactivated.all.search(params[:q])
      @reports = @search.result
    else
      @reports = Report.unscoped.deactivated.paginate(:page => params[:page], :per_page => 10)
    end
    # @reports = Report.unscoped.deactivated
  end

  def edit_multiple
    
    if params[:report_ids].nil?
      flash[:error] = "Please select the reports"
      redirect_to admin_reports_path
    else
     if params[:delete_button] == "Delete Selected"
        Report.unscoped.where(id: params[:report_ids]).delete_all
        flash[:success] = "Reports deleted successfully"
        redirect_to admin_reports_path
     else
        @reports = Report.unscoped.find(params[:report_ids])
        session[:return_to] = params[:return_to]  
     end
   end
  end
  
  def upload_reports
    respond_to do |format|
      format.html {render layout: false}
    end
  end

  def update_multiple

    @reports = Report.unscoped.find(params[:report_ids])
    @reports.reject! do |report|
      if report.country_id == "Global"
        report.update_columns(country_id: "Global")
      end
      report.update_attributes(price_params.reject { |k,v| v.blank? })
    end
    if @reports.empty?
      if session[:return_to] == "deactive"
        redirect_to admin_deactive_reports_path
      elsif session[:return_to] == "labels"
        redirect_to admin_report_labels_path
      else
        redirect_to admin_reports_path
      end
    
    else
      @report = Report.new(params[:report])
      render "edit_multiple"
    end
    session[:return_to] = nil
  end



  def upload_multiple 
    
    user_agent = UserAgent.parse(request.env["HTTP_USER_AGENT"])

    if params[:title]== "" or params[:title].nil?
      positions = { :title=>"0", :category=>"1",:report_type=>"2", :pages=>"3", :one_user_pdf=>"4",:five_user_pdf=>"5", :site_pdf=>"6",
                    :enterprise_pdf=>"7", :publish_date=>"8", :description=>"9", :table_of_content=>"10",:country=>"11",:continent=>"12",:region=>"13"}
    else
      positions = { :title=>params[:title], :category=>params[:category], :report_type =>params[:report_type], :pages=>params[:pages], :one_user_pdf=>params[:one_user_pdf],:five_user_pdf => params[:five_user_pdf],
                    :site_pdf=>params[:site_pdf], :enterprise_pdf=>params[:enterprise_pdf], :publish_date=>params[:publish_date], 
                    :description=>params[:description], :table_of_content=>params[:table_of_content],
                    :country =>params[:country],:continent=>params[:continent],:region=>params[:region]}
    end
    excelsheet_file = params[:Browse].first

    Excelsheet.create(:excelsheet_file=>excelsheet_file,:ipaddress=> request.ip, :browser_os =>user_agent.browser+"-"+user_agent.os,:positions=>positions, :status => 0)

    #Excelsheet.dump_excelsheets


    respond_to do |format|
      format.html { redirect_to admin_upload_files_path }
      format.json { render :json => {files: [{ message: "Successfully uploaded excelsheet and dumping will start soon"}] } }
    end

  end
      
  def sort_reports
    case params[:report][:report_type] 
    when "Upcoming"
      @reports = Report.search(params[:q],:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:upcoming=>1},:order=>"publish_date desc",:per_page=>10)
    when "Top Selling"
      @reports = Report.search(params[:q],:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage, report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:upcoming=>0},:order=>"selling_count desc,publish_date desc",:per_page=>10)
    when "Latest Report"
      @reports = Report.search(params[:q],:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:upcoming=>0},:order=>"publish_date desc",:per_page=>10)
    end
  end

  def search_reports
  end





  private

  def correct_user
    redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
  end

  def report_params
    params.require(:report).permit(:browser_os,:ipaddress, :title,:country_id,:description,:table_of_content,:publish_month,:day, :price, :excelsheet_row,:report_type_id, :category_id,:report_image,:report_pdf,:no_of_pages, :upcoming,:selling_count,:one_user, :five_user, :site_user, :enterprise_user,:active,:code,:region_id,:continent_id,{country_ids: []},)
  end

  def price_params
     params.require(:report).permit(:price_modification,:active,:publish_month,:upcoming)
  end
  
  def sort_column
    Report.column_names.include?(params[:sort]) ? params[:sort] : "title"
  end

  def expire_cache
    expire_fragment('upcoming_reports')
    expire_fragment('topselling_reports')
    expire_fragment('latest_reports')
    expire_fragment('allcategories')
    expire_fragment('all_categories-#{@report.category_id}')
    expire_fragment('all_countries')
    expire_fragment('allcontinents')
    expire_fragment('allregions')
    expire_fragment('report_type_reports')
    expire_fragment('report_type_report-#{@report.report_type_id}')
    expire_fragment('report_by_type')
    
  end
end
