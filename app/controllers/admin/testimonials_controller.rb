class Admin::TestimonialsController < Admin::BaseController
  before_action :set_testimonial, only: [:show, :edit, :update, :destroy]
  before_filter :correct_user 

  def index
    @testimonials = Testimonial.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @testimonial = Testimonial.new
 
  end

  def create
    @testimonial = Testimonial.new(testimonial_params)
    if @testimonial.save
      flash[:notice] = "Testimonial created successfully"
      redirect_to admin_testimonials_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @testimonial.update_attributes(testimonial_params)
      flash[:notice] = "Testimonial saved successfully"
      redirect_to admin_testimonials_path
    else
      render :new
    end
  end


  def destroy
    if @testimonial.destroy
      flash[:notice] = "Testimonial deleted successfully"
      redirect_to admin_testimonials_path
    end 
  end



  def show
    
  end


  private

  def correct_user
    redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
  end

  def set_testimonial
    @testimonial = Testimonial.find(params[:id])
  end

  def testimonial_params
    params.require(:testimonial).permit(:name,:position,:message,:company_logo)
  end
end
