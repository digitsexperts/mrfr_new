class ApplicationController < ActionController::Base
  helper_method :current_user
  before_filter :initialize_cart, except: [:load_tweets, :mark_posted,:mark_failed, :make_website_automation_on, :make_website_automation_off]
  before_filter :allow_iframe,:set_global_search_variable

  def current_user
    @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
  end


  def get_crypted
    ActiveSupport::MessageEncryptor.new(Rails.configuration.secret_key_base)
  end

  private

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  
  def require_user
    unless current_user
      flash[:alert] = "You must be logged in to access this page"
      redirect_to new_user_session_url
      return false
    end
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def initialize_cart
    @cart = ShoppingCart.load(session[:shopping_cart_id])
    session[:shopping_cart_id] = @cart['id']
  end

   def get_all_categories(category_id,region_id,continent_id,country_id,report_type_id)
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    order_query = report_type_id.present? ? 'FIELD(id, '+report_type_id.to_s+') desc,name asc' : 'name asc'
    report_types = ReportType.search(:max_matches => report_type_count,:sql=>{:select=>"report_types.name,report_types.id", :order => order_query},:per_page=>report_type_count)
    @all_report_types = report_types.paginate(:page => params[:page],:per_page=>5)
    @list_of_all_report_types = report_types

    order_query = country_id.present? ? 'FIELD(id, '+country_id.to_s+') desc,name asc' : 'name asc'
    
    countries = Country.search(:sql=>{:select=>"countries.name,countries.id",:order => order_query},:per_page=>Country.search(:sql=>{:select=>"countries.id"}).total_entries)
    @all_countries = countries.paginate(:page => params[:page],:per_page=>5)
    @list_of_all_countries = countries

    order_query = continent_id.present? ? 'FIELD(id, '+continent_id.to_s+') desc,name asc' : 'name asc'
    continents = Continent.search(:sql=>{:select=>"continents.name,continents.id",:order => order_query},:per_page=>Continent.search(:sql=>{:select=>"continents.id"}).total_entries)
    @all_continents = continents.paginate(:page => params[:page],:per_page=>4)
    @list_of_all_continets = continents

    order_query = region_id.present? ? 'FIELD(id, '+region_id.to_s+') desc,name asc' : 'name asc'
    regions = Region.search(:sql=>{:select=>"regions.name,regions.id",:order => order_query},:per_page=>Region.search(:sql=>{:select=>"regions.id"}).total_entries)
    @all_regions = regions.paginate(:page => params[:page],:per_page=>5)
    @list_of_all_regions = regions

    category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
    order_query = category_id.present? ? 'FIELD(id, '+category_id.to_s+') desc,name asc' : 'name asc'
    categories = Category.search(:max_matches =>category_count ,:sql=>{:select=>"categories.name,categories.id",:order => order_query},:with=>{:root_ancestry=>0},:per_page=>category_count)
    @all_categories = categories.paginate(:page => params[:page],:per_page=>5)
    @list_of_all_categories = categories
  end

  protected

  def allow_iframe
    response.headers.delete "X-Frame-Options"
  end

  def set_global_search_variable  
    require 'will_paginate/array'
    @report_categories = Category.get_all_categories
  end

end
