class BlogsController < ApplicationController

  before_action :set_blog, only: [:index, :show, :blog_category,:by_year_and_month, :search]

  
  def index
    require 'nokogiri'
    if params[:tag]
      @blogs = Admin::Blog.get_taged_blogs(params[:tag]).paginate(:page => params[:page], :per_page => 10)
    else
      @blogs = Admin::Blog.select("id","title", "content", "user_id", "category_id","created_at","slug").paginate(:page => params[:page], :per_page => 10)
    end
  end

  def show
  	require "html_truncator"
        
    if params[:id].is_number?
      @blog = Admin::Blog.find(params[:id])
    else
      @blog = Admin::Blog.friendly.find(params[:id])  
    end  
  	
  	@blogs_comment = BlogsComment.new
  	@blog_comments = @blog.blogs_comments.order("created_at desc")
    @tags = Admin::Blog.tag_counts_on(:tags)
  end

  def post_comment
    @blog = Admin::Blog.friendly.find(params[:blog_id])
    @blogs_comment = BlogsComment.new(name: params[:name], email: params[:email], message: params[:message], blog_id: params[:blog_id], status: false)
    @blogs_comment.save
    #UserMailer.delay(run_at: Time.now).send_blog_comment(@blogs_comment)
    UserMailer.send_blog_comment(@blogs_comment).deliver
    render :json =>  true
  end
  
  def blog_comments
    blog = Admin::Blog.friendly.find(params[:blog_id])
    @blog_comments = blog.blogs_comments.order("created_at desc")
  end
  

  def blog_category
    @category = Category.friendly.find(params[:slug])
    @blogs = Admin::Blog.get_category_blogs(params[:slug]).paginate(:page => params[:page], :per_page => 10).order("created_at desc")
    @categories = Category.where(ancestry: nil)
    render :index
  end

  def search
    index
    render :index
  end

  def by_year_and_month
    @blogs = Admin::Blog.where("YEAR(created_at) = ? AND MONTH(created_at) = ? ", params[:year], params[:month]).paginate(:page => params[:page], :per_page => 10).order("created_at DESC")

  end

  def allow
    blog_comment = BlogsComment.unscoped.find(params[:blog_comment_id])
    blog_comment.update_columns(status: true)
    flash[:success] = "Blog comment is posted"
    redirect_to blogs_path
  end


  def disallow
    blog_comment = BlogsComment.unscoped.find(params[:blog_comment_id])
    blog_comment.destroy
    flash[:success] = "Blog comment is deleted"
    redirect_to blogs_path
  end


  private

  def set_blog
    @blogs_popular = Admin::Blog.limit(10).order("created_at desc")
    @blogs_by_month = Admin::Blog.get_blog_archive 
    @popular_tags = Admin::Blog.tag_counts_on(:tags).limit(10)
    @blog_rss_feeds = Admin::BlogRssFeed.all
  end

end
