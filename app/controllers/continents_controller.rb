class ContinentsController < ApplicationController

  def index
  end

  def show
    @continent = Continent.friendly.find(params[:id])
    @categories = Category.all
    get_reports_list
    respond_to do |format|
      format.html
      format.js
    end
  end

  def get_reports_list
   all_countries = Country.search(:sql=>{:select=>"countries.id,countries.name"},:with => {:continent_id => @continent.id},:per_page=>Country.search(:sql => {:select => "countries.id"}).count)
   @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},
     :with =>  {:upcoming => 0,:country_id => all_countries.map(&:id)},
     :order => "publish_date desc,one_user desc", :page => params[:page], :per_page => 10)
    
    #get_all_categories(nil,nil,@continent.id,nil,nil)
    require 'will_paginate/array'
    @all_countries = all_countries.paginate(:per_page=>5)
    @selected_continent_id = [@continent.id]
    @contain_global = @reports.select{|country_id| country_id!=""}.include?("Global")
  end

  def get_global_reports_list
   @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},
    :conditions => {:country_name => "Global"},
    :with=>{:upcoming => 0},
    :order => "publish_date desc,one_user desc", :page => params[:page], :per_page => 10)   
    #get_all_categories(nil,nil,nil,nil,nil)
    require 'will_paginate/array'
    @all_countries = []
    @list_of_all_countries = []
    @selected_continent_id = ["Global"]
    @contain_global = true
  end

  def global
    require 'will_paginate/array'
    @categories = Category.where(parent_id: nil)
    @reports = Report.cached_global.paginate(:page => params[:page], :per_page => 10)
    get_global_reports_list
    respond_to do |format|
      format.html
      format.js
    end
  end

  def reset_filter_continent
    @continent = Continent.friendly.find(params[:id])
    session[:filterrific_students] = nil
    redirect_to continent_path(@continent.slug)
  end
end
