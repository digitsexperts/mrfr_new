class CountriesController < ApplicationController

  def index
  	@grouped = Country.grouped_by_letters
  end

  # def show
  # 	@country = Country.friendly.find(params[:id])
  #   @continents = Continent.select(:id,:name,:reports_count,:slug)
  #   @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage, report_types.name as report_type_name,reports.report_type_id,reports.category_id"},:with => {:country_id => @country.id},:page => params[:page], :per_page => 10)
  #   @grouped = {}
  #   @categories = Category.all
  #   Country.all.each do |country|
  #     letter = country.name.slice(0,1).upcase
  #     @grouped[letter] ||= []
  #     @grouped[letter] << country
  #   end
  # end

  def show
    @country = Country.friendly.find(params[:id])
    @categories = Category.all
    get_reports_list
    respond_to do |format|
      format.html
      format.js
    end
  end

  def get_reports_list
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage, report_types.name as report_type_name,reports.report_type_id,reports.category_id"},
     :with =>  {:upcoming => 0,:country_id => @country.id}, 
     :order => "publish_date desc,one_user desc", :page => params[:page], :per_page => 10)
    #get_all_categories(nil,nil,nil,@country.id,nil)
    @selected_country_id = [@country.id]
    @contain_global = @reports.select{|country_id| country_id!=""}.include?("Global")
  end

  def reset_to_default_by_category
    @category = Category.friendly.find(params[:id])
    get_reports_list
    respond_to do |format|
      format.js
    end
  end

  def reset_filter_country
    country = Country.friendly.find(params[:id])
    redirect_to country_path(country.abbreviation.downcase)
  end
end