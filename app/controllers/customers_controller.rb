class CustomersController < ApplicationController

  

  def new
    @customer = Customer.new
    @countries = Country.all
  end

  def create
    # json_data = params[:customer].merge(current_user_email: current_user.email).to_json
    # uri = URI.parse("http://52.27.11.233:81/")
    # http = Net::HTTP.new(uri.host, uri.port)
    # request = Net::HTTP::Post.new("/CRM/v1/UpdateLead")
    # request.add_field('Content-Type', 'application/json')
    # request.body = json_data
    # response = http.request(request)

    @customer = Customer.new(user_params)
    if @customer.save
      flash[:notice] = "Your account has been created."
      redirect_to root_path
    else
      flash[:notice] = "There was a problem creating you."
      render :new
    end

  end

  def edit
    
     @customer = Customer.find_by_user_id(params[:id])
     @countries = Country.all
  end

  def update
    if @customer.update_attributes(user_params)
      flash[:notice] = "News saved successfully"
      redirect_to user_url(current_user.id)
    else
      render :new
    end
  end
  
  private

  def user_params
    params.require(:customer).permit(:company_name,:job_title, :address_street1, :address_street2, :city, :zip_code, :phone_no,:country_id,:user_id)
  end
end
