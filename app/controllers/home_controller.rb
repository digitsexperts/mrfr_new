class HomeController < ApplicationController

  def new_report_enquiry 
    mail_options = {:name=>"#{params[:first_name]} #{params[:last_name]}",:email=>params[:email],:phone_no=>params[:phone_no],:enquiry=>params[:enquiry]}
    # UserMailer.delay(run_at: Time.now).new_report_request(mail_options)
    UserMailer.new_report_request(mail_options).deliver
    render :json =>{ success: true}
  end

  def index
    @cooked_reports = ReportType.where(name: "Cooked Research Reports").first.reports.order("publish_date DESC").limit(3)
    @half_cooked_reports = ReportType.where(name: "Half-Cooked Research Reports").first.reports.order("publish_date DESC").limit(3)
    @raw_reports = ReportType.where(name: "Raw Research Reports").first.reports.order("publish_date DESC").limit(3)
    @continuous_reports = ReportType.where(name: "Continuous-Feed Research").first.reports.order("publish_date DESC").limit(3)
    
  end

  def show_side_panel_of_category
    @category = Category.friendly.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def get_rss_feed_of_upcoming_reports
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:upcoming=>0},:order=>"publish_date desc")
    respond_to do |format|
      format.xml {render :layout=>false}
    end  
  end

  def get_top_selling_reports
    report_type_ids = ReportType.search(:per_page=>ReportType.search.total_entries).map(&:id)
    report_ids = []
    report_type_ids.each do |report_type_id|
      report_ids << Report.search(:sql=>{:select=>"reports.id"},:with=>{:report_type_id=> report_type_id},:order=>"publish_date desc",:per_page=>1).map(&:id)
    end
    report_ids = report_ids.compact.flatten rescue []
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:report_id=>report_ids},:order=>"publish_date desc",:per_page=>report_ids.length)
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end      
  end

  def get_rss_feed_of_popular_categories
    category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
    root_category = Category.search(:sql => {:select => "id" }, :with => {:root_ancestry => 0},:per_page=>category_count).map(&:id)
    category_ids = Category.get_category_and_subcategory(root_category)
    category_ids = category_ids.flatten.compact.uniq rescue []
    report_ids = []
    category_ids.each do |category_id|
      report_ids << Report.search(:sql=>{:select=>"reports.id"},:with=>{:category_id=> category_id},:order=>"publish_date desc",:per_page=>2).map(&:id)
    end
    report_ids = report_ids.compact.flatten rescue []
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:report_id=>report_ids},:order=>"publish_date desc",:per_page=>report_ids.length)
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end       
  end

  def get_latest_reports
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with => {:upcoming => 0}, :order => "publish_date DESC", :limit => 20).to_a
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end      
  end

  def get_rss_feed_of_news_and_update
    @news_updates = NewsUpdate.order("date desc").limit(20)
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end     
  end

  def rss_feeds
    @grouped = {}
    ReportType.all.each do |report_type|
      letter = report_type.name.slice(0,1).upcase rescue nil
      @grouped[letter] ||= []
      @grouped[letter] << report_type
    end
    @grouped_country = {}
    Country.all.each do |country|
      letter = country.name.slice(0,1).upcase
      @grouped_country[letter] ||= []
      @grouped_country[letter] << country
    end
    @continents = Continent.all
    @regions = Region.select("id","name","slug")
  end

  def get_rss_feed_of_regions
    all_countries = Country.search(:sql=>{:select=>"regions.id"},:with => {:region_ids => params[:id]},:per_page => 300).map(&:id)
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:country_id=>all_countries},:order=>"publish_date desc")
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end    
  end

  def get_rss_feed_of_category
    ids = Category.get_category_and_subcategory([params[:id]])
    ids = ids.flatten.compact rescue []
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:category_id=>ids},:order=>"publish_date desc")
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end
  end

  def get_rss_feed_of_report_type
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:report_type_id=>params[:id]},:order=>"publish_date desc")
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end    
  end

  def get_rss_feed_of_country
    @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:country_id=>params[:id]},:order=>"publish_date desc")
    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end    
  end

  def get_rss_feed_of_continents
    if params[:id]!="global" 
      all_countries = Country.search(:sql=>{:select=>"countries.id"},:with => {:continent_id => params[:id]},:per_page=>Country.search(:sql => {:select => "countries.id"}).count).map(&:id)
      @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:with=>{:country_id=>all_countries},:order=>"publish_date desc")
    else
      @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.category_id,reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"},:conditions=>{:country_name=>"Global"},:order=>"publish_date desc")
    end

    respond_to do |format|
      format.rss {render :layout=>false}
      format.xml {render :layout=>false}
    end
  end

  def verifyforzoho
    render :layout => false
  end

  def robots                                                                                                                                      
    robots = File.read(Rails.root + "config/robots.#{Rails.env}.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

  def sitemap                                                                                                                                      
    sitemap = File.read(Rails.root + "public/sitemap.xml")
    render :text => sitemap, :layout => false
  end

  def urllist
    urllist = File.read(Rails.root + "public/urllist.txt")
    render :text => urllist, :layout => false, :content_type => "text/plain"
  end

  def resubscribe
    subscriber = Subscriber.find(params[:id])
    subscriber.update_columns(:status => true)
    flash[:success] =  "Thank you for resubscribing again"

    redirect_to root_path
  end

  def site_map
  end

  def block_invitation    
  end

  def unsubscribe_page
  end

  def unsubscribe
    subscriber = Subscriber.find(params[:subscriber_id])
    if params[:reason] == "Other"
      subscriber.update_columns(:reason => params[:other_reason],:status => false)
    else
      subscriber.update_columns(:reason => params[:reason],:status => false)
    end
    UserMailer.unsubscription_email_to_team(subscriber).deliver
    
    redirect_to unsubscribe_page_path(:id => subscriber.id )
  end
end
