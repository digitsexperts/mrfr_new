class MessagesController < ApplicationController

 
  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    if current_user.nil?
       @message.user_id = params[:message][:user_id]
    else
       @message.user_id = current_user.id
    end
    @message.save!
 
    @path = conversation_path(@conversation)
  end

  def save_chat_user
    # UserMailer.delay(run_at: Time.now).send_chat_report(params)
    first_name,last_name = params["name"].split(" ")[0],params["name"].split(" ")[1]
    enquiry = Enquiry.create({first_name:first_name,last_name:last_name,email:params["email"],message:params["message"],phone_no:params["phone_no"],enquiry_type: "chat_with_us"})
    UserMailer.send_chat_report(params).deliver
    render :json=>{:status=>200, :name => params[:name]}
  end



  private
 
  def message_params
    params.require(:message).permit(:body)
  end

end
