class NewsUpdatesController < ApplicationController
  #before_action :set_search, only: [:index, :show]

  def show
  	if params[:id].is_number?
      @news_update = NewsUpdate.find(params[:id])
    else
      @news_update = NewsUpdate.friendly.find(params[:id])  
    end  
  	

  	@related_report = Report.get_news_related_reports(@news_update.id)
  end
  
  def index
  	@news_updates = NewsUpdate.select("id","news_image","title","date","slug","description").paginate(:page => params[:page], :per_page => 10)
  end

end
