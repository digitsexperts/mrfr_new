class OrdersController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper

  # before_action :set_search, only: [:index,:order_details]

  def index
    @orders = current_user.orders.all
  end

  def complete_order
    render :json => {success: true}
  end

   
  def complete_ccavenue_order
    
  end

  def express_checkout
    reports_to_be_emailed = Report.search("@(id) #{@cart["shopping_cart_items"].map{|cart_item| cart_item['item']['id'] }.join(' | ')}") 
    subject = "final_step"
    # UserMailer.delay(run_at: Time.now).user_login_for_buying_reports(current_user, @cart,subject)
    UserMailer.user_login_for_buying_reports(current_user, @cart,subject).deliver
    items=[]
    per_discount = session["discount"].to_f / @cart["shopping_cart_items"].count
    @cart['shopping_cart_items'].each do |cart_item|
      items_hash = {}
      items_hash[:name] = cart_item['item']['title']
      items_hash[:amount] = get_currency_price(@cart['currency'],cart_item['discounted_price'], cart_item['price'],per_discount)
      items_hash[:quantity] = cart_item['quantity']
      items << items_hash
    end
    amount = Float((get_rate_after_discount(@cart,session["discount"])).gsub(',',''))
    response = EXPRESS_GATEWAY.setup_purchase(items.map{|items_hash| items_hash[:amount]*items_hash[:quantity]}.sum,
      ip: request.remote_ip,
      return_url: purchase_complete_url,
      cancel_return_url: paypal_order_cancel_url,
      currency: @cart['currency'],
      amount: amount,
      allow_guest_checkout: true,
      items:  items.flatten.compact
    )
    render :json => {:redirect_url => EXPRESS_GATEWAY.redirect_url_for(response.token)}
  end


  def pay_checkout
    report = Report.search("@(id) '#{params[:report_id]}'").first
    subject = "final_step"
    #UserMailer.delay(run_at: Time.now).user_login_for_buying_reports(current_user, @cart,subject)
    UserMailer.buynow_purchasing(current_user, params[:report_id],params[:edition],params[:price]).deliver
    items=[]
      items_hash = {}
      items_hash[:name] = report.title
      items_hash[:amount] = params[:price].to_f*100
      items_hash[:quantity] = 1
      items << items_hash

    
    response = EXPRESS_GATEWAY.setup_purchase(items.map{|items_hash| items_hash[:amount]*items_hash[:quantity]}.sum,
      ip: request.remote_ip,
      return_url: checkout_complete_url,
      cancel_return_url: buynow_paypal_cancel_url,
      currency: params[:edition].split(//).last(3).join,
      amount: params[:price].to_f*100,
      allow_guest_checkout: true,
      items:  items.flatten.compact
    )
    render :json => {:redirect_url => EXPRESS_GATEWAY.redirect_url_for(response.token)}
  end

  def new
    @order = Order.new
  end

  def ccavenue_transfer
    user_session = UserSession.new(email: params[:sendto], password: params[:password])
    user_session.save
    user = User.find_by_email(params[:sendto])
    subject = "final_step"
    #UserMailer.delay(run_at: Time.now).user_login_for_buying_reports(user, @cart,subject)
    UserMailer.user_login_for_buying_reports(user, @cart,subject).deliver
    render :json =>  user.to_json(:include => [:customer => {:include => [:country]}])
  end

  def buynow_ccavenue_transfer

    email = params[:existing_email].present? ? params[:existing_email] : params[:email]
    password = params[:existing_password].present? ? params[:existing_password] : params[:password]
    user_session = UserSession.new(email: email, password: password)
    user_session.save
    user = User.find_by_email(email)
    subject = "final_step"
    #UserMailer.delay(run_at: Time.now).user_login_for_buying_reports(user, @cart,subject)
    UserMailer.buynow_purchasing(current_user, params[:report_id],params[:edition],params[:price]).deliver
    render :json => JSON::parse(user.to_json(:include => [:customer => {:include => [:country]}])).merge("report_id" => params[:report_id], "price" => params[:price],"currency"=> params[:edition].split(//).last(3).join).to_json 

  end

  def wire_transfer
    ip_address = request.remote_ip
    payment_method_type = "wire_transfer"
    @order_id = Time.now.strftime('%d%m%H%L') + "1"
    if params[:report_id].present?
      UserMailer.buynow_wire_transfer_request(current_user, params[:report_id],params[:edition],params[:price]).deliver
      begin
        Order.create_order(ip_address,current_user,payment_method_type,@order_id,params[:report_id],params[:price],params[:edition])
      rescue
        puts "Error communicating with crm..."
      end
      session[:product_id] = nil
      session[:price] = nil
      session[:edition_hash] = nil
    else
      cart = ShoppingCart.find(@cart["id"])
      #UserMailer.delay(run_at: Time.now).wire_transfer_request(current_user, @cart)
      UserMailer.wire_transfer_request(current_user, @cart).deliver
      begin
        cart.create_order(ip_address,current_user,payment_method_type,@order_id)
      rescue
        puts "Error communicating with crm..."
      end
      cart.clear
      Rails.cache.delete("user-shopping-cart-#{@cart['id']}")
    end
    render :json => {:redirect_url => wiretransfer_response_path }
  end


  def new_user_ccavenue_transfer
    user = User.new(:first_name=>params[:fname],:last_name=>params[:lname], :email=>params[:email], :password=>params[:password], :password_confirmation=>params[:password])
    if user.save
      customer = Customer.new( :company_name=>params[:company_name], :job_title=>params[:job_title], :address_street1=>params[:address_street1], :address_street2=>params[:address_street2], :zip_code=>params[:zip_code], :phone_no=>params[:phone_no], :city=>params[:city], :country_id=>params[:country_id],:user_id=>user.id)
      customer.save
      reports_to_be_emailed = Report.where(id: @cart.shopping_cart_items.pluck(:item_id)) 
      subject = "final_step"
      #UserMailer.delay(run_at: Time.now).user_login_for_buying_reports(user, reports_to_be_emailed, @cart,subject)
      UserMailer.user_login_for_buying_reports(user, reports_to_be_emailed, @cart,subject).deliver
      render :json =>  user.to_json(:include => [:customer => {:include => [:country]}])
    else
      render :json =>  {:error => user.errors.full_messages }
    end
  end

  def get_currency_price(currency,discounted_price,price,per_discount=0)
    if ["GBP", "EUR", "JPY", "INR"].include?(currency)
      if discounted_price.present?  
        final_price = discounted_price*100
      else
        final_price = (price-per_discount)*get_exchange_rate(currency)*100
      end
    else
      if discounted_price.present?  
        discounted_price*100
      else
        (price-per_discount)*100
      end
    end
  end
    
  def get_exchange_rate(currency)
    case currency
    when "GBP"
      gbp_exchange_rate = Rate.get_exchange_rate("GBP")
    when "EUR"
      eur_exchange_rate = Rate.get_exchange_rate("EUR")
    when "JPY"
      yen_exchange_rate = Rate.get_exchange_rate("JPY")
    when "INR"
      inr_exchange_rate = Rate.get_exchange_rate("INR")
    end
  end

  def purchase_complete
    cart = ShoppingCart.find(@cart["id"])
    ip_address = request.remote_ip
    payment_method_type = "paypal"
    @order_id = Time.now.strftime('%d%m%H%L') + "1"
    order = cart.create_order(ip_address,current_user,payment_method_type,@order_id)
    #UserMailer.delay(run_at: Time.now).reports_purchased(current_user,@cart)
    UserMailer.reports_purchased(current_user, @cart).deliver
    cart.clear
    Rails.cache.delete("user-shopping-cart-#{@cart['id']}")
    session["discount"] == nil
    redirect_to paypal_purchase_complete_path
    return
  end


  def checkout_complete
    ip_address = request.remote_ip
    payment_method_type = "paypal"
    @order_id = Time.now.strftime('%d%m%H%L') + "1"
    order = Order.create_order(ip_address,current_user,payment_method_type,@order_id,session[:product_id],session[:price],session[:edition_hash])
    UserMailer.buyone_reports_purchased(current_user,session[:product_id],session[:edition_hash],session[:price]).deliver
    session[:product_id] = nil
    session[:price] = nil
    session[:edition_hash] = nil
    redirect_to paypal_purchase_complete_path
    return 
  end

  def order_details
    @order_details = OrderDetail.where(order_id: params[:order_id])
  end
  
  def paypal_purchase_complete
  end


  private

  def set_search
    @search = Report.search_report(params[:q])
  end

end
