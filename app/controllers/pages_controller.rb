class PagesController < ApplicationController
  def show
  	 @page = Admin::Page.show(params[:id])
  end

  def contact_us
     @countries = Country.get_all_countries
  end

  def save_contact_us
  	@contact_us = ContactUs.new(contact_us_params)
    if @contact_us.save
      # contact_attributes = @contact_us.attributes.select do |attr, value|
      #   ContactUs.column_names.include?(attr.to_s)
      # end
      # json_data = contact_attributes.merge(:enquiry_type => "contact_us_form",:first_name => @contact_us.name, :enquiry=> @contact_us.message).to_json
      # uri = URI.parse("http://52.27.11.233:81/")
      # http = Net::HTTP.new(uri.host, uri.port)
      # request = Net::HTTP::Post.new("/CRM/v1/CreateLead")
      # request.add_field('Content-Type', 'application/json')
      # request.body = json_data
      # response = http.request(request)
      

      #UserMailer.delay(run_at: Time.now).send_message(@contact_us)
      UserMailer.send_message(@contact_us).deliver
      success = "Thank You! Wise Guy will get back to you soon."
      render :json => {notice: success} 
    else
      render :json => {error: "Please enter an correct capcha"} 
    end
  end

  def about_us
  end
  def report_type
  end
  def faqs
  end
  def type1
  end
  def type2
  end
  def type3
  end
  def type4
  end

  def vender_info
  end
  
  private

  def contact_us_params
    params.require(:contact_us).permit(:name,:email,:message,:job_title,:company,:phone_no,:country_id)
  end
end
