class PublishersController < ApplicationController

  def index
  	@grouped = {}
	  Publisher.publisher_list.each do |publisher|
	    letter = publisher.name.slice(0,1).upcase
	    @grouped[letter] ||= []
	    @grouped[letter] << publisher
    end
  end

  def show
    @publisher = Publisher.friendly.find(params[:id])
    @topselling_reports = Report.get_topselling_publisher_reports(@publisher.id)
    @publisher_reports = Report.get_publisher_reports(@publisher.id,params[:page])
  end	

end
