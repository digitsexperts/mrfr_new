class RegionsController < ApplicationController
  def index
    @regions = Region.all_regions.select("id","name","slug","reports_count")
  end

  def show
   @region = Region.friendly.find(params[:id])
   country_ids = @region.countries.pluck(:id)
   @categories = Category.all
   @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},:page => params[:page], :per_page => 20)
   @categories = Category.where(parent_id: nil)
   get_reports_list
   respond_to do |format|
      format.html
      format.js
   end
  end

  def get_reports_list
    all_countries = Country.search(:with => {:region_ids => @region.id},:per_page => 300)
     @reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},
     :with =>  {:upcoming => 0,:country_id => all_countries.map(&:id)},:order => "publish_date desc,one_user desc", :page => params[:page], :per_page => 10) 
    #get_all_categories(nil,@region.id,nil,nil,nil)
    @all_countries = all_countries
    @selected_region_id = [@region.id]
    @contain_global = @reports.select{|country_id| country_id!=""}.include?("Global")
  end
end
