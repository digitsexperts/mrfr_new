class ReportFiltersController < ApplicationController

  def show_all_report_types
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    @all_report_types = ReportType.search(:max_matches => report_type_count, :order=>"name asc",:sql=>{:select=>"report_types.id,report_types.name"},:per_page=>report_type_count)
    @selected_report_types_id = params["selected_report_types"].map(&:to_i) rescue []
    respond_to do |format|
    	format.js
    end
  end

  def show_default_report_types
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    @all_report_types = ReportType.search(:max_matches => report_type_count,:sql=>{:select=>"report_types.id,report_types.name"},:per_page=>5)
    @selected_report_types_id = params["selected_report_types"].map(&:to_i) rescue []
    respond_to do |format|
    	format.js
    end  	
  end

  def show_filter_reports
    @reports  = Report.filter_reports(params)
    respond_to do |format|
    	format.js
    end  	
  end

  def get_all_report_types
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    render :json=> ReportType.search(:max_matches => report_type_count, :sql=>{:select=>"report_types.id,report_types.name"},:order=>"name asc", :per_page => report_type_count)
  end

  def show_all_countries
    if params["selected_regions"].present?
      @all_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:with => {:region_ids => params[:selected_regions]},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries)
      @selected_countries_id = params["selected_countries"].map(&:to_i) rescue []
    elsif params["selected_continents"].present?
      @all_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:with => {:continent_id => params[:selected_continents]},:per_page=>Country.search(:sql=>{:select=>"countries.id"}).total_entries) 
      @selected_countries_id = params["selected_countries"].map(&:to_i) rescue []
    else
      @all_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries)
      @selected_countries_id = params["selected_countries"].map(&:to_i) rescue []
    end
    @selected_countries_id = [] if @selected_countries_id.nil? 
    @count = @all_countries.length
    @all_countries = JSON.parse(@all_countries.to_json)
    # @list_of_all_countries = @all_countries

    respond_to do |format|
      format.js
    end
  end

  def show_all_categories
    category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
    @all_categories = Category.search(:max_matches => category_count, :order=>"category_name asc",:sql=>{:select=>"categories.id,categories.name"},:with=>{:root_ancestry=>0},:order=>"category_name asc", :per_page => category_count)
    @selected_category_id = params["selected_categories"].map(&:to_i) rescue []
    @selected_category_id= [] if @selected_category_id.nil? 
    respond_to do |format|
      format.js
    end    
  end

  def show_all_continents
    @all_continents = Continent.search(:order=>"continent_name asc",:sql=>{:select=>"continents.id,continents.name"},:order=>"continent_name asc", :per_page => Continent.search(:sql=>{:select=>"continents.id"}).total_entries)
    @selected_continent_id = params["selected_continents"] rescue []
    @selected_continent_id= [] if @selected_continent_id.nil? 
    @list_of_all_continets = @all_continents
    respond_to do |format|
      format.js
    end
  end

  def show_all_regions
    @all_regions = Region.search(:order=>"region_name asc",:sql=>{:select=>"regions.id,regions.name"},:order=>"region_name asc", :per_page => Region.search(:sql=>{:select=>"regions.id"}).total_entries)
    @selected_regions_id = params["selected_regions"].map(&:to_i) rescue []
    @selected_regions_id= [] if @selected_regions_id.nil? 
    @list_of_all_regions = @all_regions
    respond_to do |format|
      format.js
    end
  end  

  def sort_report_types
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    report_types = ReportType.search(:max_matches => report_type_count,:name=>params[:name],:sql=>{:select=>"report_types.id,report_types.name"},:per_page=>5)
    render :json=> report_types.to_json
  end

###### For SelectingCountry Based On Regions #######

  def show_selected_regions
    
    require 'will_paginate/array'
    if params[:selected_regions].present?
      # @all_selected_countries = Region.joins(:countries).where("countries_regions.region_id in (?)",params[:selected_regions]).select()
      @all_selected_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:with => {:region_ids => params[:selected_regions]},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries) rescue []
      @default_selected_countries = @all_selected_countries.paginate(:per_page=>5) rescue []
    else
      @all_selected_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries) rescue []
      @default_selected_countries = @all_selected_countries.paginate(:per_page=>5) rescue []
    end
    respond_to do |format|
      format.js
    end
  end

###### For SelectingCountry Based On Continents #######

  def show_selected_continents
    require 'will_paginate/array'
    if params[:selected_continents].present?
      @all_continents_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:with => {:continent_id => params[:selected_continents]},:per_page=>Country.search(:sql=>{:select=>"countries.id"}).total_entries) rescue []
      @selected_continent_countries = @all_continents_countries.paginate(:per_page=>5) rescue []
    else
      @all_continents_countries = Country.search(:order=>"country_name asc",:sql=>{:select=>"countries.id,countries.name"},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries) rescue []
      @selected_continent_countries = @all_continents_countries.paginate(:per_page=>5) rescue []
    end
     respond_to do |format|
      format.js
    end
  end
end
