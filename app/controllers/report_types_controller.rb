class ReportTypesController < ApplicationController
  def index
  	@report_types = ReportType.order("created_at DESC")
  end

  def show
  	@report_type = ReportType.friendly.find(params[:id])
  	@related_reports = Report.search("@report_report_type_name '#{@report_type.name}'", limit: 5, order: 'publish_date DESC')

  end

  def report_show
  	@report_type = ReportType.friendly.find(params[:id])
  	@reports = Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},:with=>{report_type_id: @report_type.id,:upcoming => 0},:order=>"publish_date desc,one_user desc",:page => params[:page], :per_page => 10)
    @categories = Category.all
    @selected_report_type_id = [@report_type.id]
    respond_to do |format|
      format.html
      format.js
    end
  end
end
