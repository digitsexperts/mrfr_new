class ReportsController < ApplicationController
  before_action :get_form_ids, only: [:sample_request, :check_discount,:any_question,:request_toc]
  require 'RMagick'
  include Magick
  include ActionView::Helpers::TextHelper
  def index
    @reports = Report.search_report(params,params[:page])
    @categories = Category.all
  end

  def search
    fields = if (params["term"].present? && params["report_type"].present?)
               "@(title,report_type_name) '#{params['term']}','#{params['report_type']}'"
             elsif (params["term"].present? && params["report_type"].blank?)
               "@(title) '#{params['term']}'"
             elsif (params["term"].blank? && params["report_type"].present?)
               "@(report_type_name) '#{params['report_type']}'"
             else
               nil
             end

    filter = {
              :order => "#{params[:sorted_by]}", 
              :page => (request.format.symbol == :js) ? nil : params[:page], 
              :limit => 10
             }

    filter[:with] = {:one_user => eval(params[:price])} if params[:price].present?
    @reports = Report.search(fields,filter)
    #get_all_categories(nil,nil,nil,nil,nil)
    
    respond_to do |format|
      format.html {
        @categories = Category.get_all_categories
        render :template => "reports/index"
      }
      format.js { render(partial: 'reports/list') }
    end
  end


  def show
    
    if params[:id].is_number?
      @report = Report.find(params[:id])
    else
      reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
      reports.each do |report|
        @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
      end
      @report = reports.first unless @report  
    end  

    @enquiry = Enquiry.new
    
    @related_report = Report.related_report(@report.category_id)
    @rates = @report.rates
    require 'rubygems'
    require 'engtagger'
    tgr = EngTagger.new
    text = @report.title
    tagged = tgr.add_tags(text)
    @tags = tgr.get_proper_nouns(tagged).keys
    respond_to do |format|
      format.html
      format.js
      format.json { render json: @report.to_json }
    end
  end


def enquiry
    @enquiry = Enquiry.new(enquiry_params)
    if @enquiry.save
      report = Report.find(@enquiry.report_id)
      # enquiry_attributes = @enquiry.attributes.select do |attr, value|
      #   Enquiry.column_names.include?(attr.to_s)
      # end
      # report_attributes = report.attributes.select do |attr, value|
      #   Report.column_names.include?(attr.to_s)
      # end
      # json_data = enquiry_attributes.merge(report_attributes).merge(:enquiry_country_id => @enquiry.country_id).to_json
      # uri = URI.parse("http://52.27.11.233:81/")
      # http = Net::HTTP.new(uri.host, uri.port)
      # request = Net::HTTP::Post.new("/CRM/v1/CreateLead")
      # request.add_field('Content-Type', 'application/json')
      # request.body = json_data
      # response = http.request(request)

      
      success = "Thank you, Your enquiry has been send"

      #UserMailer.delay(run_at: Time.now).send_enquiry(report, @enquiry)
      UserMailer.send_enquiry(report, @enquiry).deliver
      render :json => {notice: success} 
    else

      render :json => {error: "Please enter an correct capcha"} 

    end
  end



  def email_report
    #UserMailer.delay(run_at: Time.now).email_reports(params[:email_report])
   # UserMailer.delay(run_at: Time.now).email_report_to_team(params[:email_report])
    UserMailer.email_reports(params[:email_report]).deliver
    UserMailer.email_report_to_team(params[:email_report]).deliver
    notice = "Thank you, Your message has been sent"
    render :json => {notice: notice}
    
  end

  def sample_request

  end

  def check_discount

  end

  def any_question

  end

  def request_toc
  end

  def printer_friendly
    @enquiry = Enquiry.new
    if params[:id].is_number?
      @report = Report.find(params[:id])
    else
      reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
      reports.each do |report|
        @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
       end
      @report = reports.first unless @report
    end
    @countries = Country.get_all_countries
    
    render :layout => false   
  end
  
  def subscriber
    subscriber = Subscriber.search("@(email) #{Riddle::Query.escape(params[:subscriber][:email])}")
    if subscriber.blank?
      Subscriber.create(email: params[:subscriber][:email], status: true)
      UserMailer.new_subscription_to_team(params[:subscriber][:email]).deliver
      flash[:success] = "Thank you for subscribing to Market Research Future Newsletter. We are delighted to have you !! "
      redirect_to root_path
    elsif subscriber.first.status
      flash[:success] = "You are aleady subscribed to Market Research Future Newsletter."
      redirect_to root_path
    else
      subscriber.first.update_attributes(status: true)
      UserMailer.new_subscription_to_team(params[:subscriber][:email]).deliver
      flash[:success] = "Thank you for subscribing to Market Research Future Newsletter. We are delighted to have you !! "
      redirect_to root_path
    end
  end

  def rebuild_cache
    User.build_cache
    Report.clear_fragment_cache

    `rake cache_builder:create_fragment_cache`
    render :nothing => true
  end

  def order_form
    @report = Report.search("@(id) '#{params[:report_id]}'").first
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "order_form", encoding: 'utf8'
      end
    end
  end

  def report_info
    @report = Report.search("@(id) '#{params[:report_id]}'").first
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "report_info", encoding: 'utf8'
      end
    end
  end
  

  private

  def subscriber_params
    params.require(:subscriber).permit(:email,:status)
  end

  def enquiry_params
    params.require(:enquiry).permit(:first_name,:last_name,:email,:job_title, :phone_no, :company, :interest_area, :enquiry, :message,:enquiry_type,:report_id,:country_id)
  end


  def get_form_ids
    @enquiry = Enquiry.new
    if params[:id].is_number?
      @report = Report.find(params[:id])
    else
      reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
      reports.each do |report|
        @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
       end
      @report = reports.first unless @report
    end
    @countries = Country.get_all_countries
  end
end
