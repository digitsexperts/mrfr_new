class ShoppingCartsController < ApplicationController
  # before_filter :extract_shopping_cart
include ActionView::Helpers::NumberHelper
before_filter :require_user, :except=>[:create,:show,:remove_item,:clear_cart,:add_to_cart,:load_add_to_cart,:update_quantity,:create_user,:signin_user,
  :checkout,:ccavResponseHandler,:cancel_transaction,:ccavRequestHandler,:buynowCcavenueResponse,:buynowccavRequestHandler, :wiretransfer,:get_promo_code_specific,:place_order,:load_place_order_items, :hdfc_secret,:get_promo_code]

  def create
    product = Report.search("@(id) '#{params[:report_id]}'").first
    redirect_to checkout_path(:report_id => product.id, :currency => params[:currency])
  end
 

  def add_to_cart
    product = Report.search("@(id) '#{params[:report_id]}'").first
    cart = ShoppingCart.find(@cart["id"])
    cart.add(product, get_product_price(product, params[:currency][0..-5]),1,true, params[:currency])
    cart.update_columns(:currency => params[:currency].split(//).last(3).join)
    render :json =>  true
  end

  def load_add_to_cart
  end

  def load_place_order_items
  end

  def show
  end

  def get_product_price(product,type)
    case type
    when "one_user"
      return product.one_user
    when "five_user"
      return product.five_user  
    when "site_user"
      return product.site_user
    when "enterprise_user"
      return product.enterprise_user
    end
  end

  
  def get_product_currency(currency)
    case currency
    when "USD"
      return "USD"
    when "GBP"
      return "GBP"
    when "Euro"
      return "EUR"
    when "Yen"
      return "JPY"
    when "INR"
      return "INR"
    end
  end



  def remove_item
    cart_item = ShoppingCartItem.find(params[:item_id])
    report = Report.search("@(id) #{cart_item.item_id}").first
    cart = cart_item.owner
    if cart_item.quantity==1
      cart_item.delete
    else
      cart_item.update_attribute("quantity",(cart_item.quantity-1))
    end
    Rails.cache.delete("user-shopping-cart-#{cart.id}")
    render :json => JSON::parse(cart.to_json(:include => [:shopping_cart_items])).merge("report_slug" => report.slug).to_json
  end
    
  def clear_cart
    last_cart_item_id = @cart['shopping_cart_items'].first['item_id'] unless @cart['shopping_cart_items'].length.equal?(0)
    ShoppingCart.find(@cart["id"]).clear
    Rails.cache.delete("user-shopping-cart-#{@cart["id"]}")
    redirect_to (last_cart_item_id ? {:controller => :reports, :action => :show, :id => last_cart_item_id} : root_path)
  end
  
  def manage_address
    @countries = Country.all
    reports_to_be_emailed = Report.where(id: @cart.shopping_cart_items.pluck(:item_id)) 
    subject = "manage_address"
    UserMailer.user_login_for_buying_reports(current_user, reports_to_be_emailed, @cart,subject).deliver 
  end

  def checkout

    @countries = Country.list
    cart = ShoppingCart.find(@cart["id"])
    cart.clear
    @order_id = Time.now.strftime('%d%m%H%L')
    @shop_id =  @order_id
    @product =  Report.search("@(id) '#{params[:report_id]}'").first
    @edition = params[:currency]
    @price = get_report_price(@product, params[:currency])
    @currency =@edition.split(//).last(3).join
    session[:product_id] = @product.id
    session[:price] = @price
    session[:edition_hash] = @edition
  end

  def get_report_price(product, currency)
    price = get_product_price(product,currency.split("-").first)
    case currency.split(//).last(3).join
    when "GBP"
      price.to_f*Rate.get_exchange_rate("GBP")
       
    when "EUR"
      price.to_f*Rate.get_exchange_rate("EUR")
      
    when "JPY"
      price.to_f*Rate.get_exchange_rate("JPY")
      
    when "INR"
      price.to_f*Rate.get_exchange_rate("INR")
    else
      price.to_f
    end
  end


  def place_order
    @countries = Country.list
    if params[:promocode].present?

      discount_arr = []
      cart = ShoppingCart.find(@cart["id"])
      crypt = get_crypted
      promocode_id = crypt.decrypt_and_verify(params[:promocode])
      promo_code = PromoCode.find(promocode_id)     
      if promo_code.valid_from.to_date<=Date.today && Date.today <= promo_code.valid_to.to_date && promo_code.users_count < promo_code.uses.to_i
        cart.clear
        promo_code.promo_code_for_reports.each do |promocodeforreport|
          product = Report.search("@(id) '#{promocodeforreport.product_id.to_i}'").first
          cart.add(product, get_product_price(product, promocodeforreport.edition.split("_")[0].gsub("-","_")) ,1 , true, promocodeforreport.edition.split("_")[0].gsub("-","_") + "-" +  get_product_currency(promo_code.discount_currency))
          cart.update_columns(:currency => get_product_currency(promo_code.discount_currency))
          report = cart.shopping_cart_items.where(:item_id=>promocodeforreport.product_id).first
          if promo_code.discount_type == "Percentage Discount" 
            if promo_code.product_discount == "Particular Report Discount" 
              discount = (promocodeforreport.price.to_f*promocodeforreport.discount.to_f/100).to_f.round(2)
              report.update_columns(:discount=>discount)
              discount_arr << discount
            else
              session[:discount] = (@cart["total"]*promo_code.discount.to_f/100).to_f.round(2)
            end
          else
            if promo_code.product_discount == "Particular Report Discount" 
              discount = promocodeforreport.discount
              report.update_columns(:discount=>discount)
              discount_arr << discount
            else
              session[:discount] = promo_code.discount
            end
          end
        end
        @discount = discount_arr.empty? ? session[:discount] : discount_arr.sum
        @cart = ShoppingCart.load(cart["id"])
        promo_code.update_attributes(:users_count => (promo_code.users_count + 1))
      else
        flash[:error] = "Your promo code has been expired"
      end
    elsif params[:purchase_form].present?
      discount_arr = []
      cart = ShoppingCart.find(@cart["id"])
      crypt = get_crypted
      purchase_form_id = crypt.decrypt_and_verify(params[:purchase_form])
      purchase_form = Invoice.find(purchase_form_id)
      cart.clear
      # user = User.find_by_email(purchase_form.contact_email)
      # UserSession.create(user, true)
      purchase_form.product_details.each do |product_detail|
        
        product = Report.search("@(id) '#{product_detail.report_id}'").first
        cart.add(product, get_product_price(product, product_detail.report_edition.split("_")[0].gsub("-","_")) ,1 , true, product_detail.report_edition.split("_")[0].gsub("-","_") + "-" +  get_product_currency(purchase_form.currency))
        cart.update_columns(:currency => get_product_currency(purchase_form.currency))
        report = cart.shopping_cart_items.where(:item_id=>product_detail.report_id).first
        if product_detail.discount_type == "Percentage Discount"            
          discount = (report.price*product_detail.discount/100).to_f
          report.update_columns(:discount=>discount)
          discount_arr << discount
        else
          discount = product_detail.discount
          report.update_columns(:discount=>discount)
          discount_arr << discount
        end
      end
      @discount = discount_arr.sum
      @cart = ShoppingCart.load(cart["id"])
    # elsif params[:report_id].present?
    #   cart = ShoppingCart.find(@cart["id"])
    #   cart.clear
    #   @product = Report.search("@(id) '#{params[:report_id]}'").first

    else
      session[:shopping_cart_id] = params[:purchase_id]
      @cart = ShoppingCart.load(params[:purchase_id]) if params[:purchase_id].present?

      @order_id = Time.now.strftime('%d%m%H%L') + "1"
      if session[:discount].nil?
        inr_exchange_rate = Rate.find_by_currency("inr").exchange_rate
        @cart_in_inr = @cart['total']*inr_exchange_rate
      else
        @discount = session[:discount]
      end 
    end

    
  end

  def update_quantity
    ShoppingCartItem.find(params[:item][:shopping_cart_item]).update_columns(:quantity => params[:item][:quantity])
    redirect_to shopping_cart_path
  end

  def create_user
    @user = User.new(user_params)
    if verify_recaptcha
      if @user.save
        # flash[:success] = "Your account has been created."
        redirect_to manage_address_path
      else
        flash[:alert] = "You have been already registrated. Please login."
        redirect_to checkout_path
      end
    else
      flash[:error] = "Please enter a correct code"
      flash.delete :recaptcha_error
      redirect_to checkout_path
    end

  end

  def signin_user
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      flash[:success] = "Login successful!"
      if @user_session.user.customer.present?
        redirect_to place_order_path
      else
        redirect_to manage_address_path
      end
    else
       flash[:error] = "Please enter valid credentail"
       redirect_to checkout_path
    end
  end

  def create_customer
    @customer = Customer.new(customer_params)
    if @customer.save
      flash[:success] = "Your account has been created."
      redirect_to place_order_path
    else
      redirect_to place_order_path
    end

  end


  def ccavRequestHandler
    ShoppingCart.find(params[:shop_id]).update_attributes(:order_id => params[:order_id]) if params[:shop_id].present?
    params.delete(:shop_id)
    respond_to do |format|
      format.html {render layout: false}
    end
  end

  def cancel_transaction
    redirect_to place_order_path(:purchase_id => @cart['id'])
  end

  def ccavResponseHandler
    workingKey="757C2372B0098431D0A186EA332C42C4"    
    encResponse=params[:encResp]
    crypto = Crypto.new 
    decResp = crypto.decrypt(encResponse,workingKey)
    decResp = decResp.split("&")
   
    if decResp[3].split("=").last=="Failure"
      flash[:error] = "Transaction Failure! Please try again."
      @status = "Failure"
   
    elsif decResp[3].split("=").last=="Aborted"
      flash[:error] = "Transaction Failure! Please try again."
      @status = "Failure"
    else
      cart = ShoppingCart.find(@cart["id"])
      array = decResp.flatten
      pos = array.index{|x| x.include?("payment_mode=")}
      email_pos = array.index{|x| x.include?("billing_email=")}
      order_pos = array.index{|x| x.include?("order_id=")}
      @order_id =  array[order_pos].split("=").last
      val = array[pos].split("=").last
      email_val = array[email_pos].split("=").last
      user = User.find_by_email(email_val)
      ip_address = request.remote_ip
      payment_method_type = val
      #@cart = ShoppingCart.find_by_order_id(@order_id)
      order = cart.create_order(ip_address,user,payment_method_type,@order_id)
      #UserMailer.delay(run_at: Time.now).reports_purchased(current_user,@cart)
      UserMailer.reports_purchased(current_user,@cart).deliver
      cart.clear
      Rails.cache.delete("user-shopping-cart-#{@cart["id"]}")
      flash[:success] = "Thank you for puchasing the reports"
      @status = "Success"
      session["discount"] == nil
    end

  end
  
  def buynowCcavenueResponse
    workingKey="757C2372B0098431D0A186EA332C42C4"    
    encResponse=params[:encResp]
    crypto = Crypto.new 
    decResp = crypto.decrypt(encResponse,workingKey)
    decResp = decResp.split("&")
   
    if decResp[3].split("=").last=="Failure"
      flash[:error] = "Transaction Failure! Please try again."
      @status = "Failure"
   
    elsif decResp[3].split("=").last=="Aborted"
      flash[:error] = "Transaction Failure! Please try again."
      @status = "Failure"
    else
      # cart = ShoppingCart.find(@cart["id"])
      array = decResp.flatten
      pos = array.index{|x| x.include?("payment_mode=")}
      email_pos = array.index{|x| x.include?("billing_email=")}
      order_pos = array.index{|x| x.include?("order_id=")}
      @order_id =  array[order_pos].split("=").last
      val = array[pos].split("=").last
      email_val = array[email_pos].split("=").last
      user = User.find_by_email(email_val)
      ip_address = request.remote_ip
      payment_method_type = val
     
      Order.create_order(ip_address,current_user,payment_method_type,@order_id,session[:product_id],session[:price],session[:edition_hash])
      UserMailer.buyone_reports_purchased(current_user,session[:product_id],session[:edition_hash],session[:price]).deliver
      # cart.clear
      # Rails.cache.delete("user-shopping-cart-#{@cart.id}")
      flash[:success] = "Thank you for puchasing the reports"
      @status = "Success"
      session["discount"] = nil
      session[:product_id] = nil
      session[:price] = nil
      session[:edition_hash] = nil
    end
  end



  def wiretransfer
  end

  # def get_promo_code_specific
  #    promocodes = PromoCode.where(discount_code: params[:promo_code], email: params[:email])
     
  #   if promocodes.first.nil?
  #     flash[:error] = "Please enter valid promo code"
  #     redirect_to shopping_cart_path
  #   else
  #     promo_code = PromoCode.find(promocodes.first.id)
  #     if promocodes.first.valid_from.to_date<=Date.today && Date.today <= promocodes.first.valid_to.to_date && promocodes.first.users_count < promocodes.first.uses.to_i
  #       @cart.clear
  #       promo_code.promo_code_for_reports.each do |promocodeforreport|
  #         product = Report.friendly.find(promocodeforreport.product_id)

  #         @cart.add(product, get_product_price(product, promocodeforreport.edition.split("_")[0].gsub("-","_")) ,1 , true, promocodeforreport.edition.split("_")[0].gsub("-","_") + "-" +  get_product_currency(promo_code.discount_currency))
  #         @cart.update_columns(:currency => get_product_currency(promo_code.discount_currency))
  #         report = @cart.shopping_cart_items.where(:item_id=>promocodeforreport.product_id).first
  #         if promo_code.discount_type == "Percentage Discount" 
                     
            
  #           if promo_code.product_discount == "Particular Report Discount" 
        
  #             discount = (promocodeforreport.price.to_f*promocodeforreport.discount.to_f/100).to_f.round(2)
  #             report.update_columns(:discount=>discount)
  #           else
  #             session[:discount] = (@cart["total"]*promo_code.discount.to_f/100).to_f.round(2)
  #           end
  #         else
            
  #           if promo_code.product_discount == "Particular Report Discount" 
  #             discount = promocodeforreport.discount
  #             report.update_columns(:discount=>discount)
  #           else
  #             session[:discount] = promo_code.discount
  #           end
  #         end
  #       end
  #     else
  #       flash[:error] = "Your promo code has been expired"
  #     end
  #     promocodes.first.update_attributes(:users_count => (promocodes.first.users_count + 1))
  #     redirect_to shopping_cart_path
  #   end
  # end

  def get_promo_code

    promocodes = PromoCode.where(discount_code: params[:promo_code])
    # promocodes = PromoCode.find(:all, :params => { :promocode => params[:promo_code], :email=> current_user.email })
    if promocodes.first.nil?
      flash[:error] = "Please enter valid promo code"
      redirect_to place_order_path
    else
      if promocodes.first.valid_from.to_date<=Date.today && Date.today <= promocodes.first.valid_to.to_date && promocodes.first.users_count < promocodes.first.uses.to_i
        if promocodes.first.discount_type == "Percentage Discount"
          case promocodes.first.order_type
            when "Entire Order"
              if @cart["total"] > promocodes.first.minimum_order_amount.to_f
                session[:discount] = (@cart["total"]* promocodes.first.discount/100).to_f
              else
                flash[:error] = "Your promo code maximun discount amount is less than your total cart price"
              end
            when "Orders Over"
              if @cart["total"] < promocodes.first.maximum_discount_amount.to_f && @cart["total"] > promocodes.first.minimum_discount_amount.to_f
                session[:discount] = (@cart["total"]*promocodes.first.discount/100).to_f
              else
                flash[:error] = "Your promo code has not been in a valid amount range"
              end
            when "Specific Product"
              cart = ShoppingCart.find(@cart["id"])
              cart.clear
              promocodes.first.promo_code_for_reports.each do |promocodeforreport|
                product = Report.search("@(id) '#{promocodeforreport.product_id.to_i}'").first
                cart.add(product, get_product_price(product, promocodeforreport.edition.split("_")[0].gsub("-","_")) ,1 , true, promocodeforreport.edition.split("_")[0].gsub("-","_") + "-" +  get_product_currency(promo_code.discount_currency))
                cart.update_columns(:currency => get_product_currency(promo_code.discount_currency))
                # discount = (product.price*product_detail.discount/100).to_f
                # report.update_columns(:discount=>discount)
                # discount_arr << discount
              end
              # @cart = ShoppingCart.load(cart["id"])
          end
        else
         case promocodes.first.order_type
            when "Entire Order"
              if @cart["total"] > promocodes.first.minimum_order_amount.to_f
                session[:discount] = promocodes.first.discount
              else
                flash[:error] = "Your promo code maximun discount amount is less than your total cart price"
              end
            when "Orders Over"
              if @cart["total"] < promocodes.first.maximum_discount_amount.to_f && @cart["total"] > promocodes.first.minimum_discount_amount.to_f
                session[:discount] = promocodes.first.discount
              else
                flash[:error] = "Your promo code has not been in a valid amount range"
              end
            when "Specific Product"
              # unless @cart.shopping_cart_items.where(:item_id=> promocodes.first.report_id).blank?
              #   report = @cart.shopping_cart_items.where(:item_id=> promocodes.first.report_id).first
              #   discount = promocodes.first.discount
              #   report.update_columns(:discount=>discount)
              # end
          end
        end
      else
        flash[:error] = "Your promo code has been expired"
      end
    

     promocodes.first.update_attributes(:users_count => (promocodes.first.users_count + 1))
     redirect_to place_order_path
    end
  end

  def paypal_order_cancel
    redirect_to place_order_path(:purchase_id => @cart['id'])
  end

  def buynow_paypal_cancel
    redirect_to root_path
  end

  def hdfc_secret

    hash_data = 'a9162876ad8df7a6235582f806247bb1'; 
    #Sorting the params data in ascending order
    params.delete(:controller)
    params.delete(:action)
    
    sorted_params = params.sort_by {|_key, value| _key}
    
    sorted_params.each do |key,value|
       hash_data += ("|"+value.to_s) if value.length > 0
    end
    
    hash = Digest::SHA2.new(512).hexdigest(hash_data) 
    secure_hash = hash.upcase
    render :json => {hdfc_secret: secure_hash}
  end

  def user_detail
    render :json => {user: current_user.to_json(:methods => [:customer])}
  end

  def buynowccavRequestHandler
    # ShoppingCart.find(params[:shop_id]).update_attributes(:order_id => params[:order_id]) if params[:shop_id].present?
    # params.delete(:shop_id)
    respond_to do |format|
      format.html {render layout: false}
    end
  end

  private

  def customer_params
    params.require(:customer).permit(:company_name,:job_title, :address_street1, :address_street2, :city, :zip_code, :phone_no,:country_id,:user_id)
  end

  def user_params
    params.require(:user).permit(:first_name,:last_name, :email, :password, :password_confirmation, :password_reset_token, :password_reset_sent_at)
  end

end
