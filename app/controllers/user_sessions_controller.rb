class UserSessionsController < ApplicationController
  before_filter :require_user, :only => :destroy

  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      flash[:success] = "Login successful!"
      if (@user_session.user.is_admin? || @user_session.user.is_superadmin? || @user_session.user.is_blog_admin?)
        redirect_back_or_default "/admin/dashboard" 
      else
        redirect_to root_url
      end
    else
      flash[:error] = "Your login information is invalid"
      render :new
    end
  end

  def destroy
    current_user_session.destroy
    session[:shopping_cart_id] = nil
    if params[:return] == "checkout"
      redirect_to place_order_path(:purchase_id => @cart["id"])
    else
      flash[:success] = "Logout successful!"
      redirect_to root_url
    end
    session["discount"] == nil
  end
  
end
