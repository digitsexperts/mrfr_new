class UsersController < ApplicationController

  # before_action :set_search, only: [:show,:change_password]

  def new
    if current_user.present?
      current_user_session.destroy
      current_user==nil
    end
    @user = User.new
  end


  def create
    json_data = params[:user].to_json
    
    # uri = URI.parse("http://52.27.11.233:81/")
    # http = Net::HTTP.new(uri.host, uri.port)
    # request = Net::HTTP::Post.new("/CRM/v1/CreateLead")
    # request.add_field('Content-Type', 'application/json')
    # request.body = json_data
    # response = http.request(request)

    @user = User.new(user_params)
    UserMailer.send_registration_email(user_params).deliver
    if @user.save
      flash[:success] = "Your account has been created."
      redirect_to new_customer_path
    else
      flash[:error] = "You are aleady been registered with these email address. Please signin."
      render :new
    end
  end
  

  def show
    if current_user
      @user = User.friendly.find(current_user.id)
    else
      flash[:error] = "You have already logout"
      redirect_to signup_path
    end
  end

  def update_user

    @customer = Customer.find_by_user_id(current_user.id)
    if @customer.nil?
      @customer_new = Customer.new(customer_params.merge(user_id: current_user.id))
      if @customer_new.save
        flash[:success] = "User updated successfully"
        redirect_to user_url(current_user.id)
      else
        render :new
      end
    else
      if @customer.update_attributes(customer_params)
        flash[:success] = "User updated successfully"
        redirect_to user_url(current_user.id)
      else
        render :new
      end
    end
  end
  

  def update_password
    
    @user = current_user
    if @user.valid_password? params[:user][:current_password]
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      if @user.changed? && @user.save
        flash[:success] = "Password changed successfully! Please login again."
        if(@user.is_admin? || @user.is_superadmin? || @user.is_blog_admin?)
          current_user_session.destroy

          redirect_to login_path
          #UserMailer.delay(run_at: Time.now).send_password_change_report(params,@user.email)
          UserMailer.send_password_change_report(params,@user.email).deliver
        else
          redirect_to login_path
        end
      else
        flash[:alert] = "Password and Confirm password does not match"
        if (@user.is_admin? || @user.is_superadmin? || @user.is_blog_admin?)
          redirect_to admin_admin_change_password_path 
        else
          redirect_to change_password_path
        end  
      end
    else
      flash[:error] = "Please enter correct password"
      if (@user.is_admin? || @user.is_superadmin? || @user.is_blog_admin?)
        redirect_to admin_admin_change_password_path 
      else
        redirect_to change_password_path
      end
    end
  end

  def change_password
  end

  def check_login
    user = User.where(:email => params[:email]).first
    
    if !user
      render :json => {:valid_credentials => false, message: "Invalid email or password"}
    elsif user.valid_password? params[:password]
      user_session = UserSession.create(user,true)
      user_session.errors.present? ? 
      (render :json => {:valid_credentials => false, message: "Invalid email or password"}) :
      (
        ShoppingCart.find(@cart['id']).update_attribute("user_id",user.id)
        render :json => {:valid_credentials => true}
      )
    else
      render :json => {:valid_credentials => false, message: "Invalid email or password"}
    end
  end

  def create_user
      user = User.new(:first_name=>params[:first_name],:last_name=>params[:last_name], :email=>params[:email], :password=>params[:password], :password_confirmation=>params[:password_confirmation])
      if user.save
        user_session = UserSession.create(user,true)
        begin
          customer = Customer.new( :company_name=>params[:company_name], :job_title=>params[:job_title], :address_street1=>params[:address_street1], :address_street2=>params[:address_street2], :zip_code=>params[:zip_code], :phone_no=>params[:phone_no], :city=>params[:city], :country_id=>params[:country_id],:user_id=>user.id)
          customer.save
        rescue
          puts "Exception while registering user"
        end
        render :json => {:success => true, :user => user}
      else
        render :json => {:success => false, :error_message => user.errors.full_messages.first }
      end
  end

  private
  def customer_params
    params.require(:customer).permit(:company_name,:job_title, :address_street1, :address_street2, :city, :zip_code, :phone_no,:country_id)
  end
  def user_params
    params.require(:user).permit(:first_name,:last_name, :email, :password, :password_confirmation, :password_reset_token, :password_reset_sent_at)
  end
  def set_search
    @search = Report.search_report(params[:q])
  end
end


