module ApplicationHelper
  include ActionView::Helpers::NumberHelper

  def get_area_of_intreset(enquiry_type,enquiry,interest_area,message)
    msg = case enquiry_type
    when "enquiry"
      enquiry
    when "sample_request"
      interest_area
    when "any_question"
      enquiry
    else
      message
    end
    msg
  end

  def get_item_count
    quantity =[]
    cart = @cart
    cart.shopping_cart_items.each do |item|
      quantity << item.quantity
    end
    cart.present? ? "#{quantity.inject{|sum,x| sum + x }}"  : ""
  end

  def tag_cloud(tags, classes)
    
    max = tags.sort_by(&:count).last
    tags.each do |tag|
      index = tag.count.to_f / max.count * (classes.size - 1)
      yield(tag, classes[index.round])
    end
  end

  def get_checkout_total(product, currency)
    price = get_checkout_product_price(product,currency.split("-").first)
    case currency.split(//).last(3).join
    when "GBP"
       gbp = (price.to_f*Rate.get_exchange_rate("GBP"))
      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(gbp, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
       eur = (price.to_f*Rate.get_exchange_rate("EUR"))
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(eur, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
      jpy = (price.to_f*Rate.get_exchange_rate("JPY"))
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(jpy, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      inr = (price.to_f*Rate.get_exchange_rate("INR"))
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision(price.to_f, :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end
  

  def get_checkout_product_price(product,type)
    case type
    when "one_user"
      return product.one_user
    when "five_user"
      return product.five_user  
    when "site_user"
      return product.site_user
    when "enterprise_user"
      return product.enterprise_user
    end
  end



  def get_currency_subtotal(cart)
    case cart["currency"]
    when "GBP"
       gbp = (cart['total'].to_f*Rate.get_exchange_rate("GBP"))
      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(gbp, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
       eur = (cart['total'].to_f*Rate.get_exchange_rate("EUR"))
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(eur, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
      jpy = (cart['total'].to_f*Rate.get_exchange_rate("JPY"))
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(jpy, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      inr = (cart['total'].to_f*Rate.get_exchange_rate("INR"))
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision(cart['total'].to_f, :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end

  
  def get_currency_grandtotal(cart,discount=0)

    case cart['currency']
    when "GBP"
      # gbp = (item.price*Rate.get_exchange_rate("GBP"))-discount.to_f
      # pound = gbp * item.quantity
       gbp = (cart['subtotal']*Rate.get_exchange_rate("GBP"))

      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(gbp, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
       eur = cart['subtotal']*Rate.get_exchange_rate("EUR")
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(eur, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
      jpy = cart['subtotal']*Rate.get_exchange_rate("JPY")
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(jpy, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      inr = cart['subtotal']*Rate.get_exchange_rate("INR")
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision(cart['total']-discount.to_f, :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end

  def get_rate_after_discount(cart,discount=0)
    
    case cart['currency']
    when "GBP"
      gbp = (cart['total']*Rate.get_exchange_rate("GBP"))-discount.to_f
      amount = number_with_precision(gbp, :precision => 2, :delimiter => ',')
    when "EUR"
      eur = (cart['total']*Rate.get_exchange_rate("EUR"))-discount.to_f
      amount = number_with_precision(eur, :precision => 2, :delimiter => ',')
    when "JPY"
      jpy = (cart['total']*Rate.get_exchange_rate("JPY"))-discount.to_f
      amount = number_with_precision(jpy, :precision => 2, :delimiter => ',')
    when "INR"
      inr = (cart['total']*Rate.get_exchange_rate("INR"))-discount.to_f
      amount = number_with_precision(inr, :precision => 2, :delimiter => ',')
    else
      amount = number_with_precision(cart['total']-discount.to_f, :precision => 2, :delimiter => ',')
    end
    amount
  end


  def get_rate_after_discount_hdfc(cart,discount=0)
    inr = (cart['total']*Rate.get_exchange_rate("INR"))-discount.to_f
    amount = number_with_precision(inr, :precision => 2, :delimiter => ',')
  end

  def get_currency_price(item)

    case item["currency"]
    when "GBP"  
       gbp = item["price"]*Rate.get_exchange_rate("GBP")
      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(gbp, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
       eur = item["price"]*Rate.get_exchange_rate("EUR")
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(eur, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
       jpy = item["price"]*Rate.get_exchange_rate("JPY")
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(jpy, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      inr = item["price"]*Rate.get_exchange_rate("INR")
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision(item['price'], :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end
  
  def get_currency_total_price(item)
    case item["currency"]
    when "GBP"  
      gbp = item["price"]*Rate.get_exchange_rate("GBP")
      pound = gbp * item["quantity"]
      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(pound, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
      eur = item["price"]*Rate.get_exchange_rate("EUR")
      euro = eur * item["quantity"]
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(euro, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
      jpy = item["price"]*Rate.get_exchange_rate("JPY")
      yen = jpy * item["quantity"]
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(yen, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      ind = item["price"]*Rate.get_exchange_rate("INR")
      inr = ind * item["quantity"]
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision(item['price'] * item['quantity'], :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end

  
  def get_currency_grand_price(item,discount)

    
    case item["currency"]
    when "GBP"  
      gbp = (item["price"]*Rate.get_exchange_rate("GBP"))-discount.to_f
      pound = gbp * item["quantity"]
      ShoppingCartItem.find(item["id"]).update_columns(:discounted_price => (item["price"]*Rate.get_exchange_rate("GBP"))-discount.to_f)
      %Q{ <i class="fa fa-gbp"></i>&nbsp;#{number_with_precision(pound, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "EUR"
      eur = (item["price"]*Rate.get_exchange_rate("EUR"))-discount.to_f
      euro = eur * item["quantity"]
      ShoppingCartItem.find(item["id"]).update_columns(:discounted_price => (item["price"]*Rate.get_exchange_rate("EUR"))-discount.to_f)
      %Q{ <i class="fa fa-eur"></i>&nbsp;#{number_with_precision(euro, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "JPY"
      jpy = (item["price"]*Rate.get_exchange_rate("JPY"))-discount.to_f
      yen = jpy * item["quantity"]
      ShoppingCartItem.find(item["id"]).update_columns(:discounted_price => (item["price"]*Rate.get_exchange_rate("JPY"))-discount.to_f)
      %Q{ <i class="fa fa-jpy"></i>&nbsp;#{number_with_precision(yen, :precision => 2, :delimiter => ',')}</a> }.html_safe
    when "INR"
      ind = (item["price"]*Rate.get_exchange_rate("INR"))-discount.to_f
      inr = ind * item["quantity"]
      ShoppingCartItem.find(item["id"]).update_columns(:discounted_price => (item["price"]*Rate.get_exchange_rate("INR"))-discount.to_f)
      %Q{ <i class="fa fa-inr"></i>&nbsp;#{number_with_precision(inr, :precision => 2, :delimiter => ',')}</a> }.html_safe
    else
      ShoppingCartItem.find(item["id"]).update_columns(:discounted_price => item["price"]-discount.to_f)
      %Q{ <i class="fa fa-usd"></i>&nbsp;#{number_with_precision((item['price'] * item['quantity'])-discount.to_f, :precision => 2, :delimiter => ',')}</a> }.html_safe
    end
  end

  def get_edition(type)
    case type[0..-5]
    when "one_user"
      return "1-User PDF"
    when "five_user"
       return "1-5 User PDF"
    when "site_user"
       return "Site PDF"
    when "enterprise_user"
       return "Enterprise PDF"
    end
  end
  


  def flash_class(level)
    case level
    when "success"
      "alert-success"
    when "error"
      "alert-danger"
    when "alert"
      "alert-warning"
    when "notice"
      "alert-info"
    else
      # flash_type.to_s
    end
  end
  

  def get_action_color(action)
    case action
    when "create"
      "success"
    when "destroy"
      "danger"
    when "update"
      "warning"
    else
    end
  end

  def get_active_class(country_id, country_abbr)
    if country_id == country_abbr
      return "active"
    end
  end

  def report_path(slug)
    "#{ENV['domain']}/reports/#{slug}"
  end

  def report_url(slug)
    "#{ENV['domain']}/reports/#{slug}"
  end

  def country_currency_mapping
    {
      "GBP" => {name: "gbp", icon: "fa-gbp"},
      "EUR" => {name: "eur", icon: "fa-eur"},
      "jpy" => {name: "yen", icon: "fa-jpy"},
      "INR" => {name: "inr", icon: "fa-inr"}
    }
  end

  
  def convert_toc(toc)

    lines = toc.split( /\r?\n/ )
    
    lines.map do |line|
      number = line.match(/^\d{1,2}(\.\d{1,2})*\b/i)
      number = line.match(/^\d\b/i) unless number
      if number
        float_number=number.to_s
        if float_number.to_f % 1 == 0
          "#{line}\n"
        elsif float_number.to_s.split(".").length == 1
          "&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 2
          "&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 3
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 4
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 5
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 6
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 7
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 8
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        elsif float_number.to_s.split(".").length == 9
          "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{line}\n"
        end
      else
        "#{line}\n"
      end
    end
  end

  def markdown(content)
    @markdown ||= Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, space_after_headers: true, fenced_code_blocks: true)
    @markdown.render(content)
  end 






end



