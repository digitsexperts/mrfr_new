ThinkingSphinx::Index.define :article, :with => :active_record do
  indexes id
  indexes slug
  indexes title, :sortable => true
  has published_date, :sortable => true

end
