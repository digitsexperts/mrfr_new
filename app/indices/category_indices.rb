ThinkingSphinx::Index.define :category, :with => :active_record do
  indexes ancestry, :sortable => true
  has id, :as => :category_id
  has parent_id
  indexes name, :as=>:category_name, :sortable => true
  has ancestry, :as=> :root_ancestry, :type => :integer
  indexes reports.title, :as => :report_title
  indexes report_types.id, :as => :report_type_id
end