ThinkingSphinx::Index.define :continent, :with => :active_record do
  indexes countries.name, :as=>"name"
  indexes name, :as=>:continent_name, :sortable => true
end