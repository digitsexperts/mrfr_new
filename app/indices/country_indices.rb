ThinkingSphinx::Index.define :country, :with => :active_record do
  indexes name, :as=>:country_name, :sortable => true
  has continent_id
  has regions.id, :as => :region_ids 
end