ThinkingSphinx::Index.define :enquiry, :with => :active_record do
  has id, :as => :enquiry_id
  has "DATE_FORMAT(created_at, '%Y%m%d')",type: :integer,:as => :created_date
  indexes enquiry_type
end