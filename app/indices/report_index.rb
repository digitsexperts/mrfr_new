# ThinkingSphinx::Index.define :report, :with => :active_record do
#   indexes title, :sortable => true
#   indexes publish_month, :sortable => true
#   indexes publisher.name, :as => :report_publisher_name
#   indexes publish_date, :sortable => true
#   indexes slug
#   indexes old_slug
#   indexes country_id, :as => :country_name, :sortable=> true
#   indexes country_reports.name, :as => :country_reports_name,:sortable => true

#   has one_user
#   has country_reports.country_id, :as => :country_id
#   has publisher_id, :as => :publisher_id, :sortable => true
#   has category_id ,:as=> :category_id , :sortable => true
#   has upcoming
#   has latest_publisher_report
#   has country_reports.country_id, :as=> :country_id, :sortable =>true
#   has id, :as => :report_id
# end

ThinkingSphinx::Index.define :report, :with => :active_record do
  indexes id
  indexes title, :sortable => true
  indexes publish_month, :sortable => true
  indexes report_type.name, :as => :report_report_type_name
  indexes publish_date, :sortable => true
  indexes slug
  indexes old_slug
  indexes selling_count, :sortable => true
  indexes country_id, :as => :country_name, :sortable=> true

  has one_user
  has country_reports.country_id, :as => :country_id
  has id, :as=>"report_id", :sortable => true
  has category_id
  has report_type_id, :as => :report_type_id, :sortable => true
  has upcoming
  has publish_date, :as => :report_publish_date, :sortable => true
  has active 
  has latest_report_type_report
end


