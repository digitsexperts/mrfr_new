ThinkingSphinx::Index.define :report_type, :with => :active_record do
  indexes name, :sortable => true
  #indexes abbrevation, :as => :abb

  has reports.category_id, :as => :report_category_id
  indexes reports.id, :as => :report_id
end