class UserMailer < ActionMailer::Base
   default :from => "info@marketresearchfuture.com"
   CLIENT_EMAILS = []
   DEVELOPER_EMAILS = ["pganvir@digitsexperts.com"]


  def password_reset(user)
    @user = user
    @url = "https://www.marketresearchfuture.com/password_resets/#{user.password_reset_token}/edit"
    mail :to => user.email, :subject => "Password Reset"
  end

  def send_report_pdf(user,report_list,order_id,cart,purchase_invoice_id)
    @user = user
    @report_list = report_list
    @order_id = order_id
    @cart = cart
    @purchase_invoice = PurchaseInvoice.find(purchase_invoice_id)
    mail(:subject => 'Your marketresearchfuture order of report has been placed ', :to => @user.email) do |format|
      format.html
      format.pdf do
        attachments['invoice.pdf'] = WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => "invoice",:template => 'orders/invoice_form.pdf.erb')
        )
      end
    end
     mail :to => @user.email, :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => ["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :subject => "Your marketresearchfuture order of report has been placed "
  end

  def send_emails(user, reports)
    @user = user
    @reports = reports
    mail :to => @user.email,  :subject => "New Reports added"
  end

  def email_reports(email_reports)
    @email_reports = email_reports
    @report = Report.find(email_reports["report_id"].to_i)
    mail :to => email_reports["friend_email"], :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => email_reports["email"],  :subject => "Interesting Report Shared by #{email_reports["email"]}"
  end
   
  def email_report_to_team(email_reports)
    @email_reports = email_reports
    @report = Report.find(email_reports["report_id"].to_i)
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com",  :subject => "New Report Shared!"
  end

  def user_login_for_buying_reports(user,cart,subject)
    @user = user
    @cart = cart
    @subject = subject
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => ["info@marketresearchfuture.com", 'pganvir@digitsexperts.com'], :subject => "#{get_steps_subject(@subject)}"
  end

  def reports_purchased(user,cart)
    @user = user
    @cart = cart
    @subject = "Reports purchased!!!"
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com", :subject => "Reports purchased!!!"
  end 

  def send_enquiry(report, enquiry)
    @report = report
    @report_url = "https://www.marketresearchfuture.com/reports/#{@report.slug}"
    @enquiry = enquiry
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com",  :subject => "#{get_enquiry_subject(@enquiry.enquiry_type)}"
  end
  
  
  def wire_transfer_request(user,cart)
    @user = user
    @cart = cart
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com", :subject => "Wire transfer request" 
  end


  def get_enquiry_subject(type)
    case type
    when "sample_request"
      return "Sample Request for report "
    when "check_discount"
      return "Discount query for report"
    when "enquiry"
      return "Question for report"
    when 'contact_analyst'
      return 'Contact request to analyst'
    when "request_for_toc"
      return "TOC Request for report"
    end
  end

  def get_steps_subject(type)
    case type
    when "create_user"
      return "New user has been registered to WiseGuy "
    when "manage_address"
      return "User is on the your detail step"
    when "create_customer"
      return "User has created his company detail to buy a reports"
    when "final_step"
      return "User is in the final stage and ready to buy reports"
    when "add_to_cart"
      return "User has added a report in his cart"
    end
  end

  def send_message(message)
    @message = message
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com", :subject => "Message from marketresearchfuture.com"
  end

  def send_excelsheet_upload_status(sheet_id,errors)
    @excelsheet = Excelsheet.find(sheet_id)
    @array_errors = errors
    mail :to => ["sales@marketresearchfuture.com"], :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com", :subject => "Excel sheet has been uploaded"
  end

  def send_registration_email(user_params)
    @user_params = user_params
    mail :to => ["sales@marketresearchfuture.com"], :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com", :subject => "New User Registered"
  end


  def excelsheet_download_fail(excelsheet)
    @excelsheet = excelsheet
    mail(:to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => ["info@marketresearchfuture.com","pganvir@digitsexperts.com"], :subject => "Excelsheet download failure").deliver
  end

  def invalid_excelsheet(excelsheet)
    @excelsheet = excelsheet
    mail(:to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => ["info@marketresearchfuture.com","pganvir@digitsexperts.com"], :subject => "Excelsheet processing failure").deliver
  end

  def notify_developer(excelsheet,row,e)
    @excelsheet = excelsheet
    @row = row
    @e = e
    mail(:to => ["pganvir@digitsexperts.com"], :subject => "Error Occured!").deliver
  end

  def send_excelsheet_status
    @excelsheets = Excelsheet.where(:status => 2, notified: false).order("updated_at DESC")
    mail(:to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => ["info@marketresearchfuture.com","pganvir@digitsexperts.com"], :subject => "Excelsheet status").deliver
    @excelsheets.update_all(:notified => true)
  end


  def invalid_excelsheet_columns(excelsheet,errors)
    @excelsheet = excelsheet
    @errors = errors
    mail(:to => ["pganvir@digitsexperts.com"], :subject => "Error Occured!").deliver
  end


  def notify_developer_error(excelsheet,e)
    @e = e
    @excelsheet = excelsheet
    mail(:to => DEVELOPER_EMAILS, :subject => "Error Occured!").deliver
  end

  def send_blog_comment(comment)
    @blog_comment = comment
    @blog = Admin::Blog.unscoped.find(comment.blog_id)
    @allow = "https://marketresearchfuture.com/allow?blog_comment_id=#{comment.id}" 
    @disallow = "https://marketresearchfuture.com/disallow?blog_comment_id=#{comment.id}"
    mail :to => ["prachibh1@mailinator.com"],:subject => "New blog comment posted!"

    # mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com",:subject => "New blog comment posted!"
  end
  
  def send_blog_logs(blog)
    @blog = blog
    mail :to => ["sales@marketresearchfuture.com"], :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com", :subject => "Blog logs"
  end

  def send_news_logs(news)
    @news = news
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com", :subject => "News logs"
  end

  def send_newsletter(subscriber)
     @subscriber = subscriber
     @newsletter = NewsletterTemplate.last
     mail :to => @subscriber.email, :subject => "#{@newsletter.subject}"
  end

  def send_chat_report(details)
    @chat = details
    mail :to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],  :bcc => "info@marketresearchfuture.com", :subject => "Enquiry for chat!"
  end

  def send_password_change_report(details,email)
    @detail = details
    @email = email
    mail :to => ["sales@marketresearchfuture.com"], :cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => "info@marketresearchfuture.com", :subject => "Admin password change!"
  end

  def new_report_request(options)
    @options = options
    mail(:to=>"info@marketresearchfuture.com",:cc=> [["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"],["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"]],:subject=>"Report not available in the website")
  end

  def new_subscription_to_team(email)
    @email = email
    mail(:to=>"info@marketresearchfuture.com",:subject=>"New Newsletter Subscription!!")
  end

  def unsubscription_email_to_team(subscription)
    @subscription = subscription
    mail(:to=>"info@marketresearchfuture.com",:subject=>"Newsletter Un-Subscribed!!")
  end


  def unable_to_convert_excelsheet(excelsheet)
    @excelsheet = excelsheet
    mail(:to => ["pganvir@digitsexperts.com"], :subject => "Error Occured!").deliver
  end

  def invalid_excelsheet_columns(excelsheet,errors)
    @excelsheet = excelsheet
    @errors = errors
    mail(:to => ["sales@marketresearchfuture.com"],:cc=>["sales@marketresearchfuture.com","salesteam@marketresearchfuture.com","info@marketresearchfuture.com"], :bcc => ["info@marketresearchfuture.com", 'pganvir@digitsexperts.com'], :subject => "Error Occured!").deliver
  end

  def buynow_wire_transfer_request(user,report_id,edition,price)
    @user = user
    @report = Report.search("@(id) '#{report_id}'").first
    @edition = edition
    @price = price
    mail :to => "sales@marketresearchfuture.com",:cc=>"salesteam@marketresearchfuture.com", :bcc => "info@marketresearchfuture.com",:subject => "Wire transfer request"
  end

  def buyone_reports_purchased(user,report_id,edition,price)
    @user = user
    @report = Report.search("@(id) '#{report_id}'").first
    @edition = edition
    @price = price
    mail :to => "sales@marketresearchfuture.com",:cc=>"salesteam@marketresearchfuture.com", :bcc => ["info@marketresearchfuture.com", 'pganvir@digitsexperts.com'],:subject => "Reports purchased!!!"
  end
  
  def buynow_purchasing(user,report_id,edition,price)
    @user = user
    @report = Report.search("@(id) '#{report_id}'").first
    @edition = edition
    @price = price
    mail :to => "sales@marketresearchfuture.com",:cc=>"salesteam@marketresearchfuture.com", :bcc => ["info@marketresearchfuture.com", 'pganvir@digitsexperts.com'],:subject => "Purchasing Final Step"
  end


end
