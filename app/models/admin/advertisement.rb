class Admin::Advertisement < ActiveRecord::Base
	self.table_name = "admin_advertisements"
     
    mount_uploader :advertisement_image, AdvertisementImageUploader
    validates :advertisement_image, presence: true

    validate :advertisement_image_size

    def advertisement_image_size
    	if advertisement_image.present?
		    if (advertisement_image.width < 161 || advertisement_image.height < 600)
		      errors.add(:advertisement_image, "You cannot upload an image less than 161x600")
		    end
		end
	end

end
