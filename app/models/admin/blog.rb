class Admin::Blog < ActiveRecord::Base
    self.table_name = "admin_blogs"
    include ClearBlogCache
	acts_as_taggable
	audited
	has_many :blogs_comments, :foreign_key => :blog_id
	belongs_to :category
	after_save :send_email
	after_update :clear_cache

	extend FriendlyId
    friendly_id :title, use: :slugged

	def normalize_friendly_id(string)
	  super.split("-")[0..6].join("-")
	end

    def self.get_taged_blogs(tag)
       Rails.cache.fetch("get-taged-blogs-#{tag}") do
	      Admin::Blog.tagged_with(tag)
	   end
    end

    def self.get_category_blogs(category_slug)
      Rails.cache.fetch("get-category-blogs-#{category_slug}") do
        category = Category.friendly.find(category_slug)
        Admin::Blog.where(category_id: category.descendant_ids << category.id) 
	  end
    end

    def self.get_blog_archive
      Rails.cache.fetch("get-blog-archive") do
        Admin::Blog.all.order("created_at desc").group_by { |post| post.created_at.beginning_of_month } 
      end
    end
	
	def send_email
	 # UserMailer.delay(run_at: Time.now).send_blog_logs(self)
	 UserMailer.send_blog_logs(self).deliver
	 true
	end

	def tag_list
		Rails.cache.fetch("blog-tags-#{id}") do
		  super
		end
	end

	def self.list
		Rails.cache.fetch("blog-list") do
	      Admin::Blog.select("id","title", "content", "user_id", "category_id","created_at","slug").order("created_at DESC").limit(4)
	    end
	end

	private

	def clear_cache
      Rails.cache.delete("blog-tags-#{self.id}")
      true
	end
end
