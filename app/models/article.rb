class Article < ActiveRecord::Base

   extend FriendlyId
   friendly_id :title, use: :slugged
   mount_uploader :article_image, ArticleImageUploader
   
  def normalize_friendly_id(string)
    super.split("-")[0..6].join("-")
  end

	def self.latest_articles
	  Rails.cache.fetch("latest-articles") do
	    Article.order("published_date ASC").limit(4)
	  end
	end

    def self.search_article(search_term,page=1)
      if search_term.present?
        Article.search(sphinx_search_sql(search_term),:sql => {:select => "articles.id, articles.title, articles.content,articles.published_date, articles.slug, articles.article_image as rimage"}, :order => "published_date desc",:ranker => :proximity, :page => page, :per_page => 10)
      else
        Article.search(:sql => {:select => "articles.id, articles.title, articles.content, articles.published_date, articles.slug, articles.article_image as rimage"},  :order => "published_date desc", :page => page, :per_page => 10)
      end
    end


    def self.sphinx_search_sql(search_term)
      special_chars = ['~','!','@','#','$','%','^','&','*','(',')','_','\+','=','-','[',']','{','}','|',':','\;','<','>','?','/','.',';']
      search_term = search_term.is_a?(Array) ? search_term.first : search_term
      search_term = search_term.gsub(';','') if search_term.to_s.include?(';')
      search_term = search_term.gsub(':','') if search_term.to_s.include?(':')
      if search_term
        term_combinations = []
        term_combinations << "'#{Riddle::Query.escape(search_term)}'"
        term_combinations << Riddle::Query.escape(search_term).split(' ').map{|t| "'#{t}'"}

        term_combinations.flatten!
        term_combinations.delete_if {|term| (term.length == 4) && term.start_with?("\'\\")}
        term_combinations.join(' | ').prepend('@(title,slug) ')
      else
        search_term
      end
    end

end
