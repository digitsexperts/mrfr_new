class Category < ActiveRecord::Base
  mount_uploader :category_report_image, CategoryReportImageUploader
  mount_uploader :category_image, CategoryImageUploader
  mount_uploader :category_icon, CategoryIconUploader
  include Tree
  has_many :reports, :dependent => :destroy
  has_many :news_updates, :dependent => :destroy
  has_many :blogs, :dependent => :destroy, :class_name => "Admin::Blog"
  has_many :publishers, through: :reports
  has_many :country_reports, through: :reports
  has_many :countries, {:through=>:reports, :source=>:country}
  has_many :regions, through: :country_reports
  has_many :continents, through: :countries
  has_many :report_types, through: :reports
  extend FriendlyId
  friendly_id :name, use: :slugged
  before_save   :cache_ancestry
  before_update :cache_ancestry
  before_create :cache_ancestry 
  after_save :expire_cache

  def cache_ancestry
    self.name = self.name.strip     
  end

  def reports
    Report.where("category_id in (?)", self.subtree.pluck("id")) 
  end

  def publishers
    report_type_count = ReportType.search(:sql=>{:select=>"report_types.id"}).total_entries
    ReportType.search(:max_matches => report_type_count,:with=>{:category_ids=>self.subtree.pluck("id")})
  end

  def self.get_all_categories
    Rails.cache.fetch("all-categories") do
      Category.search(:sql => {:select => "id,slug,name,reports_count,category_icon as category_icon" }, :with => {:root_ancestry => 0},:order=>"category_name asc").to_a
    end
  end

  def self.options_for_select
    Category.roots.all.map { |e| [e.name, e.id] }
  end

  def expire_cache
    Rails.cache.delete("report_category_count")
  end

  def self.category_hash
    Rails.cache.fetch("category-hash") do
      category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
      Category.search(:sql => {:select => "id,slug,name" }, :with => {:root_ancestry => 0},:per_page=>category_count).to_a.inject({}) {|i,n| i.merge({n.id => {:name => n.name, :slug => n.slug}})}
    end
  end

  def self.sphinx_get_child_ids(categories_ids)
    category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
    Category.search(:with => {:root_ancestry => categories_ids},:sql=>{:select=>"id"},:per_page=>category_count).map(&:id)
  end

  def self.get_category_and_subcategory(categories_ids)
    category_ids = categories_ids
    category_ids.push(Category.sphinx_get_child_ids(categories_ids))
    category_ids.flatten
  end

  def self.recount_report_count
    all.each do |category|
      category_count = Category.search(:sql=>{:select=>"categories.id"}).total_entries
      category_ids = [category.id,Category.search(:with => {:root_ancestry => category.id},:sql => { :select => "id" },:per_page=>category_count).map(&:id)].flatten
      new_reports_count = Report.search(:with => {:upcoming => 0, :category_id => category_ids}, :sql => {:select => 'reports.id'}).total_entries
      category.update_attribute('reports_count',new_reports_count)
    end
  end

end
