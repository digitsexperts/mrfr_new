class Continent < ActiveRecord::Base
	has_many :countries
    has_many :country_reports, through: :countries, :class_name => "Report"
    has_many :regions, through: :countries
    has_many :reports
    has_many :publishers, through: :country_reports 
    has_many :categories, through: :country_reports

	extend FriendlyId
    friendly_id :name, use: :slugged

    def self.get_country_ids_of_continents(continents_ids)
      Country.search(:sql=>{:select=>"countries.id,countries.name"},:with => {:continent_id => continents_ids},:per_page => 35).map(&:id)
    end

    def self.list
      Rails.cache.fetch("continent-list") do
        select("id","reports_count","name","slug").order("name asc")
      end
    end

    def self.filter_list(options={})
        joins_array = []
        joins_array << :categories if options[:category_ids].present?
        joins_array << :countries if options[:country_ids].present?
        joins_array << :publishers if options[:report_type_ids].present?
        joins_array << :regions if options[:region_ids].present?
        joins_array << :continents if options[:continent_ids].present?
        where_query = {}
        where_query.merge!({"categories.id" =>  options[:category_ids].map(&:to_i)}) if options[:category_ids].present?
        where_query.merge!({"countries.id" => options[:country_ids].map(&:to_i)}) if options[:country_ids].present?
        where_query.merge!({"publishers.id" => options[:report_type_ids].map(&:to_i)}) if options[:report_type_ids].present?
        where_query.merge!({"regions.id" => options[:region_ids].map(&:to_i)}) if options[:region_ids].present?
        where_query.merge!({"continents.id" => options[:continent_ids].map(&:to_i)}) if options[:continent_ids].present?
        
        if joins_array.present? && where_query.present?
          options[:model].joins(joins_array).where(where_query).distinct.select(options[:selected_parameters]).limit(5)
        else
          options[:model].where(where_query).distinct.select(options[:selected_parameters]).limit(5)
        end
    end

    def self.recount_report_count
      all.each do |continent|
        all_countries = Country.search(:with => {:continent_id => continent.id},:per_page=>Country.search(:sql => {:select => "countries.id"}).count)
        new_reports_count = Report.search({:sql=>{:joins=>[:publisher], :select=>"reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage, publishers.report_image as pimage, publishers.name as publisher_name,reports.report_type_id,reports.category_id"}, :with=>{:upcoming=>0, :country_id=>all_countries.map(&:id)}}).count
        continent.update_attribute('reports_count',new_reports_count)
      end
    end
end
