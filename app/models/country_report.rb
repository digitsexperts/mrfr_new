class CountryReport < ActiveRecord::Base
  self.table_name = "countries_reports"
  belongs_to :country
  belongs_to :report

  def continent_name
  	self.country.continent.name rescue nil
  end

  def name
  	self.country.name rescue nil
  end

  def region_names
  	self.country.regions.map(&:name).join(", ") rescue nil
  end
end
