class Customer < ActiveRecord::Base
  belongs_to  :user
  belongs_to  :country
  
  validates_uniqueness_of :user_id

  def full_address
    address_street1.to_s + " " + address_street2.to_s
  end

end
