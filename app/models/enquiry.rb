class Enquiry < ActiveRecord::Base
	belongs_to :country
	belongs_to :report
	delegate :name, to: :country, prefix: true, :allow_nil => true
	delegate :title,:one_user, to: :report, prefix: true, :allow_nil => true

	def full_name
	  "#{self.first_name} #{self.last_name}"
	end
end
