class Excelsheet < ActiveRecord::Base

  serialize :positions, Hash
  mount_uploader :excelsheet_file, ExcelsheetFileUploader
  enum status: [ :uploaded, :processing, :dumped, :failed ]

  POSITIONS = {
               :title=>0, :category=>1, :report_type=>2, :pages=>3, 
               :one_user_pdf=>4, :five_user_pdf=>5, :site_pdf=>6, 
               :enterprise_pdf=>7,  :publish_day=>8, :description=>9, 
               :table_of_content=>10, :country=>11,
               :country_ids => 12, 
               :continent=>13, :region=>14,
               :publish_month=>15,
               :report_slug => 16,
               :excelsheet_id => 17,
               :publish_date_integer => 18
              }

  DATABASE_CREDENTIALS = YAML.load_file(Rails.root.to_s+"/config/database.yml")[Rails.env]

  # 1. Read csv row by row
  # 1.1 If csv is corrupt then notify developer and client
  # 2. Add to error csv if not title present
  # 3. Add to error csv if category not present
  # 4. If 1-User PDF, 5-User PDF, Site PDF and Enterprise PDF is not present then add to csv
  # 5. If publish date is not present then ad to error csv
  # 6. If description is not present then add to error csv
  # 7. If TOC is not present the add to error csv
  # 8. If report_type not presernt then add to error csv
  # 9. If same report title, report_type,same category and same published date exists then add to error csv
  # 10. If any error occurs while inserting into database then add to error csv and developer_error_csv
  # 11. If no country, region and continent is present then make the continent global
  # 12. If report_type is not present (case insensetive) then create a report_type
  # 13. If category is not present then create a category
  # 14. If only countries are present add report to all the specified countries
  # 15. if only region is present then add report to region and all countries in region
  # 16. If only continent is present then add report to all continent and countries in continent
  # 16. If country and region is present then add report to region and all countries in region. The report should be added only once in overlapping countries between region and given countries in csv.
  # 17. If country and continent is present then add report to continennt and all countries in continent. The report should be added only once in overlapping countries.
  # 18. If region and continent is present the add report to region and continent and all non-overlapping countries (only once).

  def Excelsheet.dump_excelsheets
    require 'csv'
    require 'net/http'
    processed_any_record = false
    cron_control = CronController.first
    cron_control.update_attribute('dumping_in_progress',true)
    Excelsheet.uploaded.limit(3).each do |excelsheet|
      processed_any_record = true
      begin
      puts "processing Excelsheet-#{excelsheet.id}"
      excelsheet.processing!
      report_count = Report.recount_total

      excelsheet_file_path, csv_file_path, file_name = Excelsheet.create_folder_structure(excelsheet)

      begin
        puts "Downloading Excelsheet-#{excelsheet.id}"
        system("wget #{excelsheet.excelsheet_file.url} -O #{excelsheet_file_path}")
      rescue Exception => e
        excelsheet.failed!
        UserMailer.excelsheet_download_fail(excelsheet)
        next
      end

      if File.zero?(excelsheet_file_path)
        excelsheet.failed!
        UserMailer.excelsheet_download_fail(excelsheet)
        next
      end

      begin
        puts "Converting Excelsheet-#{excelsheet.id} to csv"
        system("ssconvert #{excelsheet_file_path} '#{csv_file_path}'")
      rescue Exception => e
        excelsheet.failed!
        UserMailer.invalid_excelsheet(excelsheet)
        next
      end

      unless File.exist?(csv_file_path)
        excelsheet.failed!
        UserMailer.unable_to_convert_excelsheet(excelsheet)
        next
      end
      
      header = `head -n 1 #{csv_file_path}`.chomp
      `sed -i '1d' #{csv_file_path}`
      
      `sed -i 's/_x000D_//g' #{csv_file_path}`
      `sed -i 's/_x000d_//g' #{csv_file_path}`
      #check for valid headers
      header_analysis = valid_header(excelsheet,header.split(","))
      if !header_analysis[0]
         UserMailer.invalid_excelsheet_columns(excelsheet,header_analysis[1])
         excelsheet.failed!
         next
      end

      #splitting files into chunks of 200
      begin
        split_excelsheets(csv_file_path)
      rescue Exception => e
        puts "Splitting error"
        next
      end
      dirname = File.dirname(csv_file_path)
      Dir["#{dirname}/split/*.csv"].each do |csv_file|
        identify_valid_data(csv_file,excelsheet)
      end

      dump_csvs(csv_file_path,excelsheet)
      associate_countries(excelsheet)
      excelsheet.dumped!
      new_report_count = Report.count
      track_dumping(excelsheet,csv_file_path, report_count, new_report_count)
      puts "Finished processing Excelsheet-#{excelsheet.id}"
      rescue Exception => e
        puts "Some error occured processing Excelsheet-#{excelsheet.id}"
        UserMailer.notify_developer_error(excelsheet,e)
      end
      cron_control.update_attributes(local_indexing_need: true)
    end #if (cron_control.dumping_allowed)
    cron_control.update_attributes(dumping_in_progress: false)
    index_wiseguy_main_db
    ActiveRecord::Base.connection.close
  end


  def Excelsheet.index_wiseguy_main_db
    require 'rake'
    puts "Checking if remote indexing needed at #{DateTime.now}"
    cron_control = CronController.first
    if !cron_control.dumping_in_progress && cron_control.local_indexing_need
      puts "Starting remote indexing at #{DateTime.now}"
      puts 'Updating old slug'
      Report.where(:old_slug => nil).update_all('old_slug = slug')
      puts "Removing duplcates"
      Report.remove_duplicates
      #puts 'Copying Global'
      #Report.where("country_ids = 'Global' and country_id = NULL").update_all("country_id = country_ids")
      output = `RAILS_ENV=production rake ts:index`
      puts output
      refresh_counts
      puts "Finished indexing, clearing cache at #{DateTime.now}"
      Rails.cache.clear
      puts "cache cleared at #{DateTime.now}"
      #User.build_cache
      #puts "cache built at #{DateTime.now}"
      puts "Finished remote indexing at #{DateTime.now}"
      puts "Sending excelsheet status at #{DateTime.now}"
      current_reports_count = Report.count
      if cron_control.old_reports_count != current_reports_count
        cron_control.update_attributes(:old_reports_count => current_reports_count)
      end
      UserMailer.send_excelsheet_status if Excelsheet.where(:status => 2, notified: false).present?
      cron_control.update_attributes(local_indexing_need: false)
    end
    ActiveRecord::Base.connection.close
    puts "Remote indexing cron finished at #{DateTime.now}"

  end


  def Excelsheet.refresh_counts
    Report.create_report_codes
    #Report.fix_bad_format
    ReportType.recount_report_count
    Category.recount_report_count
    Country.recount_report_count
    Continent.recount_report_count
    Region.recount_report_count
   # ReportType.set_latest_reports
    ActiveRecord::Base.connection.close
  end

  def Excelsheet.create_folder_structure(excelsheet)
    FileUtils.rm_rf("#{Rails.root}/public/#{excelsheet.id}/") if Dir.exists?("#{Rails.root}/public/#{excelsheet.id}/")
    Dir.mkdir("#{Rails.root}/public/#{excelsheet.id}/")
    Dir.mkdir("#{Rails.root}/public/#{excelsheet.id}/split")
    Dir.mkdir("#{Rails.root}/public/#{excelsheet.id}/failed_data")
    Dir.mkdir("#{Rails.root}/public/#{excelsheet.id}/succeeded_data")
    Dir.mkdir("#{Rails.root}/public/#{excelsheet.id}/sql_files")
    [
      "#{Rails.root}/public/#{excelsheet.id}/#{excelsheet.excelsheet_file.file.filename}",
      "#{Rails.root}/public/#{excelsheet.id}/#{excelsheet.excelsheet_file.file.filename.split(".")[0]}.csv",
       excelsheet.excelsheet_file.file.filename.split(".")[0]
    ]
  end

  def Excelsheet.write_csv(file_name,lines)
    CSV.open(file_name, "w") do |csv|
      lines.each {|line| csv << line} 
    end
  end

  def Excelsheet.split_excelsheets(csv_file_path)
    csv_folder = File.dirname(csv_file_path)
    lines_per_file = 200
    i = 0
    file_number = 0
    extension = ".csv"
    basename = File.basename(csv_file_path, extension)
    lines = []
    header = []
    CSV.foreach(csv_file_path) do |row|
      #header = row if i.zero?
      lines << row
      if lines.length >= lines_per_file
        write_csv("#{csv_folder}/split/split_#{file_number+=1}#{extension}",lines)
        lines = []
      end
      i+=1
    end

    write_csv("#{csv_folder}/split/split_#{file_number+=1}#{extension}",lines) if lines.present?
    # if Dir["#{csv_folder}/split/*.csv"].empty?
    #   FileUtils.cp(csv_file_path,"#{csv_folder}/split/split_#{file_number+=1}#{extension}")
    # end
  end

  def Excelsheet.identify_valid_data(csv_file_path,excelsheet)
    rows = CSV.readlines(csv_file_path)
    sql = []
    malformed_rows = []
    valid_rows = []
    row_number = 0
    rows=rows.reject {|row| row.compact.empty? }
    rows.each do |row|
      country_ids = countries(row)
      #row = [row[POSITIONS[:title]],row[POSITIONS[:category]],row[POSITIONS[:pages]],row[POSITIONS[:one_user_pdf]] ,row[POSITIONS[:five_user_pdf]],row[POSITIONS[:site_pdf]],row[POSITIONS[:enterprise_pdf]],row[POSITIONS[:publish_day]], row[POSITIONS[:publish_month]],row[POSITIONS[:description]],row[POSITIONS[:table_of_content]],row[POSITIONS[:report_type]],row[POSITIONS[:country]], row[POSITIONS[:country_ids]],row[POSITIONS[:continent]],row[POSITIONS[:region]],row[POSITIONS[:report_slug]],row[POSITIONS[:excelsheet_id]],row[POSITIONS[:publish_date_integer]]]
      row_number +=1

      if row[POSITIONS[:report_type].to_i].blank?
        row_error_check = [true,["report_type is not specified"]]
      elsif row[POSITIONS[:category].to_i].blank?
        row_error_check = [true,["Category is not specified"]]
      else
        #create report_type if not exist
        report_type = ReportType.where(:abbreviation => "#{row[POSITIONS[:report_type].to_i].to_s.squish}").first_or_initialize
        if report_type.new_record?
          report_type.slug = Report.new.normalize_friendly_id(report_type.name)
          report_type.save!
        end

        #create category if not exist
        category = Category.where(:name => "#{row[POSITIONS[:category].to_i].to_s.squish.gsub(' and ', ' & ')}").first_or_initialize
        if category.new_record?
          category.slug = Report.new.normalize_friendly_id(category.name)
          category.save!
        end
        day = "#{Date.parse(row[POSITIONS[:publish_day]]).strftime("%d").to_i}"
        month = "#{Date.parse(row[POSITIONS[:publish_day]]).strftime("%m/%Y")}"
        row[POSITIONS[:publish_day]] = day
        row[POSITIONS[:publish_month]] = month
        row_error_check = has_error?(row,report_type.id,category.id)
        row[POSITIONS[:category]] = category.id
        row[POSITIONS[:report_type]] = report_type.id
        row[POSITIONS[:country]] = "Global" if country_ids.empty?
        row[POSITIONS[:country_ids]] = country_ids.present? ? country_ids : nil
        publish_date = Date.parse("#{(day.to_i.zero? ? 1 : day)}/#{month}").strftime("%Y%m%d")
        duplicate_report = Report.search("@(title,publish_date) '#{Riddle::Query.escape(row[POSITIONS[:title]])}' #{publish_date}", :with => {:category_id => row[POSITIONS[:category]], :report_type_id => row[POSITIONS[:report_type]]},:limit=>1).first
        if duplicate_report
          row_error_check = [true,["Report is duplicate with #{duplicate_report.url}"]]
        else
          new_friendly_id = Report.new.normalize_friendly_id(row[0])
          duplicate_count = Report.search("@slug '#{Riddle::Query.escape(new_friendly_id)}%'", :sql => {:select => "id"}).count
          new_friendly_id += (duplicate_count.zero? ? "" : "-#{duplicate_count.humanize.gsub(",","").gsub(" ","-")}")
          row[POSITIONS[:report_slug]] = new_friendly_id
          row[POSITIONS[:excelsheet_id]] = excelsheet.id
          if publish_date.to_s.length == 7
            last_char = "0"+publish_date.to_s[-1,1]
            remaining_char = publish_date.to_s[0,publish_date.to_s.length-1]
            publish_date = remaining_char+last_char
          end
          row[POSITIONS[:publish_date_integer]] = publish_date
        end
      end

      if row_error_check[0]
        malformed_rows << [row_number,row[POSITIONS[:title]],category.name,row[POSITIONS[:pages]],row[POSITIONS[:one_user_pdf]],row[POSITIONS[:five_user_pdf]],row[POSITIONS[:site_pdf]],row[POSITIONS[:enterprise_pdf]],"#{row[POSITIONS[:publish_day]]}/#{row[POSITIONS[:publish_month]]}",row[POSITIONS[:description]],row[POSITIONS[:table_of_content]],report_type.name,row[POSITIONS[:country]],row[POSITIONS[:continent]],row[POSITIONS[:region]],row_error_check[1].join(',')]
      else
        valid_rows << row.flatten
      end
    end
    write_success_csv_file(csv_file_path,valid_rows)
    write_fail_csv_file(csv_file_path,malformed_rows)
    ActiveRecord::Base.connection.reconnect!
  end

  def Excelsheet.excelsheet_publish_date(day,month)
    "#{(day.to_i.zero? ? 1 : day)}/#{month}"
  end


  def Excelsheet.has_error?(row,report_type_id,category_id)
    error_messages = []
    if row[POSITIONS[:title].to_i].blank?
      error_messages << "Excelsheet row does not specify title"
    elsif row[POSITIONS[:category].to_i].blank?
      error_messages << "Excelsheet row does not specify category"
    elsif (row[POSITIONS[:one_user_pdf].to_i].blank? && row[POSITIONS[:five_user_pdf].to_i].blank? && row[POSITIONS[:site_pdf].to_i].blank? && row[POSITIONS[:enterprise_pdf].to_i].blank?)
      error_messages << "Excelsheet row does not specify price"
    elsif row[POSITIONS[:publish_date].to_i].blank?
      error_messages << "Excelsheet row does not specify publish date"
    elsif row[POSITIONS[:description].to_i].blank?
      error_messages << "Excelsheet row does not specify report description"
    elsif row[POSITIONS[:table_of_content].to_i].blank?
      error_messages << "Excelsheet row does not specify table of contents"
    elsif row[POSITIONS[:report_type].to_i].blank?
      error_messages << "Excelsheet row does not specify report_type"
    elsif !Report.search(Riddle::Query.escape("@(title,publish_month) '#{row[POSITIONS[:title].to_i].to_s.squish}' '#{Date.parse(row[POSITIONS[:publish_month].to_i]).strftime("%m/%Y")}'"), :with => {:category_id => "#{category_id}", :report_type_id => report_type_id}, :limit => 1 ).count.zero?
      error_messages << "Report is duplicate with #{Report.search(Riddle::Query.escape("@(title,publish_month) '#{row[POSITIONS[:title].to_i].to_s.squish}' '#{Date.parse(row[POSITIONS[:publish_month].to_i]).strftime("%m/%Y")}'"), :with => {:category_id => "#{category_id}", :report_type_id => report_type_id}, :limit => 1 ).first.url}"
    end
    [error_messages.present?,error_messages]
  end

  def Excelsheet.countries(row)
    country_list = []
    if row[POSITIONS[:country].to_i].present?
      country_list << Country.where(:name => row[POSITIONS[:country].to_i].squish.split(",")).pluck("id").flatten
    end

    if row[POSITIONS[:region].to_i].present?
      row[POSITIONS[:region].to_i].squish.split(",").each do |region|
        country_list << Region.find_by_name(region).countries.pluck("id").flatten rescue "May be region is not present"
      end
    end

    if row[POSITIONS[:continent].to_i].present?
      row[POSITIONS[:continent].to_i].squish.split(",").each do |continent|
        country_list << Continent.find_by_name(continent).countries.pluck("id").flatten rescue "May be continent is not present"
      end
    end

    country_list.flatten.uniq
  end

  def Excelsheet.valid_header(excelsheet,columns)
    errors = []
    if !columns[0].to_s.downcase.include?  'title'
      errors << "TITLE column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[1].to_s.downcase.include?  'category'
      errors << "Category column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[2].to_s.downcase.include?  'report type'
      errors << "report_type column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[3].to_s.downcase.include?  'pages'
      errors << "Pages column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[4].to_s.downcase.include?  '1-user pdf'
      errors << "1-user-pdf column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[5].to_s.downcase.include?  '5-user pdf'
      errors << "5-user-pdf column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[6].to_s.downcase.include?  'site pdf'
      errors << "Site pdf column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[7].to_s.downcase.include?  'enterprise pdf'
      errors << "Enterprise pdf column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[8].to_s.downcase.include?  'publish month'
      errors << "Publish date column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[9].to_s.downcase.include?  'description'
      errors << "Description column is invalid or not readable by the program. It can be possibly file encoding issue."
      return [errors.empty?,errors]
    end
    if !columns[10].to_s.downcase.include?  'table of content'
      errors << "Table Of Content column is invalid or not readable by the program. It can be possibly file encoding issue."
      return [errors.empty?,errors]
    end

    if !columns[11].to_s.downcase.include?  'country'
      errors << "Table Of Content column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[12].to_s.downcase.include?  'continents'
      errors << "Continents column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if !columns[13].to_s.downcase.include?  'region'
      errors << "Region column not found in excelsheet"
      return [errors.empty?,errors]
    end
    if columns.length < 14
      errors << "following columns are missing"
      if columns[0].to_s.blank?
        errors << "TITLE column not found in excelsheet"
      end
      if columns[1].to_s.blank?
        errors << "Category column not found in excelsheet"
      end
      if columns[2].to_s.blank?
        errors << "report_type column not found in excelsheet"
      end
      if columns[3].to_s.blank?
        errors << "Pages column not found in excelsheet"
      end
      if columns[4].to_s.blank?
        errors << "1-user-pdf column not found in excelsheet"
      end
      if columns[5].to_s.blank?
        errors << "5-user-pdf column not found in excelsheet"
      end
      if columns[6].to_s.blank?
        errors << "Site pdf column not found in excelsheet"
      end
      if columns[7].to_s.blank?
        errors << "Enterprise pdf column not found in excelsheet"
      end
      if columns[8].to_s.blank?
        errors << "Publish date column not found in excelsheet"
      end
      if columns[9].to_s.blank?
        errors << "Description column is invalid or not readable by the program. It can be possibly file encoding issue."
      end
      if columns[10].to_s.blank?
        errors << "Table Of Content column is invalid or not readable by the program. It can be possibly file encoding issue."
      end
      if columns[9].to_s.blank?
        errors << "Description column not found in excelsheet"
      end
      if columns[10].to_s.blank?
        errors << "Table Of Content column not found in excelsheet"
      end

      if columns[11].to_s.blank?
        errors << "Country column not found in excelsheet"
      end
      if columns[12].to_s.blank?
        errors << "Continents column not found in excelsheet"
      end
      if columns[13].to_s.blank?
        errors << "Region column not found in excelsheet"
      end
    elsif columns.length > 14
      errors << "Invalid extra columns are found in excelsheet"
    elsif columns[0].to_s.downcase != "title"
      errors << "TITLE column not found in excelsheet"
    elsif columns[1].to_s.downcase != "category"
      errors << "Category column not found in excelsheet"
    elsif (columns[2].to_s.downcase.split && ["report", "type"]).length != 2
      errors << "report_type column not found in excelsheet"
    elsif columns[3].to_s.downcase != "pages"
      errors << "Pages column not found in excelsheet"
    elsif (columns[4].to_s.downcase.split("-").map{|d| d.split}.flatten && ["1", "user", "pdf"]).length != 3
      errors << "1-user-pdf column not found in excelsheet"
    elsif (columns[5].to_s.downcase.split("-").map{|d| d.split}.flatten && ["5", "user", "pdf"]).length != 3
      errors << "5-user-pdf column not found in excelsheet"
    elsif (columns[6].to_s.downcase.split("-").map{|d| d.split}.flatten && ["site", "pdf"]).length != 2
      errors << "Site pdf column not found in excelsheet"
    elsif (columns[7].to_s.downcase.split("-").map{|d| d.split}.flatten && ["enterprise", "pdf"]).length != 2
      errors << "Enterprise pdf column not found in excelsheet"
    elsif (columns[8].to_s.downcase.split && ["publish", "date"]).length != 2
      errors << "Publish date column not found in excelsheet"
    elsif columns[9].to_s.downcase != "description"
      errors << "Description column not found in excelsheet"
    elsif (columns[10].to_s.downcase.split && ["table", "of", "content"]).length != 3
      errors << "Table Of Content column not found in excelsheet"

    elsif columns[11].to_s.downcase != "country"
      errors << "Country column not found in excelsheet"
    elsif columns[12].to_s.downcase != "continents"
      errors << "Continents column not found in excelsheet"
    elsif columns[13].to_s.downcase != "region"
      errors << "Region column not found in excelsheet"
    end

    [errors.empty?,errors]
  end

  def Excelsheet.write_success_csv_file(csv_file_path,rows)
    require 'csv'
    dirname = File.dirname(csv_file_path).gsub('/split','')
    basename = File.basename(csv_file_path)
    if rows.present?
      success_csv = CSV.open("#{dirname}/succeeded_data/#{basename}", "w") do |csv|
        rows.each do |row|
          csv << row
        end
      end
    end
  end

  def Excelsheet.write_fail_csv_file(csv_file_path,malformed_rows)
    require 'csv'
    dirname = File.dirname(csv_file_path).gsub('/split','')
    basename = File.basename(csv_file_path)
    if malformed_rows.present?
      failed_csv = CSV.open("#{dirname}/failed_data/#{basename}", "w") do |csv|
        csv << ['Row Number','Title','Category', 'Report Type','Number Of Pages','One User','Five User','Site User','Enterprise User','Publish Date','Description','Table Of Contents','Country','Continents','Region','Failure Reason']
        malformed_rows.each do |row|
          csv << row
        end
      end
    end
  end

  def Excelsheet.dump_csvs(csv_file_path,excelsheet)
    dirname = File.dirname(csv_file_path)
    Dir["#{dirname}/succeeded_data/*.csv"].each do |csv_file|
      mysql_error_file = "#{dirname}/failed_data/#{File.basename(csv_file)}.txt"
      `mysql -u #{DATABASE_CREDENTIALS['username']} --password=#{DATABASE_CREDENTIALS['password']} --show-warnings -vve "LOAD DATA LOCAL INFILE '#{csv_file}' IGNORE INTO TABLE reports CHARACTER SET UTF8 FIELDS TERMINATED BY ',' ENCLOSED BY '\\"' ESCAPED BY '\\"' LINES TERMINATED BY '\n' (title,category_id,report_type_id,no_of_pages,one_user,five_user,site_user,enterprise_user,day,description,table_of_content,country_id,country_ids,@dummy, @dummy,publish_month, slug,excelsheet_file_id,publish_date)" --database #{DATABASE_CREDENTIALS['database']} > #{mysql_error_file}`
    end
  end

  def Excelsheet.associate_countries(excelsheet)
    #Report.where("excelsheet_file_id =  ? and country_ids = 'Global'",excelsheet.id).update_all("country_id = country_ids")
    country_association = Report.where("excelsheet_file_id =  ? and country_ids != 'Global'",excelsheet.id).pluck(:id, :country_ids)
    Report.where("excelsheet_file_id =  ? and country_ids != 'Global'",excelsheet.id).update_all("country_ids = NULL")
    CSV.open("#{Rails.root}/public/country_association.csv", "w") do |csv|
        country_association.each do |row|
          row[1].split(",").each do |country_id|
            csv << [row[0],country_id]
          end
        end
    end
    ActiveRecord::Base.connection.execute("LOAD DATA LOCAL INFILE '#{Rails.root}/public/country_association.csv'  IGNORE INTO TABLE countries_reports CHARACTER SET UTF8 FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' (report_id,country_id);") unless File.zero?("#{Rails.root}/public/country_association.csv")
    FileUtils.rm_rf("#{Rails.root}/public/country_association.csv")
  end

  def Excelsheet.track_dumping(excelsheet,csv_file_path, old_report_count, new_report_count)
    dirname = File.dirname(csv_file_path)
    excelsheet_name = File.basename(csv_file_path)
    failed_csv_file_paths = dirname+"/failed_data"
    failed_data_file = "#{dirname}/failed_reports_#{excelsheet_name}.csv"
    `cat #{failed_csv_file_paths}/*.csv > #{failed_data_file}`
    rows_failed = !File.zero?("#{dirname}/failed_reports_#{excelsheet_name}.csv")
    excelsheet.update_attributes(old_reports_count: (old_report_count.to_i+1), new_reports_count: (new_report_count.to_i+1), notified: false, failed_csv_url: "https://marketresearchfuture.com#{failed_data_file.split('public')[1]}", failed_reports: rows_failed)
  end
end
