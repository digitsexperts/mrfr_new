class Lead < ActiveRecord::Base
  establish_connection :crm_database
  
  after_save ThinkingSphinx::RealTime.callback_for(:lead)
end
