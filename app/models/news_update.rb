class NewsUpdate < ActiveRecord::Base

	default_scope { order('created_at DESC') }
	audited
	mount_uploader :news_image, NewsImageUploader
	belongs_to :category
  	after_save :send_email

   extend FriendlyId
   friendly_id :title, use: :slugged

  def normalize_friendly_id(string)
    super.split("-")[0..6].join("-")
  end
  
  def rss_publish_date
    self.date.strftime("%a, %d %b %Y %H:%M:%S") 
  end

  def get_description
	  description_html = self.description.gsub("&amp;"," and ").gsub("&nbsp;","")
	  description_html = ActionView::Base.full_sanitizer.sanitize(description_html)
	  description_text = description_html.gsub("&amp;"," and ")
	  description_text.html_safe[0..300]+"..."
  end
  
  def send_email
	  UserMailer.send_news_logs(self).deliver
  end

  def self.list
    Rails.cache.fetch("news-update-list") do
      NewsUpdate.select("id","news_image","title","slug").limit(4)
    end
  end

end
