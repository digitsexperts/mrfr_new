class Order < ActiveRecord::Base

belongs_to  :user
belongs_to  :shopping_cart ,   :foreign_key => :cart_id
has_many    :order_details, dependent: :destroy


def purchase
  response = process_purchase
  transactions.create!(:action => "purchase", :amount => price_in_dollars, :response => response)
  cart.update_attribute(:purchased_at, Time.now) if response.success?
  response.success?
end

def express_token=(token)
  write_attribute(:express_token, token)
  if new_record? && !token.blank?
    details = EXPRESS_GATEWAY.details_for(token)
    self.express_payer_id = details.payer_id

    self.first_name = details.params["first_name"]
    self.last_name = details.params["last_name"]
  end
end
def self.create_order(ip_address,user,payment_method_type,order_id,report_id,price,edition)
  report = Report.search("@(id) '#{report_id}'").first
  json_data = JSON::parse(user.to_json(:include =>
     [:customer => {:include => [:country]
      }])).merge("payment_method_type" => payment_method_type,
                 "report_title"=> report.title,
                 "price"=> price,
                 "edition"=>edition).to_json 
  # begin
  #   uri = URI.parse("http://crm.wiseguyreports.com/")
  #   http = Net::HTTP.new(uri.host, uri.port)
  #   request = Net::HTTP::Post.new("/CRM/v1/CreateContact")
  #   request.add_field('Content-Type', 'application/json')
  #   request.body = json_data
  #   response = http.request(request)
  # rescue
  #   puts "Error communicating with crm..."
  # end 
end

private

def process_purchase
  if express_token.blank?
    STANDARD_GATEWAY.purchase(price_in_dollars, credit_card, standard_purchase_options)
  else
    EXPRESS_GATEWAY.purchase(price_in_dollars, express_purchase_options)
  end
end

def standard_purchase_options
  {
    :ip => ip_address,
    :billing_address => {
      :name     => "Ryan Bates",
      :address1 => "123 Main St.",
      :city     => "New York",
      :state    => "NY",
      :country  => "US",
      :zip      => "10001"
    }
  }
end

def express_purchase_options
  {
    :ip => ip_address,
    :token => express_token,
    :payer_id => express_payer_id
  }
end

def validate_card
  if express_token.blank? && !credit_card.valid?
    credit_card.errors.full_messages.each do |message|
      errors.add_to_base message
    end
  end
end	
end
