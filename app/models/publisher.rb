class Publisher < ActiveRecord::Base
  mount_uploader :logo, LogoUploader
  mount_uploader :report_image, ReportImageUploader
  mount_uploader :report_background, ReportBackgroundUploader

  has_many :reports
  has_many :country_reports, through: :reports
  has_many :categories, through: :reports
  has_many :countries, {:through=>:reports, :source=>:country}
  has_many :regions, through: :country_reports
  has_many :continents, through: :country_reports

  extend FriendlyId
  friendly_id :name, use: :slugged

  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end

  def self.publisher_list
    Rails.cache.fetch("all-publishers") do
  	  Publisher.select("id","name","slug","logo","reports_count").order("name asc")
  	end
  end

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end

  def self.recount_report_count
    all.each do |publisher|
      new_reports_count = Report.search(:sql => {:select => 'reports.id'}, :with =>  {:upcoming => 0,:report_type_id => publisher.id}).count
      publisher.update_attribute('reports_count',new_reports_count)
    end
  end

  def self.set_latest_reports
    self.select(:id).each do |publisher|
      publisher.reports.update_all("latest_publisher_report = 0")
      latest_report = publisher.reports.order("case day when '' then str_to_date(CONCAT('01/',publish_month),'%d/%m/%Y') ELSE str_to_date(CONCAT(day,'/',publish_month),'%d/%m/%Y') END  DESC").
      limit(1).
      first
      latest_report.update_attribute("latest_publisher_report",true) if latest_report
    end
  end

end
