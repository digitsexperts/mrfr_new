class PurchaseInvoice < ActiveRecord::Base
   establish_connection :crm_database
   has_many    :purchase_invoice_details
end
