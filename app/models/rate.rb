class Rate < ActiveRecord::Base
	  def self.get_exchange_rate(currency)
    case currency
    when "GBP"
      gbp_exchange_rate = Rate.find_by_currency("gbp").exchange_rate
    when "EUR"
      eur_exchange_rate = Rate.find_by_currency("eur").exchange_rate
    when "JPY"
      yen_exchange_rate = Rate.find_by_currency("yen").exchange_rate
    when "INR"
      inr_exchange_rate = Rate.find_by_currency("inr").exchange_rate
    end
  end

  def self.todays_exchange_rate
    Rails.cache.fetch("exchange_rate-#{Date.today.to_s}") do
      self.select("currency","exchange_rate").group_by(&:currency)
    end
  end


  def self.get_rate
    puts "started cron"
    self.where(currency: "gbp").first.update_columns(exchange_rate: Money::Bank::GoogleCurrency.new.get_rate(:USD,:GBP).to_f)
    self.where(currency: "eur").first.update_columns(exchange_rate: Money::Bank::GoogleCurrency.new.get_rate(:USD,:EUR).to_f)
    self.where(currency: "yen").first.update_columns(exchange_rate: Money::Bank::GoogleCurrency.new.get_rate(:USD,:JPY).to_f)
    self.where(currency: "inr").first.update_columns(exchange_rate: Money::Bank::GoogleCurrency.new.get_rate(:USD,:INR).to_f)
    puts "Done"
  end
  
end
