class Region < ActiveRecord::Base
	extend FriendlyId
    friendly_id :name, use: :slugged
    has_and_belongs_to_many :countries
    has_many :region_reports, through: :countries, :class_name => "Report"
    has_many :reports
    has_many :continents, through: :countries
    has_many :categories, through: :region_reports
    has_many :publishers, through: :region_reports

    def self.all_regions
        Rails.cache.fetch("all_regions") do 
          Region.all.order("name asc")
        end
    end

    def self.get_country_ids_of_region(region_ids)
      Country.search(:with => {:region_ids =>region_ids},:per_page=> Country.search(:sql=>{:select=>"countries.id"}).total_entries).map(&:id) rescue []    
    end

    def report_count
      Rails.cache.fetch("region-#{self.id}-report-count") do 
        self.region_reports.uniq.count
      end
    end

  def self.recount_report_count
    all.each do |region|
      country_ids = Country.search(:with => {:region_ids => region.id},:per_page => 300).map(&:id)
      new_reports_count = Report.search(:sql => {:select => 'reports.id'}, :with =>  {:upcoming => 0,:country_id => country_ids}).count
      region.update_attribute('reports_count',new_reports_count)
    end
  end
end
