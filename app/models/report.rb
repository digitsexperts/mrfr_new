class Report < ActiveRecord::Base
  require 'RMagick'
  include Magick
  include ActionView::Helpers::TextHelper
  default_scope { where(active: true) }

  has_many :country_reports
  has_many :countries, :through => :country_reports
  
	belongs_to :category
  delegate :name, :to => :category, :prefix => true
	belongs_to :report_type
  delegate :name, :to => :report_type, :prefix => true
	belongs_to :country
	belongs_to :region
	belongs_to :continent
	has_many   :order_details
  has_many :regions , :through=> :countries
  has_many :continents, :through=> :countries
  has_one :root_category, ->(category) { where(category_id: category.root.id)}, class_name: "Category"
  has_one :country_global, -> {where("country_id='Global'")}, class_name: "Report", :foreign_key=>"id"

	mount_uploader :report_image, ReportImageUploader
	mount_uploader :report_pdf, ReportPdfUploader

  scope :upcoming, -> { where("upcoming = ?",true) }
  scope :released, -> { where("upcoming = ?",false) }

  scope :global, -> { where("country_id=?", "Global") }
  scope :deactivated, -> {  where(active: false)  }

  REPORT_TYPES = ["Upcoming","Top Selling","Latest Report"]

	extend FriendlyId
  friendly_id :slug_candidates, use: :slugged
  after_save  :generate_report_code

  def rss_publish_date
    self.publish_month.to_date.strftime("%b %Y") 
  end

	def price_modification=(new_price)
	  @price_modification = new_price
	  if new_price.to_s.ends_with? "%"
	    self.one_user += (one_user * (new_price.to_d/100)).round(2)
	  else
	    self.one_user = new_price
	  end
	end

	def self.options_for_sorted_by
	  [ 
	  	['Latest Date', 'created_at_desc'],
	  	['Oldest Date first', 'created_at_asc'],
	    ['Title (a-z)', 'title_asc'],
	    ['Title (z-a)', 'title_desc'],
	    ['Price (high-low)', 'price_at_desc'],
	    ['Price (low-high)', 'price_at_asc']
	  ]
    end

    def self.options_for_select_price
      [ 
  	  	['Up to $1000', 'less_$1000'],
  	  	['$1000 - $2500', '$1000-$2500'],
  	    ['$2500 - $5000', '$2500-$5000'],
  	    ['More than $5000', 'more_$5000']
	    ]
    end

    def get_description
      description_html = self.description.gsub("&amp;"," and ").gsub("&nbsp;","")
      description_html = ActionView::Base.full_sanitizer.sanitize(description_html)
      description_text = description_html.gsub("&amp;"," and ")
      description_text.html_safe[0..300]+"..."
    end

    def self.related_report(category_id)
      Rails.cache.fetch("related-reports-#{category_id}") do
        Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"},:with => {:category_id => category_id, :upcoming => 0}, :order => "publish_date DESC", :limit => 10).to_a
      end
    end

    def self.upcoming_reports
    	Rails.cache.fetch("upcoming-reports") do
        Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"},:with => {:upcoming => 1}, :order => "publish_date DESC", :limit => 10).to_a
    	end
    end

    def self.latest_reports
    	Rails.cache.fetch("latest-reports") do
        Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"},:with => {:upcoming => 0}, :order => "publish_date DESC", :limit => 4).to_a
    	end
    end

    def self.topselling
    	Rails.cache.fetch("topselling-reports") do
    	  Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"},:with => {:latest_report_type_report => 1}, :order => "publish_date DESC", :limit => 10).to_a
    	end
    end

    def self.get_report_count(continent_id)
      Rails.cache.fetch("count-of-reports-#{continent_id}") do
        continent = Continent.find(continent_id)
        report = Report.from("(#{continent.reports.to_sql} UNION #{continent.country_reports.to_sql}) AS reports")
        return report.count
      end
    end
 
    def self.get_country_reports(country_id)
      Rails.cache.fetch("get-country-reports-#{country_id}") do
        Report.search(:conditions => {:country_name=> country_id},:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as report_image, report_types.report_image as report_type_report_image, report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"},:with => {:latest_report_type_report => 1}, :order => "publish_date DESC", :limit => 10).to_a
      end
    end

    def self.get_continent_reports(continent_id)
      Rails.cache.fetch("get-continent-reports-#{continent_id}") do
    	  continent = Continent.find(continent_id)
    	  Report.from("(#{continent.reports.to_sql} UNION #{continent.country_reports.to_sql}) AS reports").joins(:report_type).select("reports.country_id,reports.id,reports.report_type_id","title","description","day","publish_month","one_user","report_image","report_types.name as report_type_name","slug","report_types.report_image as report_type_report_image,reports.category_id")
      end
    end

    def self.get_region_reports(region_id)
      Rails.cache.fetch("get-region-reports-#{region_id}") do
        region = Region.find(region_id)
    	  country_ids = region.countries.pluck(:id)
        new_reports_count = Report.search(:sql => {:joins => [:report_type],:select => "reports.id,reports.report_type_id, reports.title, reports.day, reports.publish_month, reports.slug, reports.one_user, reports.report_image as report_image, report_types.report_image as report_type_report_image, reports.upcoming,reports.category_id"}, :order => "publish_date DESC", :with => {:country_id => country_ids}).count
      end
    end

    def self.cached_global
      Rails.cache.fetch("get-global-reports") do
        Report.search(:conditions => {:country_name=>'Global'},:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"}, :order => "publish_date DESC", :limit => 10).to_a
      end
    end
    
    def self.get_report_type_reports(report_type_id,page)
      require 'will_paginate/array'
      Rails.cache.fetch("get-report_type-reports-#{report_type_id}-#{page}") do
        Report.search(:with => {:report_type_id => report_type_id}, :sql => {:joins => [:report_type],:select => "reports.id,reports.report_type_id, reports.title, reports.day, reports.publish_month, reports.slug, reports.one_user, reports.report_image as report_image, report_types.report_image as report_type_report_image, reports.upcoming,reports.category_id"}, :order => "publish_date DESC").paginate(:page => page, :per_page => 5)
      end
    end

    def self.get_topselling_report_type_reports(report_type_id)
      Rails.cache.fetch("get-topselling-report_type-reports-#{report_type_id}") do
        Report.search(:with => {:report_type_id => report_type_id}, :sql => {:joins => [:report_type],:select => "reports.id,reports.report_type_id, reports.title, reports.day, reports.publish_month, reports.slug, reports.one_user, reports.report_image as report_image, report_types.report_image as report_type_report_image, reports.upcoming,reports.category_id"}, :order => "selling_count DESC", :limit => 5).to_a
      end
    end

    def self.get_news_related_reports(news_id)
      Rails.cache.fetch("get-newsrelated-reports-#{news_id}") do
      	news_updates = NewsUpdate.find(news_id)
      	Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name"},:with => {:category_id => news_updates.category_id},:limit => 10).to_a
      end
    end
    
    def generate_report_code
       Report.where("code IS NULL").each do |report|
        report_last = Report.where("code IS NOT NULL").order("cast(code as unsigned) desc").limit(1).first
        if report_last.present? && report_last.code.present? 
          last_vendorid_number = report_last.code.to_i
          new_vendorid_number = last_vendorid_number + 1
        else
          new_vendorid_number = 1
        end
        report.update_columns(code: new_vendorid_number)
      end
    end


    # def report_code
    #   "MRFR/"+"#{self.category.abbreviation}"+"/"+"#{self.id.to_s}"+"-"+"#{self.report_type.abbreviation}"
    # end

    def slug_candidates
      [:title, [:title, :id_for_slug]]
    end


    def normalize_friendly_id(string)
      (super.split("-")[0..5] + super.split("-").last(5)).join("-")
    end


    def should_generate_new_friendly_id?
      new_record? || self.slug.nil?
    end

    def url
      "https://marketresearchfuture.com/reports/#{self.slug}"
    end

    # FOR SPHINX ORDER bY
    def self.sphnix_sort_by_other(order_pattern)
      query = case order_pattern
      when "Latest Date"
        "publish_date desc"
      when "Oldest Date"
        "publish_date asc"
      when "Title ( A – Z )"
        "title asc"
      when "Title ( Z – A )"
        "title desc"
      when "Price ( High to Low )"
        "one_user desc"
      when "Price ( Low to High )"
        "one_user asc"   
      when "Less Than 3 Month"
        'publish_date DESC'
      when "Less Than 6 Month"
        'publish_date DESC'
      when "Less Than 1 Year"
        'publish_date DESC'
      when "Less Than One Month"
        'publish_date DESC'
      when "New Arrivals"
        "publish_date desc"
      when "Less Than One Month"
        "publish_date desc"
      when "Less Than 3 Month"
        "publish_date desc"
      when "Less Than 6 Month"
        "publish_date desc"
      when "Less Than 1 Year"
      end
      query
    end

    def self.sphinx_sort_dropdown(pattern)
       query = case pattern
      when "DatePublished_Desc"
        "publish_date desc"
      when "Price_Desc"
        "one_user desc"
      when "DatePublished_Asc"
        "publish_date asc"
      when "Price_Asc"
        "one_user asc"
      when "Relevance"
      end
      query
    end

    # FOR SPHINX Report Type
    def self.sphnix_get_report_type_query(report_type)
      query = case report_type
      when "Published Reports"
        ":upcoming => 0"
      when "Upcoming Reports"
        ":upcoming => 1"
      when "All Reports"
        ""
      end
      query
    end

    #FOR SPHINX price filter

    def self.sphinx_get_price_query(min_price,max_price)
      if min_price.present? && max_price.present?
        ":one_user=>#{min_price.to_f}..#{max_price.to_f}"
      elsif min_price.present? && max_price.nil?
        ":one_user=>#{min_price.to_f}..50000.0"
      elsif min_price.nil? && max_price.present?
        ":one_user=>#(0.0)..{max_price.to_f}"
      end          
    end

    def self.generate_country_ids_query(country_ids)
      "countries_reports.country_id in (#{country_ids.map(&:to_i).join(",")})"
    end
   

    def self.get_country_ids(params)
      country_ids = params["country_ids"]
      country_ids = params["country_ids"] - ["Global"] if params["country_ids"].present? && params["country_ids"].include?("Global")
      
      all_country_ids = if (params["sort_by_continent"].present? && country_ids.present?)
        Continent.get_country_ids_of_continents(params["sort_by_continent"].map(&:to_i)) + country_ids.map(&:to_i)
      elsif (params["sort_by_region"].present? && country_ids.present?)
        Region.get_country_ids_of_region(params["sort_by_region"].map(&:to_i)) + country_ids.map(&:to_i)
      elsif country_ids.present?
        country_ids.map(&:to_i)
      elsif params["sort_by_region"].present?
        Region.get_country_ids_of_region(params["sort_by_region"].map(&:to_i))
      elsif  params["sort_by_continent"].present? && !params["sort_by_continent"].include?('Global')
        Continent.get_country_ids_of_continents(params["sort_by_continent"].map(&:to_i))  
      else
        []
      end
      all_country_ids
    end  

    def self.get_published_date_query(publish_date)
      query = case publish_date
      when "New Arrivals"
        last_publish_date = Report.search(:with=>{upcoming:0,active:1},:order=>"publish_date desc",:per_page=>1).last.publish_date
        last_date = Date.parse(last_publish_date.to_s)
        start_date = last_date -2.days
        (start_date..last_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
        #(7.day.ago.to_date..Time.now.to_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
      when "Less Than One Month"
        (1.month.ago.to_date..Time.now.to_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
      when "Less Than 3 Month"
        (3.month.ago.to_date..Time.now.to_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
      when "Less Than 6 Month"
        (6.month.ago.to_date..Time.now.to_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
      when "Less Than 1 Year"
        (1.year.ago.to_date..Time.now.to_date).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i)
      else
        []
      end
      return query      
    end 

    # sphinx fileter_reports

    def self.filter_reports(params)
      with_query = []
      conditions_query = []
      order_query = []
      with_query << (":report_type_id => #{params[:report_type_ids].map(&:to_i)}") if params["report_type_ids"].present?
      with_query << "#{Report.sphnix_get_report_type_query(params[:report_type])}" if params["report_type"].present?
      # order_query << "#{Report.sphnix_sort_by_other(params[:published_date])}" if params[:published_date].present?
      # order_query << "#{Report.sphnix_sort_by_other(params[:sort_by_other])}" if params["sort_by_other"].present?
      order_query << "#{Report.sphinx_sort_dropdown(params[:sort_option])}" if params["sort_option"].present?

      price = params["sort_by_price"].split("..")
      if eval(price.first).present? || eval(price.last).present?
        min_price = eval(price.first)
        max_price = eval(price.last) 
        with_query << "#{Report.sphinx_get_price_query(min_price,max_price)}"
      end
      with_query << ":report_publish_date=>#{Report.get_published_date_query(params[:published_date])}" if params[:published_date].present? && !["Latest Date","Oldest Date"].include?(params["published_date"])
      
      with_query << ":category_id=>#{Category.get_category_and_subcategory(params[:sort_by_category].map(&:to_i))}" if params["sort_by_category"].present?  

      with_query << ":country_id=>#{Report.get_country_ids(params)} " if (params["sort_by_continent"].present? || params["country_ids"].present? || params["sort_by_region"].present?)
      conditions_query << ":country_name=>'Global'" if params["sort_by_continent"].present? && params["sort_by_continent"].include?("Global")
      
      conditions_query = conditions_query.select{|query| query!=""}
      conditions_hash = eval("Hash(#{conditions_query.join(',')})") rescue {}
      with_query = with_query.select{|query| query!=""}
      order_query = order_query.select{|query| query!=""}
      order_query = order_query.join(",")

      with_hash = eval("Hash(#{with_query.join(',')})") rescue {}

      per_page = params[:per_page].present? ? params[:per_page].to_i : 10
      if params["search_term"].present?
        Report.search(sphinx_search_sql(params["search_term"]),:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},:conditions=>conditions_hash,:with =>with_hash,:order=>order_query,:page => params[:page], :per_page => per_page)
      else
        Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.report_type_id,reports.category_id"},:conditions=>conditions_hash,:with =>with_hash,:order=>order_query,:page => params[:page], :per_page => per_page)
      end
    end

    #---------Code for press release ends -----------
    
    def self.released
      Rails.cache.fetch("released-reports") do
        where("upcoming = ?",false)
      end
    end

    def self.latest_report_type_reports
      Rails.cache.fetch("latest-report_type-reports") do
        search(:with => {:latest_report_type_report => 1}).to_a
      end
    end

    def self.report_type_name_image_hash
      Rails.cache.fetch("report_type_name_images") do
        report_type.all.inject({}) {|i,n| i.merge!({n.id => {:logo => n.report_image_url.nil? ? n.logo_url(:thumb) : n.report_image_url, :name => n.name, :slug => n.slug, :report_background_url => n.report_background_url, :text_position => n.text_position, :text_color => n.text_color} }) }
      end
    end

    def self.client_name_image_hash
      Rails.cache.fetch("client_name_images") do
        Client.all.inject({}) {|i,n| i.merge!({n.id => {:logo => n.client_logo_url(:thumb), :name => n.name, :slug => n.slug} }) }
      end
    end

    def rates
      Rails.cache.fetch("report-#{self.id}-rates-#{Date.today.to_s}") do
        exchange_rate = Rate.todays_exchange_rate
      
        gbp_exchange_rate = exchange_rate["gbp"][0].exchange_rate
        yen_exchange_rate = exchange_rate["yen"][0].exchange_rate
        eur_exchange_rate = exchange_rate["eur"][0].exchange_rate
        inr_exchange_rate = exchange_rate["inr"][0].exchange_rate

        {
          "yen" => {
               "one_user" => (self.one_user*yen_exchange_rate rescue nil),
               "five_user" => (self.five_user*yen_exchange_rate rescue nil),
               "site_user" => (self.site_user*yen_exchange_rate rescue nil),
               "enterprise_user"=> (self.enterprise_user*yen_exchange_rate rescue nil)
            },
          "gbp" => {
               "one_user" => (self.one_user*gbp_exchange_rate rescue nil),
               "five_user" => (self.five_user*gbp_exchange_rate rescue nil),
               "site_user" => (self.site_user*gbp_exchange_rate rescue nil),
               "enterprise_user"=> (self.enterprise_user*gbp_exchange_rate rescue nil)
            },
          "inr" => {
               "one_user" => (self.one_user*inr_exchange_rate rescue nil),
               "five_user" => (self.five_user*inr_exchange_rate rescue nil),
               "site_user" => (self.site_user*inr_exchange_rate rescue nil),
               "enterprise_user"=> (self.enterprise_user*inr_exchange_rate rescue nil)
            },
          "eur" => {
               "one_user" => (self.one_user*eur_exchange_rate rescue nil),
               "five_user" => (self.five_user*eur_exchange_rate rescue nil),
               "site_user" => (self.site_user*eur_exchange_rate rescue nil),
               "enterprise_user"=> (self.enterprise_user*eur_exchange_rate rescue nil)
            }
        }
      end
    end


    def self.search_report(search_term,page=1)
      with_query = []
      with_query << (":report_type_id => #{search_term[:report_type_ids]}") if search_term["report_type_ids"].present?
      with_query << (":upcoming => 0") 
      with_hash = eval("Hash(#{with_query.join(',')})") rescue {}
      if search_term[:q].present?
        Report.search(sphinx_search_sql(search_term[:q]),:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"}, :with => with_hash, :order => "publish_date desc, one_user desc",:ranker => :proximity, :page => page, :per_page => 10)
        #Report.search("@(title) '#{Riddle::Query.escape(search_term)}'",:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id"}, :order => "publish_date desc, one_user desc", :with => {:upcoming => 0},:ranker => :proximity, :page => page, :per_page => 10)
      else
        Report.search(:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name, report_types.id as report_type_id,reports.category_id"}, :with => {:upcoming => 0}, :order => "publish_date desc, one_user desc", :page => page, :per_page => 10)
      end
    end

    def self.admin_filter(params)
      if params['report'].present?
        publish_date = if params['report']['day'].present? && params['report']['publish_month'].present?
          year_month = params['report']['publish_month'].gsub("/","") 
          "#{year_month}#{(sprintf '%02d', params['report']['day'])}" 
        elsif !params['report']['day'].present? && params['report']['publish_month'].present?
          (params['report']['publish_month'].to_date.beginning_of_month..params['report']['publish_month'].to_date.end_of_month).map{ |date| date.strftime("%Y%m%d") }.map(&:to_i).join("|")
        else
          ''
        end
        # temp_dates = []
        
        # if !publish_date.present?
        #   ((Time.now.year-10)..(Time.now.year+1)).to_a.each do |year|
        #     temp_dates <<  "#{year}#{publish_date}"
        #   end
        #   publish_date = temp_dates.join("|")
        # end

        publish_date.class.to_s == "Array" ? publish_date.map(&:to_i) : publish_date.to_i
        sql = nil
        title = params[:report][:title].present? ? "#{params[:report][:title]}" : ''
        report_types_name = params[:report][:report_type_name].present? ? "#{params[:report][:report_type_name]}*" : ''
        report_type_ids = report_type.search("@name #{report_types_name}",:sql=>{:select=>"id"}).map(&:id) rescue [] if report_types_name.present?
        sql = "@(title,publish_date) '#{title}' #{publish_date}" if params[:report][:title].present? || publish_date.present?
        
        with_query = {:upcoming => 0}
        with_query.merge!({:report_type_id=>report_type_ids}) if report_type_ids.present?
        with_query.merge!({:one_user => (params["report"]["one_user"].to_f..params["report"]["one_user"].to_f)}) if params["report"]["one_user"].present?  
        Report.search(sql.to_s,:with => with_query,:per_page=>10,:page=>params[:page])
      else
        Report.search(:per_page=>10,:page=>params[:page])
      end
    end

    def self.sphinx_search_sql(search_term)
      special_chars = ['~','!','@','#','$','%','^','&','*','(',')','_','\+','=','-','[',']','{','}','|',':','\;','<','>','?','/','.',';']
      search_term = search_term.is_a?(Array) ? search_term.first : search_term
      search_term = search_term.gsub(';','') if search_term.to_s.include?(';')
      search_term = search_term.gsub(':','') if search_term.to_s.include?(':')
      if search_term && search_term.starts_with?("WGR")
        "@id #{search_term.gsub('WGR','')}"
      elsif search_term && !search_term.starts_with?("WGR")
        term_combinations = []
        term_combinations << "'#{Riddle::Query.escape(search_term)}'"
        term_combinations << Riddle::Query.escape(search_term).split(' ').map{|t| "'#{t}'"}
        # escaped_string = Riddle::Query.escape(search_term)
        # term_combinations = []
        # term_combinations << Riddle::Query.escape("'#{search_term}'")
        # term_combinations << Riddle::Query.escape("'#{search_term}*'")
        # while escaped_string.present?
        #   term_combinations << Riddle::Query.escape("'*#{escaped_string}*'")
        #   escaped_string = escaped_string[0...escaped_string.rindex(' ')] rescue nil
        # end
        term_combinations.flatten!
        term_combinations.delete_if {|term| (term.length == 4) && term.start_with?("\'\\")}
        term_combinations.join(' | ').prepend('@(title,slug) ')
      else
        search_term
      end
    end


    def self.search_topselling(search_term,page)
      Report.search(sphinx_search_sql(search_term),:sql => {:joins => [:report_type], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage,  report_types.name as report_type_name,reports.category_id", :order => "selling_count DESC"},:page => page, :per_page => 10)
    end

    def self.global_count
      Rails.cache.fetch('global-count') do
        Report.search(:conditions=>{:country_name=>"Global"}, :with => {:upcoming => 0}, :sql => {:select => "reports.id"}).count
      end
    end

    def self.set_incoming
      Report.update_all("upcoming = false")
      Report.where("date(created_at) between ? and ?",Date.today,Date.today+7.days).update_all("upcoming = true")
    end

    def self.create_report_codes

       Report.where("code IS NULL").each do |report|
        report_last = Report.where("code IS NOT NULL").order("cast(code as unsigned) desc").limit(1).first
        if report_last.present? && report_last.code.present? 
          last_vendorid_number = report_last.code.to_i
          new_vendorid_number = last_vendorid_number + 1
        else
          new_vendorid_number = 1
        end
        report.update_columns(code: new_vendorid_number)
      end
    end

    def self.fix_bad_format
      Report.where("slug like ?","%_x000d_%").update_all("old_slug = slug")
      Report.where("title like ?","%_x000D_%").update_all("title = REPLACE (title, '_x000D_', '')")
      Report.where("table_of_content like ?","%_x000D_%").update_all("table_of_content = REPLACE (table_of_content, '_x000D_', '')")
      Report.where("description like ?","%_x000D_%").update_all("description = REPLACE (description, '_x000D_', '')")
      Report.where("slug like ?","%_x000d_%").update_all("slug = REPLACE (slug, '_x000d_', '')")
    end

    def self.remove_duplicates
      result=Report.select("reports.id, slug, count(slug) as c").group("slug").having("count(slug) > 1")
      puts "Duplicate count: "+result.length.to_s
      result.each_with_index do |report,index|
        puts "deleting "+index.to_s
        duplicates = Report.where('slug = ?',report.slug).limit((report.c - 1)).pluck(:id)
        Report.where(:id => duplicates).delete_all
      end
    end

    def self.clear_fragment_cache
      ActionController::Base.new.expire_fragment('admin-all-categories')
      ActionController::Base.new.expire_fragment(/admin-report_type-list/)
      ActionController::Base.new.expire_fragment(/allreport_types/)
      ActionController::Base.new.expire_fragment('admin-left-menu')
      ActionController::Base.new.expire_fragment('blogs-breadcrumb')
      ActionController::Base.new.expire_fragment('all_blog_categories')
      ActionController::Base.new.expire_fragment('scoop-it')
      ActionController::Base.new.expire_fragment('blog-right-panel')
      ActionController::Base.new.expire_fragment('allcategories')
      ActionController::Base.new.expire_fragment('allcontinents')
      ActionController::Base.new.expire_fragment('all_countries')
      ActionController::Base.new.expire_fragment('rss_allcontinents')
      ActionController::Base.new.expire_fragment('rss_all_countries')
      ActionController::Base.new.expire_fragment('rss_allregions')
      ActionController::Base.new.expire_fragment('all_categories')
      ActionController::Base.new.expire_fragment('upcoming_reports')
      ActionController::Base.new.expire_fragment('topselling_reports')
      ActionController::Base.new.expire_fragment('latest_reports')
      ActionController::Base.new.expire_fragment('blog-header')
      ActionController::Base.new.expire_fragment('allreport_types')
      ActionController::Base.new.expire_fragment('allregions')
      ActionController::Base.new.expire_fragment('report_by_type')
    end


    def self.total_count
      Rails.cache.fetch("total_count") do
        Report.count
      end
    end

    def self.recount_total
      Rails.cache.delete("total_count")
      total_count
    end

    def self.category_name_image_hash
     Rails.cache.fetch("category_name_images") do
       Category.all.inject({}) {|i,n| i.merge!({n.id => {:logo => n.category_report_image.present? ? n.category_report_image.url : nil , :name => n.name,:icon => n.category_icon.url} }) }
     end
    end

    def self.report_background_image
      Report.all.each do |report|
        if Report.report_type_name_image_hash[report.report_type_id][:report_background_url].present? && !FileTest.exist?("#{Rails.root}/public/#{report.id}.png")
          img = ImageList.new(Report.report_type_name_image_hash[report.report_type_id][:report_background_url])
          txt = Draw.new
          position = Report.report_type_name_image_hash[report.report_type_id][:text_position]
          text_color = Report.report_type_name_image_hash[report.report_type_id][:text_color]
          obj = Wrap.new
          report_name = obj.warp_method(report.title)
          img.annotate(txt, 0,0,0,25, "#{report_name}"){

            txt.font_family = 'Helvetica'
            txt.fill = text_color
            txt.pointsize = 28
            txt.gravity = ("Magick::"+position).constantize
          }

          img.format = 'png'
          new_file=File.new("#{Rails.root}/public/#{report.id}.png", 'wb')
          new_file.write(img.to_blob)

          report_img = Magick::Image.read("#{Rails.root}/public/#{report.id}.png").first
          img_small = report_img.resize_to_fit(115,151)
          small_file=File.new("#{Rails.root}/public/#{report.id}_small.png", 'wb')
          small_file.write(img_small.to_blob)
        end
      end
    end

    def self.relevant_report_categories(term)
      
      #Rails.cache.fetch("relevant-categories-#{report_type_id}") do
      
      #end
    end

    def relevant_reports
      
      reports = []
      term = self.title.gsub!(/[^0-9A-Za-z]/, ' ')
      replacements = [ [" a "," "],[" an "," "],[" and ", " "],[" the "," "]]
      replacements.each {|replacement| term.gsub!(replacement[0], replacement[1])}
      term = term.split.join('|')
      # Rails.cache.fetch("relevant-reports-#{id}") do
        # categories = Category.search("@report_title #{term}",limit: 5).to_a
        # categories.each do |cat|
        #   reports << [cat.name,.to_a]
        # end
      # end
      reports = Report.search("@title #{term}", limit: 5)

    end

end

  


require 'action_view'
class Wrap
  include ActionView::Helpers::TextHelper
  def warp_method(report_title)
    word_wrap(report_title, :line_width => 30)
  end
end