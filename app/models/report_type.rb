class ReportType < ActiveRecord::Base
  mount_uploader :report_type_image, ReportTypeImageUploader
  has_many :reports
  extend FriendlyId
  friendly_id :name, use: :slugged

  def should_generate_new_friendly_id?
    name_changed?
  end
  
  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end

  def self.recount_report_count
    all.each do |type|
      new_reports_count = Report.search(:sql => {:select => 'reports.id'}, :with =>  {:upcoming => 0,:report_type_id => type.id}).count
      type.update_attribute('reports_count',new_reports_count)
    end
  end

  def self.report_type_list
    Rails.cache.fetch("all-report_types") do
      ReportType.select("id","name","slug","abbreviation","reports_count").order("name asc")
    end
  end

  # def self.set_latest_reports
  #   self.select(:id).each do |type|
  #     type.reports.update_all("latest_publisher_report = 0")
  #     latest_report = type.reports.order("case day when '' then str_to_date(CONCAT('01/',publish_month),'%d/%m/%Y') ELSE str_to_date(CONCAT(day,'/',publish_month),'%d/%m/%Y') END  DESC").
  #     limit(1).
  #     first
  #     latest_report.update_attribute("latest_publisher_report",true) if latest_report
  #   end
  # end

  def relevant_reports
    Rails.cache.fetch("relevant-reports-report-type-#{id}") do
      reports = []
      categories = Category.search("@report_type_id #{self.id}",limit: 5).to_a
      categories.each do |cat|
        reports << [cat,Report.search(with: {report_type_id: self.id,category_id: cat.id},sql: {order: "created_at desc"}, limit: 3).to_a]
      end
      reports
    end
  end

  def related_categories
    Rails.cache.fetch("relevant-reports-report-type--category-#{id}") do
      Category.search("@report_type_id #{self.id}").to_a.uniq
    end
  end
end
