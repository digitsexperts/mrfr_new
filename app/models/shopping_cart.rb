class ShoppingCart < ActiveRecord::Base
  acts_as_shopping_cart

  has_many   :orders,   :foreign_key => :cart_id
  belongs_to :user
 


  def add(object, price, quantity = 1, cumulative = true,purchase_type)
    cart_item = item_for(object)
    shopping_cart_items.create(:item => object, :price => price, :quantity => quantity,:purchase_type => purchase_type)
  end

  def total
    self.shopping_cart_items.collect {|item| item.discounted_price.nil? ? (item.quantity*item.price) : (item.quantity*item.discounted_price) }.sum
  end

  def create_order(ip_address,user,payment_method_type,order_id)
    require 'net/http'
    report_arr = []
    user_order = user.orders.create(:order_status=>'PENDING', :ip_address => ip_address, :order_total => self.total,:payment_method_type => payment_method_type)
    contact = Contact.create(:first_name=> user.first_name, :last_name => user.last_name,:primary_email=>user.email,
                   :mobile_phone=>user.customer.phone_no,:department=>user.customer.job_title)
     ContactAddressDetail.create(:mailing_city=> user.customer.city,:mailing_zip=>user.customer.zip_code,:mailing_country_id=>user.customer.country_id,:contact_id => contact.id)
     purchase_invoice = PurchaseInvoice.create(:contact_id=>contact.id,:status=>"paid",:payment_method_type=>payment_method_type,:email=>user.email)
    self.shopping_cart_items.each do |item|
      if OrderDetail.exists?(report_id: item.item_id, order_id: user_order.id)        
        order_detail = OrderDetail.where(order_details_status: 'PENDING',report_id: item.item_id, order_id: user_order.id).first
        order_detail.update_attribute("quantity", (order_detail.quantity+item.quantity))
      else
        order_detail=OrderDetail.create(order_details_status: 'PENDING', report_id: item.item_id, report_value: item.price,
          quantity: item.quantity, order_id: user_order.id)
      end
      @report = Report.find(item.item_id)
      @report.increment!(:selling_count) 
       PurchaseInvoiceDetail.create(:purchase_invoice_details_status=>"paid", :report_title=>@report.title, :report_value=>item.price,
                   :report_license_type=>item.purchase_type[0..-5], :discounted_price=>item.discounted_price, :quantity=> item.quantity,:purchase_invoice_id=>purchase_invoice.id)
    end
    self.shopping_cart_items.each do |i|
      report_arr << i.item_id
    end
      # report_list = Report.where(id: self.shopping_cart_items.pluck(:item_id))
      #UserMailer.send_report_pdf(user,report_list,order_id,self,purchase_invoice.id).deliver

 
       # uri = URI("http://216.157.85.174/api/purchase_invoices/send_reports")
       # # uri = URI("http://localhost:3000/api/purchase_invoices/send_reports")
       # Net::HTTP.post_form(uri, {:id => purchase_invoice.id, :user_id => user.id, "report_list[]" => report_arr,:order_id => order_id,:cart => self.id }) 

  end


  def self.load(shopping_cart_id)
    shopping_cart_id = create.id unless shopping_cart_id
      JSON.parse(find(shopping_cart_id).to_json(
        :only => [:id,:currency],
        :include => {
                      :shopping_cart_items => 
                      {:only => [:id,:item_id,:quantity,:purchase_type,:price,:discounted_price,:discount], 
                        :include => 
                        {:item => 
                          { :only => [:id,:title,:report_image, :slug, :day, :publish_month],
                            :include => {
                                          :report_type => {:only => [:name]
                                                        },
                                          :category => {:only => [:id]
                                                        } 
                                        }
                          }
                        },
                        :methods => [:currency]
                      }
                    },
                    :methods => [:total,:subtotal]                          )
                )
  end


end



