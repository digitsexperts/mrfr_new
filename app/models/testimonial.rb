class Testimonial < ActiveRecord::Base
    mount_uploader :company_logo, CompanyLogoUploader
	def self.list
      Rails.cache.fetch("testimonial-list") do
      	Testimonial.select("id","message","name","position").limit(2)
      end
	end

	
end
