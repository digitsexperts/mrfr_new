class User < ActiveRecord::Base
  
  acts_as_authentic do |c|
    c.login_field = 'email'
    c.validate_login_field = false;
    c.validates_length_of_password_field_options( :minimum => 6, :on => :update, :if => :password_changed? )
  end 
  has_one     :customer , dependent: :destroy
  has_one     :shopping_cart
  has_many    :orders , dependent: :destroy
  has_many    :order_details, through: :orders
  has_many    :reports,  through: :order_details
  has_many    :categories, through: :reports
  has_many    :authorizations, :dependent => :destroy
  extend FriendlyId
  friendly_id :full_name, use: :slugged
  serialize :omniauth_data, JSON

  accepts_nested_attributes_for :customer

  
  def full_name
    first_name.to_s.capitalize + " " + last_name.to_s.capitalize
  end
  
  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    #UserMailer.delay(run_at: Time.now).password_reset(self)
    UserMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def self.create_from_omniauth_data(omniauth_data)
    user = User.new(
      :first_name => omniauth_data['info']['first_name'].to_s.downcase,
      :last_name => omniauth_data['info']['last_name'].to_s.downcase,
      :email => omniauth_data['info']['email'].to_s.downcase,
      :password => (0...50).map { ('a'..'z').to_a[rand(26)] }.join,
      :single_access_token => omniauth_data.extra.access_token.token
      )
    user.omniauth_data = omniauth_data.to_json #shove OmniAuth::AuthHash as json data to be parsed later!

    user.save(:validate => false) #create without validations because most of the fields are not set.
    user.reset_persistence_token! #set persistence_token else sessions will not be created
    user
  end

  def self.build_cache
    puts "caching categories..."
    Category.all.each do |category|
      Rails.cache.delete("related-reports-#{category.id}")
      Report.related_report(category.id)
    end

    Rails.cache.delete("all-categories")
    Category.get_all_categories
    Rails.cache.delete("category-hash")
    Category.category_hash

    puts "caching continents..."
    Continent.list
    Rails.cache.delete("continent-list")
    Continent.all.each do |continent|
      Rails.cache.delete("get-continent-reports-#{continent.id}")
      Report.get_continent_reports(continent.id)
    end
    
    puts "caching countries...."
    Rails.cache.delete("all-countries")
    Country.get_all_countries
    Rails.cache.delete("country-list")
    Country.list
    Country.select("id,abbreviation").each do |country|
      Rails.cache.delete("get-country-reports-#{country.id}")
      Report.get_country_reports(country.abbreviation)
    end

    puts "caching regions..."
    Rails.cache.delete("all_regions")
    Region.all_regions
    Region.select("id").each do |region|
      Rails.cache.delete("region-#{region.id}-report-count")
      region.report_count
      Rails.cache.delete("get-region-reports-#{region.id}")
      Report.get_region_reports(region.id)
    end
    puts "caching reports...."
    Rails.cache.delete("upcoming-reports")
    Report.upcoming_reports
    Rails.cache.delete("latest-reports")
    Report.latest_reports
    Rails.cache.delete("topselling-reports")
    Report.topselling
    Rails.cache.delete("get-global-reports")
    Report.cached_global
    puts "caching publishers......"
    Publisher.select("id").each do |publisher|
      (1..10).to_a.each {|page_index| 
        Rails.cache.delete("get-publisher-reports-#{publisher.id}-#{page_index}")
        Report.get_publisher_reports(publisher.id,page_index)
      }
      Rails.cache.delete("get-topselling-publisher-reports-#{publisher.id}")
      Report.get_topselling_publisher_reports(publisher.id)
    end

    puts "caching news updates..."
    Rails.cache.delete("news-update-list")
    NewsUpdate.list
    NewsUpdate.where("category_id != NULL").select("id").each do |news|
      Rails.cache.delete("get-newsrelated-reports-#{news.id}")
      Report.get_news_related_reports(news.id)
    end
    puts "caching testimonials...."
    Rails.cache.delete("testimonial-list")
    Testimonial.list

    puts "Delete global count cache"
    Rails.cache.delete('global-count')
    Report.global_count
    Rails.cache.delete("tweet-list")
  end
end
