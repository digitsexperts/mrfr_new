class UserSession < Authlogic::Session::Base
  generalize_credentials_error_messages "Your login information is invalid"
  last_request_at_threshold = 10.minutes
end
