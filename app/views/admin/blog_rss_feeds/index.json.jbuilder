json.array!(@admin_blog_rss_feeds) do |admin_blog_rss_feed|
  json.extract! admin_blog_rss_feed, :id, :url, :title, :number_of_feeds
  json.url admin_blog_rss_feed_url(admin_blog_rss_feed, format: :json)
end
