
xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    # xml.title "Report"
    # xml.description "Report Description"
    # xml.pubDate "Report Published Date"
    # xml.category_id "Category Id"
    # xml.continentsName "Report Belongs To Continents"
    # xml.pubName "Report Publisher Name"
    # xml.link rss_feeds_url
    
    for report in @reports
      xml.item do
        xml.title report.title
        xml.description :type => "html" do
          xml.cdata!(report.get_description)
        end
        xml.pubDate report.rss_publish_date
        xml.category_id report.category_id
        for country in report.country_reports do
          xml.continentsName country.continent_name
        end 
        xml.typeName report.report_type_name
        xml.link request.protocol + request.host_with_port+report_path(report.slug)
        xml.guid request.protocol + request.host_with_port+report_path(report.slug)
      end
    end
    unless @reports.present?
      xml.title "No feed items found"
    end
  end
end