Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  #config.cache_classes = true

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  # config.action_controller.perform_caching = true

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load


  config.action_controller.perform_caching = false
  config.reload_classes_only_on_change = true
  config.cache_classes = false 


  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  ENV['PUBLISHABLE_KEY'] =  "pk_test_RWP7KmuM5AaStKjtespBHo89"
  ENV['SECRET_KEY'] = "sk_test_K6m4mvl15p9Z6FUZ0LdHtSmw"
  # config.action_mailer.smtp_settings = {
  #   :address   => "smtp.mandrillapp.com",
  #   :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
  #   :enable_starttls_auto => true, # detects and uses STARTTLS
  #   :user_name => "sbodhankar@digitsexperts.com",
  #   :password  => "M5ItmAKLRGGbzCcznhBsBg", # SMTP password is any valid API key
  #   :authentication => 'login', # Mandrill supports 'plain' or 'login'
  #   :domain => 'gmail.com', # your domain to identify your server when connecting
  # }
config.action_mailer.default_url_options = { :host => 'http://127.0.0.1:3000' }


  config.after_initialize do
    paypal_options = {
      :login => "pankaj.bomble_api1.wiseguyreports.com",
      :password => "PBDAG7XTNTARRHPQ",
      :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31Am.UMeGAKKvFqkjfMHpQRxwpr9vQ"
    }
    ::STANDARD_GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
    ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
  end

  # config.after_initialize do
  #   ActiveMerchant::Billing::Base.mode = :test
  #   paypal_options = {
  #     :login => "pratik.ganvir-facilitator_api1.cipher-tech.com",
  #     :password => "1378965376",
  #     :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31AUG24faRoygJXyQvREa8OeI54ptZ"
  #   }
  #   ::STANDARD_GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
  #   ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
  # end

  config.secret_key_base = '87128ecf396e5daa18d29fa6b83b9696f27612e83a2dac32a7e894f298d16627bb88704415ed7ce5022d4b821b20b3dbf8ad71674b1a1e4da826e0cb1dc0e6cd'
end

 