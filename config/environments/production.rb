Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_assets = true

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  # Generate digests for assets URLs.
  config.assets.digest = true
  config.assets.precompile += %w( *.eot *.woff *.ttf *.svg )
  # `config.assets.precompile` has moved to config/initializers/assets.rb


  # config.logger = Logger.new("/var/log/apache2/error.log")

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :info
  ENV['PUBLISHABLE_KEY'] =  "pk_test_RWP7KmuM5AaStKjtespBHo89"
  ENV['SECRET_KEY'] = "sk_test_K6m4mvl15p9Z6FUZ0LdHtSmw"
  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets.
  # application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
  # config.assets.precompile += %w( search.js )

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  
  # config.action_mailer.smtp_settings = {
  #   :address   => "smtp.mandrillapp.com",
  #   :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
  #   :enable_starttls_auto => true, # detects and uses STARTTLS
  #   :user_name => "sbodhankar@digitsexperts.com",
  #   :password  => "M5ItmAKLRGGbzCcznhBsBg", # SMTP password is any valid API key
  #   :authentication => 'login', # Mandrill supports 'plain' or 'login'
  #   :domain => 'gmail.com', # your domain to identify your server when connecting
  # }
  config.action_mailer.default_url_options = { :host => 'http://www.marketresearchfuture.com' }




  config.after_initialize do
    paypal_options = {
      :login => "pankaj.bomble_api1.wiseguyreports.com",
      :password => "PBDAG7XTNTARRHPQ",
      :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31Am.UMeGAKKvFqkjfMHpQRxwpr9vQ"
    }
    ::STANDARD_GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
    ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
  end



  config.secret_key_base = '87128ecf396e5daa18d29fa6b83b9696f27612e83a2dac32a7e894f298d16627bb88704415ed7ce5022d4b821b20b3dbf8ad71674b1a1e4da826e0cb1dc0e6cd'
end


