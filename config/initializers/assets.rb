# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( application.css)
Rails.application.config.assets.precompile += %w( admin.css)
Rails.application.config.assets.precompile += %w( bootstrap.css)
Rails.application.config.assets.precompile += %w( font-awesome.min.css)
Rails.application.config.assets.precompile += %w( slider.css)
Rails.application.config.assets.precompile += %w( flaticon.css)
Rails.application.config.assets.precompile += %w( style.css)
Rails.application.config.assets.precompile += %w( blog_menu.css)
Rails.application.config.assets.precompile += %w( color.css)
Rails.application.config.assets.precompile += %w( font-open-sans.css)

Rails.application.config.assets.precompile += %w( lightslider.min.css)


















Rails.application.config.assets.precompile += %w( admin/*.css)

Rails.application.config.assets.precompile += %w( filter.js)
Rails.application.config.assets.precompile += %w( cbox.js)
Rails.application.config.assets.precompile += %w( objlib.js)

Rails.application.config.assets.precompile += %w( progressbar.gif )
Rails.application.config.assets.precompile += %w( loading.gif )
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( bootstrap.min.js )
Rails.application.config.assets.precompile += %w( owl.carousel.js )
Rails.application.config.assets.precompile += %w( isotope.pkgd.min.js )
Rails.application.config.assets.precompile += %w( jquery.magnific-popup.min.js )
Rails.application.config.assets.precompile += %w( creative-brands.js )
Rails.application.config.assets.precompile += %w( color-switcher.js )
Rails.application.config.assets.precompile += %w( custom.js )
Rails.application.config.assets.precompile += %w( jquery.validate.js )
Rails.application.config.assets.precompile += %w( validation.js )
Rails.application.config.assets.precompile += %w( admin/admin.js )
Rails.application.config.assets.precompile += %w( nicEdit.js )
Rails.application.config.assets.precompile += %w( prototype.js )
Rails.application.config.assets.precompile += %w( http://localhost:9292/faye.js )
Rails.application.config.assets.precompile += %w( checkout.js )
Rails.application.config.assets.precompile += %w( jquery.blockUI.js )
Rails.application.config.assets.precompile += %w( loader.js )
Rails.application.config.assets.precompile += %w( jquery.payment.js )
Rails.application.config.assets.precompile += %w( private_pub.js )
Rails.application.config.assets.precompile += %w( test_chat.js )
Rails.application.config.assets.precompile += %w( users.js )
Rails.application.config.assets.precompile += %w( admin/*.js )
Rails.application.config.assets.precompile += %w( admin/themes/*.js )
Rails.application.config.assets.precompile += %w( admin/layouts/*.js )
Rails.application.config.assets.precompile += %w( captcha.js )
Rails.application.config.assets.precompile += %w( jquery.cslider.js )
Rails.application.config.assets.precompile += %w( modernizr.custom.28468.js )
Rails.application.config.assets.precompile += %w( responsive-slider.js )
Rails.application.config.assets.precompile += %w( jquery.event.move.js )
Rails.application.config.assets.precompile += %w( jquery.flexslider-min.js )
Rails.application.config.assets.precompile += %w( owl.carousel.min.js )
Rails.application.config.assets.precompile += %w( admin/jquery.mtz.monthpicker.js )
Rails.application.config.assets.precompile += %w( jquery.marquee.js )
Rails.application.config.assets.precompile += %w( fwslider.js )
Rails.application.config.assets.precompile += %w( jquery.multiple.select.js )
Rails.application.config.assets.precompile += %w( jquery.fileupload.js )
Rails.application.config.assets.precompile += %w( jquery.fileupload-process.js )
Rails.application.config.assets.precompile += %w( jquery.fileupload-ui.js )
Rails.application.config.assets.precompile += %w( jquery.fileupload-jquery-ui.js )
Rails.application.config.assets.precompile += %w( jquery.ui.widget.js )
Rails.application.config.assets.precompile += %w( jquery.iframe-transport.js)
Rails.application.config.assets.precompile += %w( jquery.fileupload-video.js)
Rails.application.config.assets.precompile += %w( jquery.fileupload-audio.js)
Rails.application.config.assets.precompile += %w( jquery.fileupload-image.js)
Rails.application.config.assets.precompile += %w( jquery.fileupload-validate.js)
Rails.application.config.assets.precompile += %w( main.js)

Rails.application.config.assets.precompile += %w( jquery.social.media.tabs.1.7.1.js)
Rails.application.config.assets.precompile += %w( jquery.plugins.js)
Rails.application.config.assets.precompile += %w( jquery.site.js)
Rails.application.config.assets.precompile += %w( jquery.validationEngine-en.js)
Rails.application.config.assets.precompile += %w( jquery.validationEngine.js)
Rails.application.config.assets.precompile += %w( superfish.js)
Rails.application.config.assets.precompile += %w( jquery.browser.js)

Rails.application.config.assets.precompile += %w( select2.js)

Rails.application.config.assets.precompile += %w( jquery.switchButton.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )
# Rails.application.config.assets.precompile += %w( users.js )


Rails.application.config.assets.precompile += %w(typeahead.jquery.js)
Rails.application.config.assets.precompile += %w( bloodhound.js )
Rails.application.config.assets.precompile += %w( select2.js )
Rails.application.config.assets.precompile += %w( select2.css )
Rails.application.config.assets.precompile += %w( notify.min.js )
Rails.application.config.assets.precompile += %w( jquery.fancybox-1.3.4.js )
Rails.application.config.assets.precompile += %w( utils.js )
Rails.application.config.assets.precompile += %w( intlTelInput.js )
Rails.application.config.assets.precompile += %w( jquery-1.11.3.min.js )
Rails.application.config.assets.precompile += %w( plugins.js )
Rails.application.config.assets.precompile += %w( main1.js )
Rails.application.config.assets.precompile += %w( bootstrap.min.css )
Rails.application.config.assets.precompile += %w( category_carousal.js )


Rails.application.config.assets.precompile += %w( modernizr.custom.17475.js )
Rails.application.config.assets.precompile += %w( lightslider.min.js )
Rails.application.config.assets.precompile += %w( jquery.appear.js )
Rails.application.config.assets.precompile += %w( jquery.easing.1.3.js)