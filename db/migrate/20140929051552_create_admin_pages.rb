class CreateAdminPages < ActiveRecord::Migration
  def change
    create_table :admin_pages do |t|
      t.string :title
      t.text :description
      t.string :slug

      t.timestamps
    end
    add_index :admin_pages, :slug, unique: true
  end
end
