class CreateNewsUpdates < ActiveRecord::Migration
  def change
    create_table :news_updates do |t|
      t.string      :title
      t.text        :description
      t.date        :date
      t.timestamps
    end
  end
end
