class AddSlugToReports < ActiveRecord::Migration
  def change
    add_column :reports, :slug, :string
  end
end
