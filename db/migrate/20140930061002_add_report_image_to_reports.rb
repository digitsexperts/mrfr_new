class AddReportImageToReports < ActiveRecord::Migration
  def change
    add_column :reports, :report_image, :string
  end
end
