class AddDetailsToReports < ActiveRecord::Migration
   def change
    add_column :reports, :upcoming, :boolean
    add_column :reports, :selling_count, :integer
  end
end
