class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string   :company_name
      t.string   :job_title
      t.string   :address_street1
      t.string   :address_street2
      t.string   :city
      t.string   :zip_code
      t.string   :phone_no
      t.integer  :country_id
      t.integer  :user_id
      t.timestamps
    end
  end
end
