class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer  :cart_id
      t.string   :ip_address
      t.string   :order_status
      t.string   :order_total
      t.string   :payment_method_type
      t.integer  :user_id
      t.timestamps
    end
  end
end
