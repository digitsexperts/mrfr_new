class AddCurrencyToShoppingCarts < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :currency, :string
  end
end
