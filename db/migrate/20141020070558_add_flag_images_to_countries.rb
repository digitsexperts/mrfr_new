class AddFlagImagesToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :flag_image, :string
  end
end
