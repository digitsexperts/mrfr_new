class AddSlugToContinent < ActiveRecord::Migration
  def change
    add_column :continents, :slug, :string
  end
end
