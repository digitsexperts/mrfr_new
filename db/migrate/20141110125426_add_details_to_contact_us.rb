class AddDetailsToContactUs < ActiveRecord::Migration
  def change
    add_column :contact_us, :job_title, :string
    add_column :contact_us, :company, :string
    add_column :contact_us, :phone_no, :string
  end
end
