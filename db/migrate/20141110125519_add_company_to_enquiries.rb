class AddCompanyToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :company, :string
  end
end
