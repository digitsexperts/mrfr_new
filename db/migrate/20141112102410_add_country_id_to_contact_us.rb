class AddCountryIdToContactUs < ActiveRecord::Migration
  def change
    add_column :contact_us, :country_id, :integer
  end
end
