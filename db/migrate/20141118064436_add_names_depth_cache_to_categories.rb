class AddNamesDepthCacheToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :names_depth_cache, :string
    add_column :categories, :ancestry, :string
 
  end
end
