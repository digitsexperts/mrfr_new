class AddCountryIdsToReports < ActiveRecord::Migration
  def change
  	change_column :reports, :country_id, :text
  end
end
