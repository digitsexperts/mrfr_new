class AddTitletextToReports < ActiveRecord::Migration
  def change
  	change_column :reports, :title, :text
  	change_column :reports, :description, :text, :limit => 4294967295
  end
end
