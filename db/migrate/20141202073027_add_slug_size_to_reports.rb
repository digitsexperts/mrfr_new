class AddSlugSizeToReports < ActiveRecord::Migration
  def change
  	change_column :reports, :slug, :text
  end
end
