class ReportCountriesJoinTable < ActiveRecord::Migration
  def change
    create_table :countries_reports, id: false do |t|
      t.integer :report_id
      t.integer :country_id
    end
  end
end
