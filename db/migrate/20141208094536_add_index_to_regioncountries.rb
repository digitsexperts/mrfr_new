class AddIndexToRegioncountries < ActiveRecord::Migration
  def change
  	add_index :countries_regions, [:country_id, :region_id]
    add_index :countries_regions, [:region_id, :country_id]
  end
end
