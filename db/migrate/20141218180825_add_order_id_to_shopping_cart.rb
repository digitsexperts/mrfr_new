class AddOrderIdToShoppingCart < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :order_id, :string
  end
end
