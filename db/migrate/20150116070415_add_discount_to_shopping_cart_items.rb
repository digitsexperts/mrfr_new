class AddDiscountToShoppingCartItems < ActiveRecord::Migration
  def change
    add_column :shopping_cart_items, :discount, :string
  end
end
