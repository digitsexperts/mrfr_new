class AddIndexesToPublishers < ActiveRecord::Migration
 def change
    add_index :publishers, :name
  end
end
