class AddMaximumPublishPerDayToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :maximum_publish_per_day, :integer
  end
end
