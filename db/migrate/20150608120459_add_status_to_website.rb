class AddStatusToWebsite < ActiveRecord::Migration
  def change
    add_column :websites, :automation_on_flag, :boolean
  end
end
