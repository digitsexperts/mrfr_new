class AddOldSlugToReport < ActiveRecord::Migration
  def change
    add_column :reports, :old_slug, :text
  end
end
