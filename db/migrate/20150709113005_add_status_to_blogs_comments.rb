class AddStatusToBlogsComments < ActiveRecord::Migration
  def change
    add_column :blogs_comments, :status, :boolean
  end
end
