class AddSlugToAdminBlogs < ActiveRecord::Migration
  def change
    add_column :admin_blogs, :slug, :text
  end
end
