class CreateAdminBlogRssFeeds < ActiveRecord::Migration
  def change
    create_table :admin_blog_rss_feeds do |t|
      t.string :url
      t.string :title
      t.integer :number_of_feeds

      t.timestamps
    end
  end
end
