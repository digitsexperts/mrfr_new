class AddReportsCountToPublisher < ActiveRecord::Migration
  def change
    add_column :publishers, :reports_count, :integer
  end
end
