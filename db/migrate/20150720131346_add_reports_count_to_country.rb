class AddReportsCountToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :reports_count, :integer
  end
end
