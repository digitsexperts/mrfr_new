class AddLatestPublisherReportToReport < ActiveRecord::Migration
  def change
    add_column :reports, :latest_publisher_report, :boolean
    add_index  :reports, :latest_publisher_report
  end
end
