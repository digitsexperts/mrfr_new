class CreateNewsletterTemplates < ActiveRecord::Migration
  def change
    create_table :newsletter_templates do |t|
      t.text :title
      t.text :header
      t.text :body
      t.text :footer

      t.timestamps
    end
  end
end
