class AddSubjectToNewsletterTemplates < ActiveRecord::Migration
  def change
    add_column :newsletter_templates, :subject, :string
  end
end
