class AddUpdateDateToNewsletterTemplates < ActiveRecord::Migration
  def change
    add_column :newsletter_templates, :update_date, :integer
  end
end
