class AddDeltaToCategories < ActiveRecord::Migration
  def change
  	add_column :categories, :delta, :boolean, :default => true
  end
end
