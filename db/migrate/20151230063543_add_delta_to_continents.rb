class AddDeltaToContinents < ActiveRecord::Migration
  def change
  	add_column :continents, :delta, :boolean, :default => true
  end
end
