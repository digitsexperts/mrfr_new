class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :client_logo
      t.string :slug
      t.timestamps
    end
  end
end
