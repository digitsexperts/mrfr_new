class CreateReportTypes < ActiveRecord::Migration
  def change
    create_table :report_types do |t|
      t.string :name
      t.text :description
      t.string :abbreviation
      t.string :slug
      t.timestamps
    end
  end
end
