class RenamePublisherIdInReports < ActiveRecord::Migration
  def change
  	rename_column :reports, :publisher_id, :report_type_id
  end
end
