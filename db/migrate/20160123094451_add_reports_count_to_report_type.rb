class AddReportsCountToReportType < ActiveRecord::Migration
  def change
    add_column :report_types, :reports_count, :string
  end
end
