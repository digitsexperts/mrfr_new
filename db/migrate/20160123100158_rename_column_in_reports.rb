class RenameColumnInReports < ActiveRecord::Migration
  def change
  	rename_column :reports, :latest_publisher_report, :latest_report_type_report
  end
end
