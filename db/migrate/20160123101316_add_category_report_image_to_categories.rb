class AddCategoryReportImageToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :category_report_image, :string
  end
end
