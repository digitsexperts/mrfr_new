class AddToSeoDescriptionToReportTypes < ActiveRecord::Migration
  def change
  	add_column :report_types, :seo_description, :text
  end
end
