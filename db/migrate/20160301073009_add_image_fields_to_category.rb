class AddImageFieldsToCategory < ActiveRecord::Migration
  def change
  	add_column :categories, :category_image, :string
  	add_column :categories, :category_icon, :string
  end
end
