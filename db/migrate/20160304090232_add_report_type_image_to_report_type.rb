class AddReportTypeImageToReportType < ActiveRecord::Migration
  def change
    add_column :report_types, :report_type_image, :string
  end
end
