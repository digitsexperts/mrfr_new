class AddCompanyLogoToTestimonials < ActiveRecord::Migration
  def change
    add_column :testimonials, :company_logo, :string
  end
end
