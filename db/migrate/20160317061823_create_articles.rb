class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :content, :limit => 4294967295
      t.date :published_date
      t.text :slug

      t.timestamps
    end
  end
end
