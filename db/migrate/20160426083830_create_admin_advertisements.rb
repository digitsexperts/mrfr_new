class CreateAdminAdvertisements < ActiveRecord::Migration
  def change
    create_table :admin_advertisements do |t|
      t.string :name

      t.timestamps
    end
  end
end
