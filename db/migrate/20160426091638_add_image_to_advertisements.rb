class AddImageToAdvertisements < ActiveRecord::Migration
  def change
    add_column :admin_advertisements, :advertisement_image, :string
  end
end
