# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160426091638) do

  create_table "admin_advertisements", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "advertisement_image"
  end

  create_table "admin_blog_rss_feeds", force: true do |t|
    t.string   "url"
    t.string   "title"
    t.integer  "number_of_feeds"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_blogs", force: true do |t|
    t.text     "title"
    t.text     "content"
    t.integer  "user_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "author"
    t.text     "slug"
  end

  create_table "admin_pages", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_pages", ["slug"], name: "index_admin_pages_on_slug", unique: true, using: :btree

  create_table "article_excelsheets", force: true do |t|
    t.string   "article_excelsheet_file"
    t.string   "ipaddress"
    t.string   "browser_os"
    t.text     "positions"
    t.integer  "status"
    t.boolean  "notified"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "articles", force: true do |t|
    t.text     "title"
    t.text     "content",        limit: 2147483647
    t.date     "published_date"
    t.text     "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "article_image"
  end

  create_table "audits", force: true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index", using: :btree
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
  add_index "audits", ["created_at"], name: "index_audits_on_created_at", using: :btree
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
  add_index "audits", ["user_id", "user_type"], name: "user_index", using: :btree

  create_table "authorizations", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banned_ips", force: true do |t|
    t.string   "ip_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blogs_comments", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.integer  "blog_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reports_count",         default: 0
    t.string   "slug"
    t.string   "names_depth_cache"
    t.string   "ancestry"
    t.boolean  "delta",                 default: true
    t.string   "abbreviation"
    t.string   "category_report_image"
    t.string   "category_image"
    t.string   "category_icon"
  end

  add_index "categories", ["name"], name: "index_categories_on_name", using: :btree

  create_table "chat_users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "chats", force: true do |t|
    t.string   "customer_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "client_logo"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contact_us", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "job_title"
    t.string   "company"
    t.string   "phone_no"
    t.integer  "country_id"
  end

  create_table "continents", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "reports_count"
    t.boolean  "delta",         default: true
  end

  create_table "conversations", force: true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string  "name"
    t.string  "abbreviation",  limit: 5
    t.boolean "active",                  default: false
    t.string  "flag_image"
    t.integer "continent_id"
    t.text    "region_ids"
    t.integer "reports_count"
    t.boolean "delta",                   default: true
  end

  add_index "countries", ["active"], name: "index_countries_on_active", using: :btree
  add_index "countries", ["name"], name: "index_countries_on_name", using: :btree

  create_table "countries_regions", id: false, force: true do |t|
    t.integer "country_id"
    t.integer "region_id"
  end

  add_index "countries_regions", ["country_id", "region_id"], name: "index_countries_regions_on_country_id_and_region_id", using: :btree
  add_index "countries_regions", ["region_id", "country_id"], name: "index_countries_regions_on_region_id_and_country_id", using: :btree

  create_table "countries_reports", id: false, force: true do |t|
    t.integer "report_id"
    t.integer "country_id"
  end

  add_index "countries_reports", ["country_id", "report_id"], name: "index_countries_reports_on_country_id_and_report_id", using: :btree
  add_index "countries_reports", ["report_id", "country_id"], name: "index_countries_reports_on_report_id_and_country_id", using: :btree

  create_table "cron_controllers", force: true do |t|
    t.boolean  "refresh_cache"
    t.boolean  "dumping_in_progress"
    t.boolean  "dumping_allowed"
    t.boolean  "local_indexing_need"
    t.boolean  "remote_indexing_need"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "old_reports_count"
    t.boolean  "remote_indexing_in_progress"
    t.boolean  "local_indexing_in_progress"
  end

  create_table "custom_auto_increments", force: true do |t|
    t.string   "model_name"
    t.integer  "counter",    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "custom_auto_increments", ["model_name"], name: "index_custom_auto_increments_on_model_name", using: :btree

  create_table "customers", force: true do |t|
    t.string   "company_name"
    t.string   "job_title"
    t.string   "address_street1"
    t.string   "address_street2"
    t.string   "city"
    t.string   "zip_code"
    t.string   "phone_no"
    t.integer  "country_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "enquiries", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "job_title"
    t.string   "phone_no"
    t.text     "interest_area"
    t.text     "enquiry"
    t.text     "message"
    t.string   "enquiry_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "report_id"
    t.string   "company"
    t.integer  "country_id"
  end

  create_table "excelsheets", force: true do |t|
    t.string   "excelsheet_file"
    t.string   "ipaddress"
    t.string   "browser_os"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "positions"
    t.boolean  "dump_status",          default: false
    t.integer  "status"
    t.integer  "old_reports_count"
    t.integer  "new_reports_count"
    t.integer  "failed_reports_count"
    t.boolean  "notified",             default: false
    t.text     "failed_csv_url"
    t.boolean  "failed_reports"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "infographics_details", force: true do |t|
    t.string   "infographics"
    t.integer  "report_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "news_updates", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "news_image"
    t.integer  "category_id"
    t.text     "slug"
  end

  create_table "newsletter_templates", force: true do |t|
    t.text     "title"
    t.text     "header"
    t.text     "body"
    t.text     "footer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "subject"
    t.integer  "update_date"
  end

  create_table "order_details", force: true do |t|
    t.integer  "order_id"
    t.string   "order_details_status"
    t.integer  "report_id"
    t.string   "report_value"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.integer  "cart_id"
    t.string   "ip_address"
    t.string   "order_status"
    t.string   "order_total"
    t.string   "payment_method_type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "press_release_reports", force: true do |t|
    t.integer  "website_id"
    t.integer  "report_id"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "success"
    t.text     "failed"
  end

  create_table "publishers", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo"
    t.string   "slug"
    t.string   "report_image"
    t.boolean  "active",             default: true
    t.integer  "reports_count"
    t.boolean  "sample_report_flag", default: true
    t.boolean  "report_price_flag",  default: true
    t.boolean  "delta",              default: true
    t.string   "report_background"
    t.string   "text_position"
    t.string   "text_color"
  end

  add_index "publishers", ["name"], name: "index_publishers_on_name", using: :btree

  create_table "rates", force: true do |t|
    t.string   "currency"
    t.decimal  "exchange_rate", precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "regions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "reports_count"
    t.boolean  "delta",         default: true
  end

  create_table "report_types", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "abbreviation"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reports_count"
    t.text     "seo_description"
    t.string   "report_type_image"
  end

  create_table "reports", force: true do |t|
    t.string   "title",                     limit: 500
    t.text     "description",               limit: 2147483647
    t.text     "table_of_content",          limit: 2147483647
    t.integer  "publish_date"
    t.float    "price",                     limit: 24
    t.integer  "report_type_id"
    t.integer  "category_id"
    t.text     "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "slug"
    t.string   "report_image"
    t.boolean  "upcoming"
    t.integer  "selling_count"
    t.string   "report_pdf"
    t.string   "code"
    t.float    "one_user",                  limit: 24
    t.float    "five_user",                 limit: 24
    t.float    "site_user",                 limit: 24
    t.float    "enterprise_user",           limit: 24
    t.integer  "no_of_pages"
    t.string   "day"
    t.string   "publish_month"
    t.boolean  "active",                                       default: true
    t.integer  "region_id"
    t.integer  "continent_id"
    t.string   "excelsheet_row"
    t.string   "browser_os"
    t.string   "ipaddress"
    t.integer  "excelsheet_file_id"
    t.text     "old_slug"
    t.boolean  "delta",                                        default: true, null: false
    t.text     "country_ids"
    t.boolean  "latest_report_type_report"
    t.boolean  "sample_report_flag",                           default: true
    t.text     "infographics"
  end

  add_index "reports", ["category_id"], name: "index_reports_on_category_id", using: :btree
  add_index "reports", ["continent_id"], name: "index_reports_on_continent_id", using: :btree
  add_index "reports", ["enterprise_user"], name: "index_enterprise_user", using: :btree
  add_index "reports", ["five_user"], name: "index_five_user", using: :btree
  add_index "reports", ["latest_report_type_report"], name: "index_reports_on_latest_report_type_report", using: :btree
  add_index "reports", ["one_user"], name: "index_one_user", using: :btree
  add_index "reports", ["region_id"], name: "index_reports_on_region_id", using: :btree
  add_index "reports", ["report_type_id"], name: "index_reports_on_report_type_id", using: :btree
  add_index "reports", ["site_user"], name: "index_site_user", using: :btree

  create_table "shopping_cart_items", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "quantity"
    t.integer  "item_id"
    t.string   "item_type"
    t.float    "price",            limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "purchase_type"
    t.string   "discount"
    t.decimal  "discounted_price",            precision: 10, scale: 0
  end

  create_table "shopping_carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "currency"
    t.string   "order_id"
  end

  create_table "simple_captcha_data", force: true do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

  create_table "subscribers", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status"
    t.text     "reason"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "testimonials", force: true do |t|
    t.string   "name"
    t.string   "position"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "company_logo"
  end

  create_table "users", force: true do |t|
    t.string   "first_name",                         null: false
    t.string   "last_name",                          null: false
    t.string   "crypted_password",                   null: false
    t.string   "password_salt",                      null: false
    t.string   "email",                              null: false
    t.string   "persistence_token",                  null: false
    t.string   "single_access_token",                null: false
    t.string   "perishable_token",                   null: false
    t.integer  "login_count",            default: 0, null: false
    t.integer  "failed_login_count",     default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.boolean  "is_admin"
    t.string   "slug"
    t.text     "omniauth_data"
    t.boolean  "is_superadmin"
    t.boolean  "is_blog_admin"
  end

  create_table "websites", force: true do |t|
    t.string   "website_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "password"
    t.integer  "maximum_publish_per_day"
    t.boolean  "automation_on_flag"
  end

  create_table "wiseguy_tweets", force: true do |t|
    t.string   "user_screen_name"
    t.text     "tweet_text"
    t.datetime "tweet_created_at"
    t.string   "tweet_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
